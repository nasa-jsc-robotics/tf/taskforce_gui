from pyqtlib import QtCore


class SubscriberThread(QtCore.QThread):
    """
    A QThread attached to an taskforce_common.Subscriber port that
    will emit an event when a new message is received.

    signals:
    - messageReceived(object) - gets emitted when a message is received on the port.
                                the data value is the message received
    """

    messageReceived = QtCore.pyqtSignal(object)

    def __init__(self, subscriber, parent=None):
        """
        Args:
            subscriber: Subscriber object which is a subclass of a taskforce_common.Subscriber
            parent: Qt Parent
        """
        QtCore.QObject.__init__(self, parent)
        self.subscriber = subscriber
        self._running = False

    def run(self):
        """Main thread loop.  Emits messages coming from the subscriber"""
        self._running = True
        while self._running:
            msg = self.subscriber.receive()
            if msg is not None:
                self.messageReceived.emit(msg)

    def stop(self):
        """
        Stop the main loop.
        """
        self._running = False
