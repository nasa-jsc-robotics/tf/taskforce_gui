from pyqtlib import QtCore

from taskforce_common import Group
from taskforce_common.utils import createObject


class GroupModelException(Exception):
    pass


class GroupModel(QtCore.QObject):
    """The GroupModel acts as a data model for a Group."""

    # Signals
    taskAdded = QtCore.pyqtSignal(str, str)  # name, className - Emitted when a Task is added
    subGroupAdded = QtCore.pyqtSignal(str, str)  # name, fileName - Emitted when a SubBlock is added
    blockRemoved = QtCore.pyqtSignal(str)  # instanceName - Emitted when either a Task or SubBlock is removed
    blockNameChanged = QtCore.pyqtSignal(str,
                                         str)  # oldname, newname - Emitted whenever a Task or SubBlock has changed it's name

    subscribeEvent = QtCore.pyqtSignal(str, str, str,
                                       str, bool)  # eventName, source, destination, callback - Emitted when a Task/SubBlock subscribes to an event
    unsubscribeEvent = QtCore.pyqtSignal(str, str, str,
                                         str)  # eventName, source, destination, callback - Emitted when an event is unsubscribed
    unsubscribeFromSourceEvent = QtCore.pyqtSignal(str,
                                                   str)  # subscriberName, source - Emitted when all Tasks/SubBlocks should unsubscribe from an event source

    # Signal
    statusChanged = QtCore.pyqtSignal(str, dict)  # name, status - Emitted when the status of an object has changed

    # This is a special name used to reference this block
    BlockAlias = '<group>'

    def __init__(self, name, file_name, parent=None):
        QtCore.QObject.__init__(self, parent)

        self.group = Group(name, file_name)
        self.reset(name, file_name)         # defer to an init method so we can re-use code in deserialize

    @property
    def name(self):
        return self.group.name

    @property
    def path(self):
        return self.group.path

    @property
    def fileName(self):
        return self.group.fileName

    def _clear(self):
        """
        Clear all data from the model.

        NOTE: This also destroys the information for this block
        """
        self.group.clearAll()

    def addSubGroup(self, group):
        """Add a sub-group

        This method will emit the subGroupAdded signal

        Args:
            group: Group object
        """
        self.group.addSubGroup(group)
        self.subGroupAdded.emit(group.name, group.fileName)

    def addTask(self, task):
        """Add a Task to the Group

        Args:
            task: Task object
        """
        self.group.addTask(task)
        self.taskAdded.emit(task.name, task.className)

    def changeName(self, oldName, newName):
        """Change the name of a task of group.

        Args:
            oldName: current/old name of the object as a string
            newName: new name for the object as a string
        """
        self.group.changeName(oldName, newName)
        self.blockNameChanged.emit(oldName, newName)

    def deserialize(self, definition, continue_on_error=False):
        """
        Populate the model from a model definition

        Args:
            definition: Model definition as a Python dictionary
        """
        name = str(definition['group']['name'])
        # file_name = str(definition['group']['fileName'])
        self._clear()

        # subscriptionDict = {}

        for task in definition['group']['tasks']:
            task = task['task']
            name = str(task['name'])
            module = task['module']
            className = str(task['class'])
            parameters = task['parameters']
            taskObj = createObject(module, className, args=(name,))
            taskObj.setParameter(parameters)

            self.addTask(taskObj)

        for subGroup in definition['group']['subGroups']:
            name = str(subGroup['name'])
            fileName = str(subGroup['fileName'])
            parameters = subGroup.get("parameters", {})
            blockTask = Group(name, fileName=fileName)
            blockTask.setParameter(parameters)
            self.addSubGroup(blockTask)

        for subscription in definition['group']['subscriptions']:
            breakpoint = bool(subscription.get('breakpoint', False))
            self.subscribe(str(subscription['eventName']), str(subscription['source']), str(subscription['destination']), str(subscription['callback']), breakpoint)

        for parameter_binding in definition['group']['parameterBindings']:
            sourceKey = parameter_binding['sourceKey']
            bindings = parameter_binding['bindings']
            for binding in bindings:
                target = binding['target']
                destinationKey = binding['destinationKey']
                self.group.bindParameter(sourceKey, destinationKey, target)

        group_parameters = definition['group']['parameters']
        self.group.setParameter(group_parameters)

    def getBlocks(self, name=None):
        """
        Args:
            name: The name of the block trying to get. This differs from "getChild" in that the root group can be returned.

        Returns: The desired block object
        """
        if name == self.name:
            return self.group
        return self.group.getBlocks(name)

    def getChild(self, name):
        """
        Return the block (Task or SubGroup) with the given name
        """
        return self.group.getChild(name)

    def getChildren(self, recursive=True):
        """
        Get all objects in the model as a dictionary.

        Args:
            recursive: Return children of subgroups as well

        Returns:
            Dictionary of name : instance
        """
        return self.group.getChildren(recursive)

    def getCommandNames(self, name):
        """Get the list of registered command names from a block

        Args:
            name: name of the block

        Returns:
            Command names as a list of strings
        """
        child = self.group.getBlocks(name, recursive=False)
        return child.getCommandNames()

    def getEventNames(self, name):
        """
        Get a list of all emitted events from an object

        Args:
            name: Name of the object (Task or SubBlock)

        Returns: List of strings
        """
        block = self.group.getBlocks(name)
        return block.getEventNames()

    def getFileName(self):
        """
        Get the file name of this block

        Returns:
            Filename as a string
        """
        return self.group.fileName

    def getInfo(self, keyword=None):
        """
        Gets information about the object

        Args:
            keyword: Key to index the dictionary that holds information about the object

        Returns:
            If keyword is none, returns a dictionary of basic info about the object. Otherwise, returns the value
            indexed by the key.
        """
        return self.group.getInfo(keyword)

    def getSubGroups(self, recursive=True):
        """ Get the subgroups

        Args:
            recursive: get subgroups from subgroups as well

        Returns:
            A set of subgroups
        """
        return self.group.getSubGroups(recursive)

    def getSubscriptions(self, recursive=True):
        """Get the subscriptions

        Args:
            recursive: get subscriptions from subgroups as well

        Returns:
            A set of subscriptions
        """
        return self.group.getSubscriptions(recursive)

    def getTasks(self, recursive=True):
        """
        Gets the tasks that are in the group

        Args:
            recursive:  Boolean that indicates whether or not to search recursively

        Returns:
            Tasks that are assigned to the Group
        """
        return self.group.getTasks(recursive)

    def removeChild(self, name):
        """
        Remove a Task or SubBlock by name,

        NOTE: This method will emit the "blockRemoved" signal.

        Args:
            name: Name of the child block as a string
        """
        child = self.group.removeChild(name)

        if child is not None:
            self.blockRemoved.emit(name)

    def reset(self, name=None, file_name=None):
        """
        Reset the model.  This will remove all added Tasks and sub-blocks

        Args:
            name: New name for the block as a string.  If None, the block's name will not change
            file_name: New file_name for the block as a string.  If None, the block's file_name will not change
        """
        self._clear()

    def serialize(self):
        """
        Serialize this model into a Python dictionary.

        Returns:
            Serialized model as a Python dictionary

        """
        return self.group.serialize()

    def setFileName(self, name):
        """
        Set the filename of this block

        Args:
            name: new filename as a string
        """
        self.group._fileName = name

    def setName(self, name):
        """
        Set the name of this block

        Args:
            name: name of the block as a string
        """
        self.group._name = name

    def setStatus(self, name, status):
        """Set the status of a child

        This method will emit the statusChanged signal

        Args:
            name: name of the child as a string
            status: status dictionary
        """
        child = self.group.getChild(name)

        currentStatus = child.status()
        if currentStatus != status:
            child.setStatus(status)
            self.statusChanged.emit(name, status)

    def status(self, keyword=None):
        """Gets the status of a group and adds additional "fileName" information

        Args:
            keyword: The name of the key used to index an item in the dictionary. If None, returns whole dictionary

        Returns:
            If keyword is None, returns the status information of the Block as a dictionary. Otherwise, returns
            the item indexed by the keyword.
        """
        return self.group.status(keyword)

    def subscribe(self, eventName, source, destination, callback, breakpoint):
        """
        Subscribed one task / port to another task/port

        This method will emit the "subscribeEvent" signal

        Args:
            eventName : Name of the event as a string
            source: Source Group / Task name as a string
            destination: Target Group / Task subscribing to the event as a string
            callback: Callback method in the destination as a string
        """
        self.group.subscribeEvent(eventName, source, destination, callback, breakpoint)
        self.subscribeEvent.emit(eventName, source, destination, callback, breakpoint)

    def unsubscribe(self, eventName, source, destination, callback):
        """ Unsubscribe an event

        Args:
            eventName : Name of the event as a string
            source: Source Group / Task name as a string
            destination: Target Group / Task subscribing to the event as a string
            callback: Callback method in the destination as a string
        """
        self.group.unsubscribeEvent(eventName, source, destination, callback)
        self.unsubscribeEvent.emit(eventName, source, destination, callback)

    def unsubscribeFromSource(self, subscriberName, source):
        """
        Unsubscribe all events from a particular source
        Args:
            subscriberName: name of Task/SubBlock that should unsubscribe
            source: Source from which all events should be unsubscribed
        """
        subscriber = self.getChild(subscriberName)
        subscriber.unsubscribeFromSource(source)
        self.unsubscribeFromSourceEvent.emit(subscriberName, source)
