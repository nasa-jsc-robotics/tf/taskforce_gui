from pyqtlib import QtGui, QtCore
import os
import re
import sys
import fnmatch

from taskforce_library.utils import getLibraryInfo
from taskforce_common import BLOCK_FILE_EXTENSION
from .color_definitions import TaskForceColors

from taskforce_common.utils import jsonToString


class LibraryModel(QtGui.QFileSystemModel):
    """
    The LibraryModel is an extension of the QFileSystemModel.  This subclass allows for having multiple "roots"
    within the same QFileSystemModel by leveraging symbolic links.  The idea is that the root is set to a container,
    and all sub-libraries are symlinks to other directories.

    For drag-and-drop events, this model provides serialized JSON dictionary as a string with the following format:

    {
        "rootPath" : <string>,
        "relativePaths" : [ <string>, ... ]
    }

    To convert back to a dictionary, use taskforce_common.utils.stringToJson

    The LibraryModel is probably best viewed with the LibraryView
    """
    def __init__(self, parent=None):
        super(LibraryModel, self).__init__(parent)

        self.fileMap = {}
        self.reverseFileMap = {}
        self.filters = []

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        Overload the base data method to handle the QtCore.Qt.ForegroundRole.

        Args:
            index: QModelIndex
            role: data role

        Returns:
            If ForegroundRole color, else base class return value
        """
        if role == QtCore.Qt.ForegroundRole:
            data = str(super(LibraryModel, self).data(index, QtCore.Qt.DisplayRole))
            if data == '__init__.py':
                return None
            elif data.endswith(BLOCK_FILE_EXTENSION):
                return TaskForceColors.GroupBlockDefaultColor
            elif data.endswith('.py'):
                return TaskForceColors.TaskBlockDefaultColor
            else:
                return None
        else:
            return QtGui.QFileSystemModel.data(self, index, role)

    def mimeData(self, list_of_QModelIndex):
        """
        Called when mimeData is required from the model.  This generally happens during drag-n-drop events.

        This method will add a text value to start QFileSystemModel's standard file url.  This is to make it
        compatible with the GroupEditor.

        Args:
            list_of_QModelIndex: list of indexes

        Returns:
            QMime object
        """
        mime = super(LibraryModel, self).mimeData(list_of_QModelIndex)
        paths = []

        if len(mime.urls()) > 0:
            # convert every URL to a string
            for url in mime.urls():
                path = str(url.path())
                relative_path = self.getRelativePath(path)
                paths.append(relative_path)

        data = {'rootPath': str(self.rootPath()),
                'relativePaths': paths}

        jsonString = jsonToString(data)
        mime.setText(jsonString)
        return mime

    def getFileMap(self):
        """
        Return the mapping of relative paths to full paths

        Returns:
            Dictionary
        """
        return self.fileMap

    def getFullPath(self, relativePath):
        """
        Convert a relative path to an absolute path.

        If the file is not a sub-path of the library, the original path is returned.
        If the file does not exist, None is returned.

        Args:
            relativePath: library-root-relative path as a string

        Returns:
            Path as a string, or None
        """
        try:
            return self.fileMap[relativePath]
        except KeyError:
            pass

        root = str(self.rootPath())
        path = os.path.realpath(os.path.join(root, relativePath))
        if os.path.exists(path):
            return path
        else:
            return None

    def getLibraryInfo(self):
        """
        Get the information dictionary of all the modules in the library.

        This dictionary has the form:
            <module name as a string> : { 'hash': <module sha1 hash in hex>,
                                          'path': <module absolute path (str)>,
                                          'rel_path': <module relative path (str)> }

        Returns:
            Python dictionary of module info.
        """
        info = getLibraryInfo(str(self.rootPath()), followlinks=True)
        return info

    def setNameFilters(self, names):
        """
        Set the name filters for the QFileSystemModel.

        This method is overloaded in order to create regex filters for the file mapper.

        Args:
            names: list of strings (i.e. ["*.py", "*.block"])
        """
        super(LibraryModel, self).setNameFilters(names)
        self.filters = []
        for name in names:
            regex = fnmatch.translate(name)
            self.filters.append(re.compile(regex))

    def getRelativePath(self, path):
        """
        Return the library-relative path.

        If the path is not a sub-path of the library, the original path is returned.
        If the path does not exist, None is returned.

        Args:
            path: absolute path as a string

        Returns:
            Relative path as a string
        """
        try:
            return self.reverseFileMap[path]
        except KeyError:
            pass

        root = str(self.rootPath())
        relative_path = os.path.relpath(path, root)
        if relative_path.startswith('..'):
            return path
        else:
            return relative_path

    def setRootPath(self, path):
        """
        Set the root path for the QFileSystemModel.

        This method will also add this path to sys.path

        Args:
            path: root folder of the library
        """
        path = str(path)
        if not os.path.exists(path):
            raise Exception('Trying to set library root, but the path does not exist:{}'.format(path))
        root_dir = str(self.rootPath())
        if root_dir in sys.path:
            sys.path.remove(root_dir)

        super(LibraryModel, self).setRootPath(path)

        root_dir = str(self.rootPath())
        sys.path = [root_dir] + sys.path

        self.updateFileMap()

    def updateFileMap(self):
        """
        Refresh the internal file mappings

        Args:
            filters: regex filter expressions

        Returns:
            tuple of (dictionary of relative-to-absolute path, dictionary of absolute-to-relative path)
        """
        library_root = str(self.rootPath())
        map = {}
        reverse_map = {}

        for root, dirs, files in os.walk(library_root, followlinks=True):
            for file in files:
                match = False
                for filt in self.filters:
                    if filt.match(file):
                        match = True
                        break

                if match:
                    relative_path = self.getRelativePath(os.path.join(root, file))
                    real_path = self.getFullPath(relative_path)

                    map[relative_path] = real_path
                    reverse_map[real_path] = relative_path

        self.fileMap = map
        self.reverseFileMap = reverse_map

        return map, reverse_map
