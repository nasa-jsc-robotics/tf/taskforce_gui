from pyqtlib import QtCore
import logging
from .SubscriberThread import SubscriberThread


class EngineStatusModel(QtCore.QObject):

    statusUpdated = QtCore.pyqtSignal()          # gets emitted when new data is available
    connectionStateChanged = QtCore.pyqtSignal(bool)   # sends connected state

    statusTimeout = 2000  # milliseconds
    subscriberShutdownTimeout = 5000  # milliseconds

    def __init__(self, subscriber, parent=None):
        QtCore.QObject.__init__(self, parent)

        self.status = {}
        self.subscriberThread = SubscriberThread(subscriber)
        self.subscriberThread.messageReceived.connect(self.updateStatus)

        self._connected = False
        self._shuttingDown = False

        self.staleTimer = QtCore.QTimer()
        self.staleTimer.timeout.connect(self.staleTimerTimeout)
        self.staleTimer.setSingleShot(True)

        self.subscriberThread.start()
        self.logger = logging.getLogger(self.__class__.__name__)

    @QtCore.pyqtProperty(bool, notify=connectionStateChanged)
    def connected(self):
        return self._connected

    @connected.setter
    def connected(self, value):
        """
        Set the connection property.

        When this property toggles (i.e. goes from False to True, or vice verse), this class
        will emit the "connectionStateChanged" signal

        Args:
            value: new connection state as a boolean
        """
        stateChanged = (self._connected != value)
        self._connected = value
        if stateChanged:
            self.connectionStateChanged.emit(self._connected)

    @property
    def connectedAsString(self):
        if self.connected:
            return 'connected'
        else:
            return 'disconnected'

    @QtCore.pyqtSlot()
    def staleTimerTimeout(self):
        """
        Slot connected to the stale data timeout timer.
        """
        self.connected = False

    def getStatus(self, task=None):
        """
        Get the current status from the engine

        Returns:
            Lastest engine status message
        """
        if task is not None:
            try:
                return self.status[task]
            except Exception:
                msg = (
                    'Group ' + task + ' is listed in engine view but is shutdown. '
                    'To clear the from view, shutdown all groups and blocks in the top level group.'
                )
                self.logger.error(msg)
                return None
        else:
            return self.status

    @QtCore.pyqtSlot(object)
    def updateStatus(self, msg):
        """
        Slot connected to the Subscriber thread.

        This method will emit the statusUpdated signal.
        If connection is being re-established, the connectionStateChanged signal will be emitted.

        Args:
            msg: incoming status message from the engine
        """
        if self._shuttingDown:
            self.staleTimer.stop()
            return
        self.connected = True
        self.status = msg.data
        self.statusUpdated.emit()

        self.staleTimer.start(EngineStatusModel.statusTimeout)

    @QtCore.pyqtSlot()
    def shutdown(self, block=True):
        """
        Shutdown the subscriber

        Args:
            block: If true, this method will block until the subscriber thread exits.
        """
        self._shuttingDown = True
        # force a status update
        self.updateStatus(None)
        self.subscriberThread.stop()
        if block:
            self.subscriberThread.wait(EngineStatusModel.subscriberShutdownTimeout)
