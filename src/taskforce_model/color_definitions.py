# This file contains common color definitions.
from pyqtlib import QtGui, QtCore


class TaskForceColors(object):

    Orange = QtGui.QColor(0xFF, 0xA5, 0x00)

    # TaskBlockDefaultColor = QtGui.QColor(0x33,0xCC,0xFF)
    TaskBlockDefaultColor = QtGui.QColor(0x00, 0xAA, 0xFF)
    GroupBlockDefaultColor = QtGui.QColor(QtCore.Qt.darkYellow)

    ProxyBorderDefaultColor = QtGui.QColor(QtCore.Qt.gray)

    SelectedTextDefaultColor = QtGui.QColor(QtCore.Qt.blue)

    StatusNotDeployed = QtGui.QColor(QtCore.Qt.black)
    StatusDeployed = TaskBlockDefaultColor
    StatusInitialized = QtGui.QColor(QtCore.Qt.white)
    StatusStarted = QtGui.QColor(QtCore.Qt.green)
    StatusRunning = QtGui.QColor(QtCore.Qt.green)
    StatusComplete = QtGui.QColor(QtCore.Qt.darkGreen)
    StatusFailed = QtGui.QColor(QtCore.Qt.red)
    StatusStopped = QtGui.QColor(QtCore.Qt.gray)
    StatusPaused = None
    StatusShutdown = QtGui.QColor(QtCore.Qt.darkGray)
    StatusUnknown = QtGui.QColor(QtCore.Qt.black)

    StatusColor = {'DEPLOYED': StatusDeployed,
                   'NOTDEPLOYED': StatusNotDeployed,
                   'INITIALIZED': StatusInitialized,
                   'STARTED': StatusStarted,
                   'RUNNING': StatusRunning,
                   'COMPLETE': StatusComplete,
                   'FAILED': StatusFailed,
                   'STOPPED': StatusStopped,
                   'PAUSED': StatusPaused,
                   'SHUTDOWN': StatusShutdown}

    StatusCallbackRunning = QtGui.QColor(QtCore.Qt.green)

    BlockEditorBackgroundColor = QtGui.QColor(QtCore.Qt.white)
    BlockLiveViewBackgroundColorConnected = QtGui.QColor(QtCore.Qt.lightGray)
    BlockLiveViewBackgroundColorDisconnected = QtGui.QColor(QtCore.Qt.yellow)
