from pyqtlib import QtCore, QtGui
from taskforce_common import GroupException

TARGET_PLACEHOLDER = '<block>'
DESTINATION_PLACEHOLDER = '<param>'


class ParameterBindingModel(QtGui.QStandardItemModel):

    targetColumn = 0
    destinationKeyColumn = 1

    bindingsChanged = QtCore.pyqtSignal()

    def __init__(self, group=None, parent=None):
        super(ParameterBindingModel, self).__init__(parent)
        self.group = None
        self.setGroup(group)
        self.itemChanged.connect(self._itemChangedHandler)

    def _itemChangedHandler(self, item):
        row = item.row()
        parent = item.parent()
        try:
            target = parent.child(row, ParameterBindingModel.targetColumn).data(QtCore.Qt.DisplayRole)
            destinationKey = parent.child(row, ParameterBindingModel.destinationKeyColumn).data(QtCore.Qt.DisplayRole)
        except:
            return

        if target and destinationKey and target != TARGET_PLACEHOLDER and destinationKey != DESTINATION_PLACEHOLDER:
            self.setGroupBindingsFromModel()

    def addBinding(self, item, target=TARGET_PLACEHOLDER, destinationKey=DESTINATION_PLACEHOLDER):
        """
        Add a binding

        Args:
            item: QStandardItem
            target: Name of the target block for the binding
            destinationKey: Parameter name of the target for the binding
        """
        if item.parent() is not None:
            raise Exception('Bindings must be added to group parameters')
        param_item = self.item(item.row(), ParameterBindingModel.targetColumn)
        param_item.appendRow([QtGui.QStandardItem(target), QtGui.QStandardItem(destinationKey)])

    def insertBinding(self, idx, target=TARGET_PLACEHOLDER, destinationKey=DESTINATION_PLACEHOLDER):
        """
        Insert a binding

        Args:
            idx: QModelIndex
            target: Name of the target block for the binding
            destinationKey: Parameter name of the target for the binding
        """
        if idx.parent() == self.invisibleRootItem().index():
            raise Exception('Bindings must be added to group parameters')
        param_item = self.item(idx.parent().row(), ParameterBindingModel.targetColumn)
        param_item.insertRow(idx.row(), [QtGui.QStandardItem(target), QtGui.QStandardItem(destinationKey)])

    def getOptions(self, index):
        """
        Get a list of options for the index.

        Use this method to determine valid options for a cell based on the current Group

        Args:
            index: QModelIndex

        Returns:
            List of strings
        """
        parentIdx = index.parent()
        # if the parent is the root, then we are in the first column, and the options are parameter keys of the Group
        if parentIdx == self.invisibleRootItem().index():
            return sorted(self.group.getParameter().keys())

        if index.column() == ParameterBindingModel.targetColumn:
            return sorted(self.group.getChildren(recursive=False).keys())

        if index.column() == ParameterBindingModel.destinationKeyColumn:
            blockItemIndex = index.sibling(index.row(), ParameterBindingModel.targetColumn)
            blockItemName = blockItemIndex.data(QtCore.Qt.DisplayRole)
            try:
                block = self.group.getChild(blockItemName)
                return sorted(block.getParameter().keys())
            except GroupException:
                pass
        return []

    def setGroup(self, group):
        """
        Set the Group for the model

        NOTE: This will clear and reset the model.

        Args:
            group: taskforce_common.task.Group object
        """
        self.group = group
        self.setModelFromGroupBindings()

    def removeBinding(self, idx):
        """
        Remove an existing binding by index

        Args:
            idx: QModelIndex of the binding to remove.

        Raises:
            Exception - The index must be from a row containing a target binding and not the actual group parameter
        """
        if idx.parent() == self.invisibleRootItem().index():
            raise Exception('Bindings must be removed from group parameters')
        self.removeRow(idx.row(), idx.parent())

    def removeLastBinding(self, item):
        """
        Remove the last binding addded to this parameter

        Args:
            item (QStandardItem)
        """
        rows = item.rowCount()
        self.removeRow(rows-1, item.index())

    def setGroupBindingsFromModel(self):
        """
        Set the internal group's bindings to the bindings described in the QStandardItemModel
        """
        self.group.clearBindings()
        for i in range(self.rowCount()):
            parameterItem = self.item(i, 0)
            sourceKey = parameterItem.data(QtCore.Qt.DisplayRole)
            for j in range(parameterItem.rowCount()):
                target = parameterItem.child(j, 0).data(QtCore.Qt.DisplayRole)
                destinationKey = parameterItem.child(j, 1).data(QtCore.Qt.DisplayRole)
                # Don't crash if trying to set placeholder text or bad user input
                try:
                    self.group.bindParameter(sourceKey, destinationKey, target)
                except Exception as e:
                    pass
        self.bindingsChanged.emit()

    def setModelFromGroupBindings(self):
        """
        Set the QStandardItemModel to the bindings stored in the internal Group object
        """
        self.clear()

        if self.group is None:
            return

        # populate the top-level items with the group parameters
        params = self.group.getParameter()
        for paramKey in sorted(params.keys()):
            paramKeyItem = QtGui.QStandardItem(paramKey)
            paramKeyItem.setEditable(False)
            blankItem = QtGui.QStandardItem('')
            blankItem.setEditable(False)
            self.appendRow([paramKeyItem, blankItem])

        # set the model with existing parameter bindings
        parameterBindings = self.group.getParameterBindings()
        for param, bindings in parameterBindings.items():
            item = self._getParameterItem(param)
            if item is not None:
                for binding in sorted(bindings):  # bindings are defined in taskforce_common.task.Group.py
                    targetItem = QtGui.QStandardItem(binding.target)
                    destinationKeyItem = QtGui.QStandardItem(binding.destinationKey)
                    item.appendRow([targetItem, destinationKeyItem])

    def _getParameterItem(self, name):
        """
        Return the item representing the Group's parameter matching

        Args:
            name: name of the parameter as a string

        Returns:
            QStandardItem or None if the item is not found
        """
        for i in range(self.rowCount()):
            item = self.item(i)
            value = item.data(QtCore.Qt.DisplayRole)
            if name == value:
                return item
        return None
