from pyqtlib import QtGui, QtCore
from pyqtlib import PathModel

from taskforce_model import TaskForceColors


class EnginePathModel(PathModel):
    """
    The EnginePathModel merges the PathModel and the EngineModel into a single model.

    When set as the model for a QTreeView, the status of the engine will be visible as a tree.
    """

    def __init__(self, *args, **kwargs):
        super(EnginePathModel, self).__init__(*args, **kwargs)
        self.setHeaderLabels(['task', 'state', 'status', 'threads', 'events', 'class'])
        self.status = {}
        self.lastStatus = {}
        self._engineModel = None

    @property
    def engineModel(self):
        return self._engineModel

    @engineModel.setter
    def engineModel(self, model):
        self._engineModel = model
        self._engineModel.getEngineStatusModel().statusUpdated.connect(self.updateStatus)

    def data(self, QModelIndex, role=None):
        if role == QtCore.Qt.BackgroundColorRole:
            row = QModelIndex.row()
            col = QModelIndex.column()
            if self.getHeaderLabel(col) == 'state':
                text = self.data(QModelIndex, QtCore.Qt.DisplayRole)
                try:
                    color = TaskForceColors.StatusColor[text]
                except KeyError:
                    color = None

                threadsLabelIndex = self.getHeaderLabelIndex('threads')
                if threadsLabelIndex is not None:
                    thisItem = self.itemFromIndex(QModelIndex)
                    parentItem = thisItem.parent()
                    if parentItem:
                        threadsItem = parentItem.child(row, threadsLabelIndex)
                        if threadsItem:
                            threads = threadsItem.data(QtCore.Qt.DisplayRole)
                            if threads != '':
                                color = TaskForceColors.StatusCallbackRunning

                return color
        return PathModel.data(self, QModelIndex, role)

    @QtCore.pyqtSlot()
    def updateStatus(self):
        self.lastStatus = self.status
        self.status = self.engineModel.getEngineStatusModel().getStatus()

        taskRemoved = False
        for block in self.lastStatus.keys():
            if block not in self.status:
                taskRemoved = True

        self.lastStatus = self.status
        if self.status == {} or taskRemoved:
            self.clearData()

        if self.status != {}:
            for block, status in self.status.items():
                if block == "waiting_breakpoints":
                    continue
                if not isinstance(status, dict):
                    self.setValue(block, 'state', 'UNKNOWN')
                    self.setValue(block, 'class', 'UNKNOWN')
                    self.setValue(block, 'status', 'UNKNOWN')
                    self.setValue(block, 'threads', 'UNKNOWN')
                    self.setValue(block, 'events', 'UNKNOWN')
                else:
                    self.setValue(block, 'state', status.get('state', 'UNKNOWN'))
                    self.setValue(block, 'class', status.get('class', 'UNKNOWN'))
                    self.setValue(block, 'status', status.get('customStatus', ''))
                    threadValues = []
                    for thread in status.get('eventThreadStatus', []):
                        callback = thread.get('callback', 'emitStatusEvent')
                        if callback != 'emitStatusEvent':
                            threadValues.append(thread['callback'])
                    self.setValue(block, 'threads', '\n'.join(threadValues))

                    events = []
                    eventQueue = status.get('eventQueue', [])
                    self.setValue(block, 'events', '\n'.join(eventQueue))

    def taskDeploy(self, definition):
        return self.engineModel.taskDeploy(definition)

    def taskGetParameter(self, taskName, key=None):
        return self.engineModel.taskGetParameter(taskName, key)
        
    def taskInit(self, taskName):
        return self.engineModel.taskInit(taskName)

    def taskStatus(self, taskName):
        return self.engineModel.taskStatus(taskName)

    def taskSendCommand(self, taskName, command, *args, **kwargs):
        return self.engineModel.taskSendCommand(taskName, command, *args, **kwargs)

    def taskShutdown(self, taskName, shutdownChildren=True):
        return self.engineModel.taskShutdown(taskName, shutdownChildren)

    def taskShutdownAll(self):
        return self.engineModel.taskShutdownAll()

    def taskSetLogLevel(self, level, taskName=None):
        return self.engineModel.taskSetLogLevel(level, taskName)

    def taskSetParameter(self, taskName, keyValues):
        return self.engineModel.taskSetParameter(taskName, keyValues)

    def taskStartup(self, taskName):
        return self.engineModel.taskStartup(taskName)

    def taskStop(self, taskName):
        return self.engineModel.taskStop(taskName)
