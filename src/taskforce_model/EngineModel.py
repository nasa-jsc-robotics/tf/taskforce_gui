from pyqtlib import QtCore

from taskforce_engine import EngineManager
from taskforce_common import GroupDefinitionLoader
from .library_model import LibraryModel
from taskforce_common import BLOCK_FILE_EXTENSION

import os
import logging

from .EngineStatusModel import EngineStatusModel


class EngineModel(QtCore.QObject):

    engineResponseReceived = QtCore.pyqtSignal(object)

    def __init__(self, clientPort, subscriber, localLibraryRoot=None, parent=None):
        """

        Args:
            clientPort: taskforce_common.connection.Client for sending commands to the Engine
            subscriber: taskforce_common.connection.Subscriber for status info
            localLibraryRoot: Root folder for local TaskForce library
            parent: Qt parent
        """
        super(EngineModel, self).__init__()
        self.engineManager = EngineManager(clientPort)

        self.localLibraryModel = LibraryModel()
        self.localLibraryModel.setNameFilters(['*.py', '*' + BLOCK_FILE_EXTENSION])
        self.localLibraryModel.setNameFilterDisables(False)
        self.localLibraryModel.setResolveSymlinks(True)
        self.localLibraryModel.setReadOnly(False)

        self.engineStatusModel = EngineStatusModel(subscriber)

        # Set the root path last, since this will refresh the root directory, and update the filters.
        if localLibraryRoot is not None:
            self.setLocalLibraryRoot(localLibraryRoot)

        self.logger = logging.getLogger(self.__class__.__name__)

    def addBreakpoint(self, eventName, source, destination, callback):
        return self.engineManager.addBreakpoint(eventName, source, destination, callback)

    def continueBreakpoint(self, eventName, source, destination, args, kwargs):
        return self.engineManager.continueBreakpoint(eventName, source, destination, args, kwargs)

    @QtCore.pyqtSlot(str, str, result=QtCore.QVariant)
    def groupDeploy(self, path, name=None):
        """ Deploy a group to the engine

        Args:
            path: library-relative pathname of the block
            name: Optional filename to apply to the deployed block
        """
        self.logger.info('Parsing block file:{}'.format(path))
        self.localLibraryModel.updateFileMap()
        relative_path = self.localLibraryModel.getRelativePath(path)
        definition = GroupDefinitionLoader.loadDefinition(relative_path, self.localLibraryModel.getFileMap(), name)
        self.logger.info('Deploying block...')
        if not self.synchEngineLibraryWithLocalLibrary():
            self.logger.error("Error synching with engine.  Abort 'blockDeploy'")
            return None
        response = self.engineManager.groupDeploy(definition)
        return response

    def getLocalLibraryModel(self):
        return self.localLibraryModel

    def getEngineStatusModel(self):
        return self.engineStatusModel

    def localLibraryAddLink(self, path):
        """
        Add a top-level symbolic link to the local library

        Args:
            path: path of a folder to link
        """
        package_path = str(path)
        symlink_root = self.localLibraryModel.rootPath()
        symlink_path = os.path.join(symlink_root, os.path.basename(package_path))

        if not os.path.exists(symlink_path):
            os.symlink(package_path, symlink_path)

    def localLibraryRemoveLink(self, path):
        """
        Remove a symbolic link
        Args:
            path:

        Returns:

        """
        raise NotImplementedError("Haven't gotten to this one yet.  Sorry.")

    def engineShutdown(self):
        return self.engineManager.engineShutdown()

    def engineSerialize(self):
        return self.engineManager.engineSerialize()

    def engineSetLogLevel(self, level):
        return self.engineManager.engineSetLogLevel(level)

    def engineSetStatusRate(self, rate):
        return self.engineManager.engineSetStatusRate(rate)

    def engineStatus(self):
        return self.engineManager.engineStatus()

    def engineLibraryAddModule(self, module_name, source_code=None, force_reload=False):
        return self.engineManager.libraryAddModule(module_name, source_code, force_reload)

    def engineLibraryAddModuleFile(self, module_name, file_path, force_reload=False):
        return self.engineManager.libraryAddModuleFile(module_name, file_path, force_reload)

    def engineLibraryClear(self):
        return self.engineManager.libraryClear()

    def engineLibraryGetInfo(self):
        return self.engineManager.libraryGetInfo()

    def engineLibraryGetModulePath(self, module_name):
        return self.engineManager.libraryGetModulePath(module_name)

    def engineLibraryGetModuleHash(self, module_name):
        return self.engineManager.libraryGetModuleHash(module_name)

    def engineLibraryGetModuleInfo(self, module_name):
        return self.engineManager.libraryGetModuleInfo(module_name)

    def engineLibraryRemoveModule(self, module_name):
        return self.engineManager.libraryRemoveModule(module_name)

    def engineLibraryRemovePackage(self, package_name):
        return self.engineManager.libraryRemovePackage(package_name)

    def engineListTasks(self):
        return self.engineManager.listTasks()

    def setLocalLibraryRoot(self, path):
        self.engineManager.clearLocalLibraryFolders()
        for p in os.listdir(path):
            if os.path.isdir(os.path.join(path, p)):
                self.engineManager.addLocalLibraryFolder(os.path.join(path, p))
        self.localLibraryModel.setRootPath(path)

    def shutdown(self):
        self.engineStatusModel.shutdown()

    def synchEngineLibraryWithLocalLibrary(self):
        """Pushes any local library changes to the engine library

        Returns:
            True on success, False on failure

        """
        guiLibraryInfo = self.localLibraryModel.getLibraryInfo()
        engineLibraryInfo = self.engineManager.libraryGetInfo()

        if engineLibraryInfo is None:
            return False

        for moduleName, info in guiLibraryInfo.items():
            if moduleName not in engineLibraryInfo.keys():
                self.engineLibraryAddModuleFile(moduleName, info['path'])
            else:
                engineInfo = engineLibraryInfo[moduleName]
                if info['hash'] != engineInfo['hash']:
                    self.engineLibraryAddModuleFile(moduleName, info['path'], force_reload=True)

        return True

    def taskDeploy(self, definition):
        return self.engineManager.taskDeploy(definition)

    def taskGetParameter(self, taskName, key=None):
        return self.engineManager.taskGetParameter(taskName, key)

    def taskInit(self, taskName):
        return self.engineManager.taskInit(taskName)

    def taskSendCommand(self, taskName, command, args=(), kwargs={}):
        return self.engineManager.taskSendCommand(taskName, command, *args, **kwargs)

    def taskSetLogLevel(self, level, taskName=None):
        return self.engineManager.taskSetLogLevel(level, taskName)

    def taskSetParameter(self, taskName, keyValues):
        return self.engineManager.taskSetParameter(taskName, keyValues)

    def taskShutdown(self, taskName, shutdownChildren=True):
        return self.engineManager.taskShutdown(taskName, shutdownChildren)

    def taskShutdownAll(self):
        return self.engineManager.taskShutdownAll()

    @QtCore.pyqtSlot(str, result=QtCore.QVariant)
    def taskStartup(self, taskName):
        return self.engineManager.taskStartup(taskName)

    @QtCore.pyqtSlot(str, result=QtCore.QVariant)
    def taskStatus(self, taskName):
        return self.engineStatusModel.getStatus(taskName)

    @QtCore.pyqtSlot(str, result=QtCore.QVariant)
    def taskStop(self, taskName):
        return self.engineManager.taskStop(taskName)

    def removeBreakpoint(self, eventName, source, destination, callback):
        return self.engineManager.removeBreakpoint(eventName, source, destination, callback)
