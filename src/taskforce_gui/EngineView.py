from pyqtlib import QtCore, QT_LIB
from pyqtlib import TreeView

from EngineModelContextMenu import EngineModelContextMenu

DEFAULT_COMMANDS = ['startup', 'init', 'stop', 'shutdown']

normal_data_stylesheet = """
    QAbstractScrollArea {
        alternate-background-color: rgb(CCCCCC);
        background-color: white;
        color: black;
        font-style: normal;
        font-size: 12px;
        font-family: "Courier New";
    }
"""

stale_data_stylesheet = """
    QAbstractScrollArea {
        color: gray;
        font-style: italic;
        background-color: yellow;
    }
"""


class EngineView(TreeView):

    """
    The EngineView is expecting it's model to be a subclass of the EnginePathModel.
    """
    taskSelected = QtCore.pyqtSignal(str, object)
    taskDoubleClicked = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super(EngineView, self).__init__(parent)
        self.setStyleSheet(stale_data_stylesheet)

    @QtCore.pyqtSlot(bool)
    def connectionStateChanged(self, connected):
        if connected:
            self.setStyleSheet(normal_data_stylesheet)
        else:
            self.setStyleSheet(stale_data_stylesheet)

    def contextMenuEvent(self, event):
        menu = self.getContextMenu(event)
        menu.exec_(event.globalPos())

    def getContextMenu(self, event):
        taskName = self.getSelectedTask()

        if taskName is None:
            return TreeView.getContextMenu(self, event)

        menu = EngineModelContextMenu(taskName, self.model())
        menu.addSeparator()
        menu.addActions(TreeView.getContextMenu(self, event).actions())
        return menu

    def getSelectedTask(self):
        indexes = self.selectionModel().selection().indexes()

        if len(indexes) > 0:
            item = indexes[0]
            to_return = ""
            while True:
                if item.data(QtCore.Qt.DisplayRole) is None:
                    break
                to_return = item.data(QtCore.Qt.DisplayRole) + to_return
                item = item.parent()
            return to_return
        return None

    def model(self):
        model = super(EngineView, self).model()
        if QT_LIB is 'PyQt4':
            if isinstance(model, QtGui.QAbstractProxyModel):
                model = model.sourceModel()
        else:
            if isinstance(model, QtCore.QAbstractProxyModel):
                model = model.sourceModel()
        return model

    def mouseDoubleClickEvent(self, QMouseEvent):
        taskName = self.getSelectedTask()
        if taskName is not None:
            self.taskDoubleClicked.emit(taskName)

    def selectionChanged(self, QItemSelection, QItemSelection_1):
        taskName = self.getSelectedTask()
        if taskName is not None:
            self.taskSelected.emit(taskName, self.model().taskStatus(taskName))
        return super(EngineView, self).selectionChanged(QItemSelection, QItemSelection_1)
