import json
import logging

from pyqtlib import QtCore, QtGui

from taskforce_model import TaskForceColors
from taskforce_common import TaskState


from .GroupController import EditSubscriptionCommand, GroupController, GroupModel
from .EngineModelContextMenu import EngineModelContextMenu
from .SubscriptionConnection import SubscriptionConnection
from .SubGroupItem import SubGroupItem
from .TaskItem import TaskItem

logger = logging.getLogger(__name__)


class GroupLiveScene(GroupController):

    statusUpdated = QtCore.pyqtSignal()

    def __init__(self, blockName, parent=None):
        super(GroupLiveScene, self).__init__(parent=None)

        self.status = {}
        self.engineModel = None
        self.engineStatusModel = None
        self.blockName = blockName

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateViews)
        self.timer.start(100)

    def addBreakpoint(self, item):
        item.breakpoint = True
        self.undoStack.push(EditSubscriptionCommand(self.model, item.destinationName,
                                                    item.eventName, item.sourceName, item.callback, True, 'Add breakpoint'))
        self.engineModel.addBreakpoint(item.eventName, self.getTaskFullPath(item.sourceName),
                                       self.getTaskFullPath(item.destinationName), item.callback)

    def continueBreakpoint(self, item, args, kwargs):
        """
        Continue from breakpoint

        Args:
            SubscriptionConnection
        """
        self.engineModel.continueBreakpoint(item.eventName, self.getTaskFullPath(item.sourceName), self.getTaskFullPath(item.destinationName),
                                            eval(args), json.loads(kwargs))
        item.waiting_bps.remove((args, kwargs))
        if len(item.waiting_bps) == 0:
            item.waiting = False
        self.updateViews()

    def deleteItems(self, items):
        """
        Delete deployed items

        This method is called in response to the user manually deleting items, usually by selecting one or
        more items and pressing 'delete' on the keyboard.

        For live objects, we currently do not allow this behavior

        Args:
            items: List of graphics items
        """
        logger.warning('Cant delete items while group is deployed')

    def getContextMenu(self, event):
        item = self.getTaskItemAtPoint(event.scenePos())
        if item is None:
            return None
        if isinstance(item, SubscriptionConnection):
            if item.waiting:
                menu = QtGui.QMenu('Subscription')
                for bp in item.waiting_bps:
                    continueBreakpointAction = QtGui.QAction('Continue breakpoint {} {}'.format(bp[0], bp[1]), self,
                                                             triggered=lambda: self.continueBreakpoint(item, bp[0], bp[1]))
                    menu.addAction(continueBreakpointAction)
                return menu
            else:
                return self.getSubscriptionConnectionContextMenu(item, event)
        elif isinstance(item, (TaskItem, SubGroupItem)):
            taskName = self.getNameFromItem(item)
            menu = EngineModelContextMenu(taskName, self.enginePathModel)
            return menu
        else:
            return None

    def getTaskFullPath(self, name):
        if name == str(GroupModel.BlockAlias):
            return self.blockName
        return '/'.join([self.blockName, name])

    def getNameFromItem(self, item):
        name = GroupController.getNameFromItem(self, item)
        return self.getTaskFullPath(name)

    def getTaskRelativePath(self, name):
        split = name.split('/')[-1]
        blockNameSplit = self.blockName.split('/')[-1]
        if split == blockNameSplit:
            return str(GroupModel.BlockAlias)
        return split

    def getTaskParameters(self, taskName):
        if taskName == str(GroupModel.BlockAlias):
            taskName = self.blockName
        status = self.status.get(taskName, None)
        if status is None:
            return None
        return status['parameters']

    def getTaskRelativeName(self, fullName):
        relativeTaskName = fullName.split('/')[-1]
        return relativeTaskName

    def removeBreakpoint(self, item):
        item.breakpoint = False
        self.undoStack.push(EditSubscriptionCommand(self.model, item.destinationName,
                                                    item.eventName, item.sourceName, item.callback, False, 'Remove breakpoint'))
        self.engineModel.removeBreakpoint(item.eventName, self.getTaskFullPath(item.sourceName),
                                          self.getTaskFullPath(item.destinationName), item.callback)

    def setEnginePathModel(self, model):
        self.enginePathModel = model
        self.engineModel = model.engineModel
        self.engineStatusModel = self.engineModel.getEngineStatusModel()
        self.engineStatusModel.statusUpdated.connect(self.updateStatus)
        self.engineStatusModel.connectionStateChanged.connect(self.updateConnectionState)
        self.updateConnectionState()

    def setTaskParameters(self, taskName, parameters):
        if taskName == str(GroupModel.BlockAlias):
            taskName = self.blockName
        self.engineModel.taskSetParameter(taskName, parameters)

    @QtCore.pyqtSlot(bool)
    def updateConnectionState(self, connected=None):
        """
        Slot connected to the engineStatusModel's "connectionStateChanged" signal.

        This method will change the background color based on the connection state.

        Args:
            connected: Current connection state as a boolean.
        """
        if self.engineStatusModel.connected:
            self.setBackgroundBrush(TaskForceColors.BlockLiveViewBackgroundColorConnected)
        else:
            self.setBackgroundBrush(TaskForceColors.BlockLiveViewBackgroundColorDisconnected)

    def updateEvents(self):
        waiting_breakpoints = self.status['waiting_breakpoints']
        for breakpoint in waiting_breakpoints:
            source = self.getTaskRelativePath(breakpoint[2])
            destination = self.getTaskRelativePath(breakpoint[1])
            callback = breakpoint[3]
            name = self.createSubscriptionLookupName(source, destination, breakpoint[0], callback, SubscriptionConnection.__name__)
            if name in self.nameToItem:
                self.nameToItem[name].waiting = True
                self.nameToItem[name].waiting_bps.add((breakpoint[4], breakpoint[5]))

    @QtCore.pyqtSlot()
    def updateStatus(self):
        status = self.engineModel.taskStatus(None)
        if status is None:
            self.status = {}
        else:
            self.status = status
        self.updateBlockModel()
        self.updateEvents()
        self.statusUpdated.emit()

    @QtCore.pyqtSlot()
    def updateViews(self):
        for view in self.taskViews:
            taskName = view.getLinkedTask()
            if taskName is not None:
                fullTaskName = self.getTaskFullPath(taskName)
                status = self.status.get(fullTaskName, None)
                if status is None:
                    status = {'state': TaskState.stateToString[TaskState.NOTDEPLOYED]}
                view.updateView(status)

    def updateBlockModel(self):
        for name, obj in self.model.getTasks().items():
            fullTaskName = self.getTaskFullPath(obj.name)
            status = self.status.get(fullTaskName, None)

            if status is not None:
                obj.setStatus(status)
            else:
                status = {'state': None}
            self.updateTaskStatus(obj.name, status)
