from pyqtlib import QtGui, QtCore
import os

from pyqtlib import FileSystemView

from taskforce_common.utils import getSourceCode, saveJsonFile
from taskforce_model import GroupModel
from taskforce_common import BLOCK_FILE_EXTENSION

TASK_TEMPLATE = 'taskforce_common.task.task_template'
TEMPLATE_TASK_NAME = 'TEMPLATE_TASK_NAME'

class LibraryView(FileSystemView):

    deployBlock = QtCore.pyqtSignal(str, str)   # block path, block name
    openFile = QtCore.pyqtSignal(str)           # file path
    openFileAsText = QtCore.pyqtSignal(str)     # file path
    uploadModule = QtCore.pyqtSignal(str, str)  # modulename, file path

    def __init__(self, parent=None):
        super(LibraryView, self).__init__(parent)

        self.openFileAction = QtGui.QAction('Open', self, triggered=self.openSelectedFile)
        self.openFileAction.setEnabled(False)
        self.openFileAsTextAction = QtGui.QAction('Open as text', self, triggered=self.openSelectedFileAsText)
        self.openFileAsTextAction.setEnabled(False)

        self.newGroupAction = QtGui.QAction('New Group', self, triggered=self.createNewGroupDialog)

        self.newTaskFromDefaultTemplateAction = QtGui.QAction('New Task (default template)', self, triggered=self.newTaskFromDefaultTemplate)
        self.newTaskChooseTemplateDialogAction = QtGui.QAction('New Task (choose template)', self, triggered=self.newTaskChooseTemplateDialog)
        self.newTaskEmptyAction = QtGui.QAction('New Task (empty)', self, triggered=self.newTaskEmpty)

        self.deployAction   = QtGui.QAction('Deploy', self, triggered=self.deploySelected)
        self.deployAction.setEnabled(False)
        self.deployAsAction = QtGui.QAction('Deploy As...', self, triggered=self.deployAsDialog)
        self.deployAsAction.setEnabled(False)

        self.uploadAction = QtGui.QAction('Upload to engine', self, triggered=self.uploadSelectedToEngine)

        self.fileActionGroup.addAction(self.openFileAction)
        self.fileActionGroup.addAction(self.openFileAsTextAction)
        self.fileActionGroup.addAction(self.newGroupAction)

        self.newTaskActionGroup = QtGui.QActionGroup(self)
        self.newTaskActionGroup.addAction(self.newTaskFromDefaultTemplateAction)
        self.newTaskActionGroup.addAction(self.newTaskChooseTemplateDialogAction)
        self.newTaskActionGroup.addAction(self.newTaskEmptyAction)

        self.engineActionGroup = QtGui.QActionGroup(self)
        self.engineActionGroup.addAction(self.deployAction)
        self.engineActionGroup.addAction(self.deployAsAction)
        self.engineActionGroup.addAction(self.uploadAction)

        self.fileDoubleClicked.connect(self.openSelectedFile)

    def createNewGroupDialog(self):
        """
        Prompt the user with an QInputDialog to create a new group file.

        If a new file is created, the 'openFile' signal is emitted
        """
        indexes = self.selectedIndexes()
        root = self.model().rootPath()
        if len(indexes) > 0:
            index = indexes[0]
            root = self.model().filePath(index)

        # Get the block name from the user
        filename, ok = QtGui.QInputDialog.getText(self, 'Group name', 'name')
        if ok:
            filename = str(filename)
            if not filename.endswith(BLOCK_FILE_EXTENSION):
                filename += BLOCK_FILE_EXTENSION

            # calculate the paths
            path = os.path.join(root, filename)
            relative_path = self.model().getRelativePath(path)
            name = os.path.basename(relative_path)

            if name.endswith(BLOCK_FILE_EXTENSION):
                name = name.rsplit('.', 1)[0]

            model = GroupModel(name, relative_path)

            data = model.serialize()
            saveJsonFile(path,data)

            self.openFile.emit(path)

    def createNewTaskDialog(self):
        """
        Prompt the user with an QInputDialog to create a new Task

        Returns:
            Tuple of (TaskName, Path) as strings
        """
        indexes = self.selectedIndexes()
        root = self.model().rootPath()
        if len(indexes) > 0:
            index = indexes[0]
            root = self.model().filePath(index)

        filename, ok = QtGui.QInputDialog.getText(self, 'Task name', 'name')
        if ok:
            taskname = os.path.basename(str(filename))
            filename = str(filename)
            if not filename.endswith('.py'):
                filename += '.py'
            if taskname.endswith('.py'):
                taskname = taskname.rsplit('.',1)[0]
            path = os.path.join(root, filename)
            f_ptr = open(path, 'w')
            f_ptr.close()
            return taskname, path
        return None

    def contextMenuEvent(self, event):
        """
        Context menu event.

        Args:
            event:
        """
        menu = QtGui.QMenu(self)
        menu.addActions(self.getEngineMenu().actions())
        menu.addSeparator()
        menu.addActions(self.getViewMenu().actions())
        menu.addSeparator()
        menu.addActions(self.getFileMenu().actions())
        menu.exec_(event.globalPos())

    def deploySelected(self, name=None):
        index = self.selectedIndexes()[0]
        if index is not None:
            path = self.model().filePath(index)

            if name is None or name is False:
                name = os.path.basename(path)
                name = name.split(BLOCK_FILE_EXTENSION)[0]

            if path != None:
                self.deployBlock.emit(path, name)

    def deployAsDialog(self):
        index = self.selectedIndexes()[0]
        if index is not None:
            path = self.model().filePath(index)
            name, ok = QtGui.QInputDialog.getText(self, 'Deploy as...', 'Input deployment name')
            self.deploySelected(name)

    def getEngineMenu(self):
        menu = QtGui.QMenu()
        menu.addActions(self.engineActionGroup.actions())
        return menu

    def getNewMenu(self):
        menu = QtGui.QMenu('New', self)
        menu.addAction(self.newGroupAction)
        menu.addActions(self.newTaskActionGroup.actions())
        menu.addSeparator()
        menu.addAction(self.newFolderAction)
        menu.addAction(self.newFileAction)
        return menu

    def getFileMenu(self):
        menu = QtGui.QMenu('File', self)
        menu.addAction(self.openFileAction)
        menu.addAction(self.openFileAsTextAction)
        menu.addMenu(self.getNewMenu())
        menu.addAction(self.addLinkDialogAction)
        menu.addAction(self.unlinkAction)
        menu.addSeparator()
        menu.addAction(self.deleteAction)
        return menu

    def newTaskChooseTemplateDialog(self):
        taskname, path = self.createNewTaskDialog()
        if path is None:
            return

        template_path, filter = QtGui.QFileDialog.getOpenFileNameAndFilter(self, caption='Select Template')
        if template_path is None:
            return

        with open(str(template_path), 'r') as f_ptr:
            template_code = f_ptr.read()
        f_ptr.close()
        template_code = template_code.replace(TEMPLATE_TASK_NAME, taskname)
        with open(path, 'w') as f_ptr:
            f_ptr.write(template_code)
        f_ptr.close()
        self.openFile.emit(path)

    def newTaskEmpty(self):
        taskname, path = self.createNewTaskDialog()
        if path is None:
            return
        self.openFile.emit(path)

    def newTaskFromDefaultTemplate(self):
        taskname, path = self.createNewTaskDialog()
        if path is None:
            return
        source = getSourceCode(TASK_TEMPLATE)
        source = source.replace(TEMPLATE_TASK_NAME, taskname)
        with open(path, 'w') as f_ptr:
            f_ptr.write(source)
        f_ptr.close()
        self.openFile.emit(path)

    def openSelectedFile(self):
        index = self.selectedIndexes()[0]
        path = self.model().filePath(index)
        self.openFile.emit(os.path.realpath(path))

    def openSelectedFileAsText(self):
        index = self.selectedIndexes()[0]
        path = self.model().filePath(index)
        self.openFileAsText.emit(os.path.realpath(path))

    def selectionChanged(self, QItemSelection, QItemSelection_1):
        FileSystemView.selectionChanged(self, QItemSelection, QItemSelection_1)

        self.deployAction.setEnabled(False)
        self.deployAsAction.setEnabled(False)
        self.openFileAction.setEnabled(False)
        self.openFileAsTextAction.setEnabled(False)

        self.newGroupAction.setEnabled(True)
        self.newTaskActionGroup.setEnabled(True)

        indexes = self.selectedIndexes()
        if len(indexes) > 0:
            index = indexes[0]
            if not self.model().isDir(index):
                self.openFileAction.setEnabled(True)
                self.openFileAsTextAction.setEnabled(True)
                self.newGroupAction.setEnabled(False)
                self.newTaskActionGroup.setEnabled(False)
                if self.model().filePath(index).endswith(BLOCK_FILE_EXTENSION):
                    self.deployAction.setEnabled(True)
                    self.deployAsAction.setEnabled(True)

    def uploadSelectedToEngine(self):
        indexes = self.selectedIndexes()
        if len(indexes) == 0:
            path = str(self.model().rootPath())
        else:
            path = self.model().filePath(indexes[0])

        for root, dirs, files in os.walk(path, followlinks=True):
            for file in files:
                if file.endswith('.py') and file != "__init__.py":
                    filePath = os.path.realpath(os.path.join(root, file))
                    relative_path = self.model().getRelativePath(filePath)
                    moduleName = relative_path.replace('/', '.').split('.py')[0]
                    self.uploadModule.emit(moduleName, filePath)
