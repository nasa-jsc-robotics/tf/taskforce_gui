from pyqtlib import QtGui, QtCore

from pyqtlib import DictionaryDialog, ComboBoxInputWidget
from pyqtlib.node_graph import Element


class TaskView(Element):

    """
    A TaskView is a visual element that can be placed in the scene within the Block Editor window.  Once placed in
    the scene, the View must be "linked" to a Task.  A View can only be linked to a single Task.

    A custom view can be created by sub-classing this method.  Then, the subclass should overload the folloing methods:
    boundingRect - sets the bounding rectangle of the view
    updateView - called periodically to refresh the view

    If any additional information is needed for serializing the view, the "serialize" method can be overloaded.
    """

    def __init__(self, name=None, parent=None):
        """
        Args:
            taskNames: list-of-strings of linked task names
            parent: parent graphics item
        """
        super(TaskView, self).__init__(name, parent)
        self.linkedTask = None

    def boundingRect(self):
        """
        By default, the actual view class does not have a size.  If there are children items, we still don't
        need to have a size for the view class.

        Returns:
            QRectF
        """
        return QtCore.QRectF(0.0, 0.0, 0.0, 0.0)

    def contextMenuEvent(self, event):
        event.accept()
        menu = self.getContextMenu(event)
        menu.exec_(event.screenPos())

    def getContextMenu(self, event):
        """
        Create and return the contextMenu.

        Returns:
            QMenu
        """
        menu = QtGui.QMenu('Link')
        menu.addAction('Link to Task', self.linkTaskDialog)
        menu.addAction('Unlink task', self.unlinkTask)
        return menu

    def getLinkedTask(self):
        """
        Get the name of the linked Task

        Returns:
            Task name as a string
        """
        return self.linkedTask

    def linkTaskDialog(self):
        """
        Link task dialog box.

        Calling this method will present the user with a modal dialog box allowing the user to link this view with a
        task.
        """
        modelObjectDictionary = self.scene().model.getBlocks()
        taskNames = sorted(modelObjectDictionary.keys())

        taskComboBox = QtGui.QComboBox()
        taskComboBox.addItems(taskNames)

        dialog = DictionaryDialog('Link to Task')
        dialog.addSetting('current link', default_value=self.getLinkedTask())
        dialog.addSetting('task', ComboBoxInputWidget(comboBox=taskComboBox))

        if dialog.exec_() == QtGui.QDialog.Accepted:
            data = dialog.getData()
            taskName = str(data['task'])
            self.setLinkedTask(taskName)

    def serialize(self):
        """
        Serialize information about this view into a Python dictionary.

        Sub-classes should overload this method and fill-in any extra information that it needs when serializing
        the view.  All keys and values must be serializeable into a JSON object.

        Returns:
            Dictionary
        """
        args = ()
        kwargs = {}
        linkedTask = self.getLinkedTask()
        position = (self.scenePos().x(), self.scenePos().y())
        definition = {
            'module': self.__module__,
            'class': self.__class__.__name__,
            'args': args,
            'kwargs': kwargs,
            'linkedTask': linkedTask,
            'position': position
        }
        return definition

    def setLinkedTask(self, name):
        """
        Set the Task associated with this view.

        Args:
            name: Name of the linked task.
        """
        self.linkedTask = name
        item = self.scene().getItemFromName(name)
        pos = self.mapFromScene(item.scenePos())
        self.setParentItem(item)
        self.setPos(-pos.x(), -pos.y())

    @QtCore.pyqtSlot()
    def updateView(self):
        """
        Slot to be connected to a status update signal.

        Should be overloaded by sub-class

        Args:
            name: name of the task
            status: status dictionary of the task
        """
        pass

    def unlinkTask(self):
        self.linkedTask = None
        pos = self.scenePos()
        self.setParentItem(None)
        self.setPos(pos)
