from pyqtlib import QtGui, QtCore


class FileToolBar(QtGui.QToolBar):

    openFile = QtCore.pyqtSignal()
    saveFile = QtCore.pyqtSignal()
    saveFileAs = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(FileToolBar, self).__init__(parent)
        self.setOrientation(QtCore.Qt.Horizontal)

        openFileIcon = QtGui.QIcon.fromTheme("document-open")
        saveFileIcon = QtGui.QIcon.fromTheme("document-save")
        saveFileAsIcon = QtGui.QIcon.fromTheme("document-save-as")

        self.openFileAction = QtGui.QAction(openFileIcon, "&Open", self)
        self.openFileAction.triggered.connect(self.openFile)

        self.saveFileAction = QtGui.QAction(saveFileIcon, "&Save", self)
        self.saveFileAction.triggered.connect(self.saveFile)

        self.saveFileAsAction = QtGui.QAction(saveFileAsIcon, "S&ave As...", self)
        self.saveFileAsAction.triggered.connect(self.saveFileAs)

        self.addAction(self.openFileAction)
        self.addAction(self.saveFileAction)
        self.addAction(self.saveFileAsAction)
