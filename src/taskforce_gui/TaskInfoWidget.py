from pyqtlib import QtGui, QtCore
from pyqtlib import ObjectTreeWidget


class TaskInfoWidget(QtGui.QWidget):

    def __init__(self, parent=None):
        """
        A widget to display information about a task

        @type parent QWidget
        @param parent Parent widget
        """
        super(TaskInfoWidget, self).__init__(parent)
        self.objectTreeWidget = ObjectTreeWidget()

        self.layout = QtGui.QVBoxLayout()
        self.layout.addWidget(self.objectTreeWidget)
        self.setLayout(self.layout)
        self.show()

    @QtCore.pyqtSlot(str, object)
    def updateView(self, name, status):
        """
        Update the view with the status of the object

        @type name string
        @param name Name of the task

        @type status dictionary
        @param status The status of the task
        """
        self.objectTreeWidget.clear()
        self.objectTreeWidget.updateView(name, status)
        self.objectTreeWidget.expandAll()
        self.objectTreeWidget.adjustColumns()
