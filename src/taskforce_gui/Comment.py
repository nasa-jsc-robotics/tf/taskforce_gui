from pyqtlib import QtCore
from pyqtlib.node_graph import TextItem


class Comment(TextItem):

    textEditedUndo = QtCore.pyqtSignal(object, str, str)

    def __init__(self, text, pos=(0, 0)):
        super(Comment, self).__init__(text, pos=pos)
        self.setEditable(False)

    def focusOutEvent(self, event):
        """
        Overload the focusOutEvent to emit a signal with the current comment to properly undo text changes

        Args:
            event: QFocusEvent
        """
        self.setTextInteraction(False)
        super(Comment, self).focusOutEvent(event)

        if self.text() != self.preFocusText:
            self.textEditedUndo.emit(self, self.preFocusText, self.text())

    def mouseDoubleClickEvent(self, event):
        """
        Overload mouseDoubleClickEvent so that it will trigger the editor

        Args:
            event: QGraphicsSceneMouseEvent or QMouseEvent
        """
        if self.textInteractionFlags() == QtCore.Qt.TextEditorInteraction:
            # if editor mode is already on, pass double click events on to the editor
            # NOTE: This does not work right now because a double click is
            # of type QMouseEvent. QGraphicsMouseSceneEvent has no public constructor in PyQt5
            # super(Comment, self).mouseDoubleClickEvent(event)
            return

        # turn editor mode on
        self.setTextInteraction(True)

        # NOTE: This does not work right now because a double click is
        # of type QMouseEvent. QGraphicsMouseSceneEvent has no public constructor in PyQt5
        # click = QtGui.QGraphicsSceneMouseEvent(QtCore.QEvent.GraphicsSceneMousePress)
        # click.setButton(event.button())
        # click.setPos(event.pos())
        # self.mousePressEvent(click)

    def setTextInteraction(self, on):
        """
        Helper function to manually turn on and off editing the text interactions

        Args:
            on (bool)
        """
        if on and self.textInteractionFlags() == QtCore.Qt.NoTextInteraction:
            self.setEditable(True)
            # manually do what a mouse click does
            self.setFocus(QtCore.Qt.MouseFocusReason)
            self.setSelected(True)

        elif not on and self.textInteractionFlags() == QtCore.Qt.TextEditorInteraction:
            # turn off editor mode:
            self.setEditable(False)

            # deselect text (else it keeps gray shade):
            c = self.textCursor()
            c.clearSelection()
            self.setTextCursor(c)
            self.clearFocus()
