from pyqtlib import QtGui, QtCore
import os

LOCAL_DIR = os.path.dirname(__file__)

# map of logical name to icon filename
ICONS_MAP = {
	'taskforce_logo': 'taskforce_logo.png',
	'undo_icon' : 'undo12.png',
	'redo_icon' : 'redo11.png',
	'connection_tool_icon' : 'connection.png',
	'select_tool_icon' : 'selectTool.png',
	'comment_icon': 'comment.png'
	}


def get_taskforce_logo():
	return QtGui.QIcon(os.path.join(LOCAL_DIR, ICONS_MAP['taskforce_logo']))

def get_undo_icon():
	return QtGui.QIcon(os.path.join(LOCAL_DIR, ICONS_MAP['undo_icon']))

def get_redo_icon():
	return QtGui.QIcon(os.path.join(LOCAL_DIR, ICONS_MAP['redo_icon']))

def get_connection_tool_icon():
	return QtGui.QIcon(os.path.join(LOCAL_DIR, ICONS_MAP['connection_tool_icon']))

def get_selection_tool_icon():
	return QtGui.QIcon(os.path.join(LOCAL_DIR, ICONS_MAP['select_tool_icon']))
	
def get_comment_icon():
	return QtGui.QIcon(os.path.join(LOCAL_DIR, ICONS_MAP['comment_icon']))
