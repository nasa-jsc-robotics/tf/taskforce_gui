import os

from .GroupLiveScene import GroupLiveScene
from .GroupEditor import GroupEditor
from .BlockItem import BlockItem
from .EngineLibraryWidget import EngineLibraryWidget
from .EngineView import EngineView
from .LibraryView import LibraryView
from .ParameterBindingWidget import ParameterBindingWidget
from .ParameterWidget import ParameterWidget
from .TaskInfoWidget import TaskInfoWidget
from .TaskView import TaskView
from .TextEditor import TextEditor
from .UndoToolbar import UndoToolbar

def getResourceDirectory():
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), 'resources')
