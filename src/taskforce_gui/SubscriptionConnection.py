from pyqtlib import QtCore
from pyqtlib.node_graph import MultiStyleConnection


class SubscriptionConnection(MultiStyleConnection):
    """
    SubscriptionConnection is a line that is drawn in the scene to represent a subscription between two tasks.
    """
    ConnectionStyle = MultiStyleConnection.ConnectionStyle

    def __init__(self, destinationName, eventName, sourceName, callback, breakpoint, style=ConnectionStyle.Line, parent=None):
        super(SubscriptionConnection, self).__init__(parent=parent, style=style)
        self.destinationName = destinationName
        self.eventName = eventName
        self.sourceName = sourceName
        self.callback = callback
        self._breakpoint = breakpoint
        self._waiting = False
        self._breakpoint_color = QtCore.Qt.red
        self.waiting_bps = set()

    @property
    def breakpoint(self):
        return self._breakpoint

    @breakpoint.setter
    def breakpoint(self, value):
        self._breakpoint = value
        self.update()

    @property
    def waiting(self):
        return self._waiting

    @waiting.setter
    def waiting(self, value):
        if (value == self.waiting):
            return
        self._waiting = value
        if self.waiting:
            self._breakpoint_color = QtCore.Qt.yellow
        else:
            self._breakpoint_color = QtCore.Qt.red
        self.update()

    def paint(self, painter, option, widget):
        super(SubscriptionConnection, self).paint(painter, option, widget)
        if self.breakpoint:
            breakpoint_pos = self.getPathMidpoint()
            painter.fillRect(breakpoint_pos.x()-7.5, breakpoint_pos.y()-7.5, 15, 15, self._breakpoint_color)

    def setNamesFromLookupString(self, value):
        split_value = value.split(',')
        try:
            self.setDestinationName(split_value[0])
            self.setSourceName(split_value[1])
            self.setEventName(split_value[2])
            self.setCallback(split_value[3])
        except Exception:
            pass

    def setDestinationName(self, value):
        self.destinationName = value

    def setEventName(self, value):
        self.eventName = value

    def setSourceName(self, value):
        self.sourceName = value

    def setCallback(self, value):
        self.callback = value

    def setWaiting(self):
        self.breakpoint_color = QtCore.Qt.yellow
        self.update()
