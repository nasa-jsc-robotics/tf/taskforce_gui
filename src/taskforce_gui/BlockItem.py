from pyqtlib import QtCore
from pyqtlib.node_graph import BoxNode, ProvidesPort, RequiresPort, PortOrientation

from taskforce_model import TaskForceColors


class SignalObject(QtCore.QObject):
    """
    This class serves as a proxy for sending signals.
    """
    signal = QtCore.pyqtSignal(object, str, str)

    def __init__(self):
        super(SignalObject, self).__init__()

    def getSignal(self):
        return self.signal


class BlockItem(BoxNode):
    """
    This is the basic rounding box item that represents a Task or SubBlock in a GraphScene.
    """

    def __init__(self, instanceName, typeName, x=0, y=0, width=100, height=100, defaultColor=TaskForceColors.TaskBlockDefaultColor, parent=None):
        """
        Args:
            instanceName:  Instance of this class/type.  This text is displayed just below the item.
                           This value is editable by the user.
            typeName: class or type of this item.  This text is displayed at the top of the box.
                      This value is NOT editable by the user.
            x: x position of the block
            y: y position of the block
            width: width of the block
            height: height of the block
            parent: parent QtGraphicsItem
        """
        BoxNode.__init__(self, instanceName, typeName, parent=parent)

        self.instanceTextItem.textEdited.connect(self.emitInstanceNameEdited)
        self.instanceTextItem.textEdited.connect(self.emitInstanceNameChanged)
        self.instanceNameEditedObject = SignalObject()
        self.instanceNameChangedObject = SignalObject()

        self._defaultColor = defaultColor
        self.color = defaultColor

        self.status = {}
        self.connectable = True

    def connect(self, item):
        pass

    def getSourcePort(self, name):
        try:
            port = self.getPort(name)
        except KeyError:
            port = ProvidesPort(name, orientation=PortOrientation.Right)
            self.addPort(port)
        return port

    def getDestinationPort(self, name):
        try:
            port = self.getPort(name)
        except KeyError:
            port = RequiresPort(name, orientation=PortOrientation.Left)
            self.addPort(port)
        return port

    @property
    def defaultColor(self):
        return self._defaultColor

    @defaultColor.setter
    def defaultColor(self, value):
        self._defaultColor = value

    def emitInstanceNameChanged(self, oldName, newName):
        self.instanceNameChanged.emit(self, oldName, newName)

    def emitInstanceNameEdited(self, oldName, newName):
        """
        Helper method to emit a signal when the instance name has been changed.

        This slot is connected to the TextItem's 'textChanged' signal.

        :param oldName: name before the change
        :param newName: name after the change
        """
        self.instanceNameEdited.emit(self, oldName, newName)

    @property
    def instanceNameEdited(self):
        """
        Signal emitted when the instanceName text edit has been edited manually by the user.

        Returns the signal from the SignalObject.

        By setting the 'instanceNameEdited' method as a property, we can use new-style signal connection:
            self.blockItem.nameChanged.connect(....)
        :return: signal nameChanged(QGraphicsItem, oldName, newName)
        """
        return self.instanceNameEditedObject.getSignal()

    @property
    def instanceNameChanged(self):
        return self.instanceNameChangedObject.getSignal()

    @BoxNode.instanceName.setter
    def instanceName(self, value):
        """
        Set the 'instanceName' of the block

        Args:
            value: new value as a string
        """
        oldValue = self.instanceTextItem.text()
        self.instanceTextItem.setText(value)

        if oldValue != value:
            self.instanceNameChanged.emit(self, oldValue, value)

    def updatePath(self):
        super(BlockItem, self).updatePath()
        self.connectPoint = QtCore.QPointF(0.0, self.height/2.)

    def updateStatus(self, status):
        self.status = status
        self.updateColorStatus()

    def updateColorStatus(self):
        state = self.status.get('state', None)

        if state is None:
            newColor = TaskForceColors.StatusUnknown
        elif state in ['NOTDEPLOYED', 'DEPLOYED']:
            newColor = self.defaultColor
        else:
            newColor = TaskForceColors.StatusColor[state]

        eventThreadStatus = self.status.get('eventThreadStatus', [])
        for thread in eventThreadStatus:
            if thread.get('callback', 'emitStatusEvent') != 'emitStatusEvent':
                newColor = TaskForceColors.StatusCallbackRunning

        if newColor is not None and newColor != self.color:
            self.color = newColor
