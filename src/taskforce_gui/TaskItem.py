from taskforce_model import TaskForceColors

from .BlockItem import BlockItem


class TaskItem(BlockItem):
    """
    The TaskItem is a rounded square to represent a sub-block.
    """
    def __init__(self, instanceName, typeName):
        BlockItem.__init__(self, instanceName, typeName, defaultColor=TaskForceColors.TaskBlockDefaultColor)
        self.connectable = True
