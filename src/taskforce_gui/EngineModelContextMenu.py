import logging

from pyqtlib import QtGui, QtCore
from pyqtlib import WorkerThread

from TaskCommandDialog import TaskCommandDialog


class EngineModelContextMenu(QtGui.QMenu):

    """
    Convenience Menu class to perform common actions on a class.
    """

    def __init__(self, taskName, enginePathModel, title='Engine', parent=None):
        """
        Args:
            taskName: Name of the task as a string
            enginePathModel: An instance of an EnginePathModel
            title: Title of the menu.  Default is 'Engine'
            parent: parent item
        """
        QtGui.QMenu.__init__(self, title, parent)

        self.enginePathModel = enginePathModel
        self.engineModel = self.enginePathModel.engineModel
        self.engineStatusModel = self.engineModel.getEngineStatusModel()

        ##########
        # Actions
        ##########
        self.startupTaskAction = QtGui.QAction('&startup', self, triggered=lambda : self.enginePathModel.taskStartup(taskName))
        self.initTaskAction = QtGui.QAction('&init', self, triggered=lambda : self.enginePathModel.taskInit(taskName))
        self.stopTaskAction = QtGui.QAction('s&top', self, triggered=lambda : self.stopTaskAndSubTasks(taskName))
        self.shutdownTaskAction = QtGui.QAction('sh&utdown', self, triggered=lambda : self.enginePathModel.taskShutdown(taskName, False))
        self.shutdownTaskAndSubtasksAction = QtGui.QAction('shut&down (and subtasks)', self, triggered=lambda : self.enginePathModel.taskShutdown(taskName, True))
        self.sendCommandDialogAction = QtGui.QAction('send command...', self, triggered= lambda : self.sendCommandDialog(taskName))

        self.shutdownTaskConfirmAction = QtGui.QAction('sh&utdown', self, triggered=lambda : self.confirmAction(self.shutdownTaskAction))
        self.shutdownTaskAndSubtasksConfirmAction = QtGui.QAction('shut&down (and subtasks)', self, triggered=lambda: self.confirmAction(self.shutdownTaskAndSubtasksAction))

        # self.setBreakPointAction = QtGui.QAction('set &breakpoint', self, triggered=lambda : self.enginePathModel.taskSetBreakPoint(taskName, True))
        # self.clearBreakPointAction = QtGui.QAction('clear &breakpoint', self, triggered=lambda : self.enginePathModel.taskSetBreakPoint(taskName, False))
        # self.continueFromBreakPointAction = QtGui.QAction('&continue breakpoint', self, triggered=lambda :self.taskSendCommand.emit(taskName, 'continueFromBreakPoint', [], {}))
        # self.clearEventQueueAction = QtGui.QAction('clear event queue', self, triggered=lambda : self.taskSendCommand.emit(taskName, 'clearEventQueue', [], {}))

        self.addAction(self.startupTaskAction)
        self.addAction(self.initTaskAction)
        self.addAction(self.stopTaskAction)
        self.addAction(self.shutdownTaskConfirmAction)
        self.addAction(self.shutdownTaskAndSubtasksConfirmAction)
        self.addSeparator()
        self.addMenu(self.getSetLogLevelMenu(taskName))
        self.addSeparator()
        self.addAction(self.sendCommandDialogAction)
        self.addMenu(self.getEmitEventMenu(taskName))
        self.addSeparator()
        #self.addAction(self.getBreakPointAction(taskName))
        #self.addAction(self.continueFromBreakPointAction)
        #self.addAction(self.clearEventQueueAction)

        # if (len(self.enginePathModel.taskStatus(taskName)['eventQueue']) == 0):
        #     self.continueFromBreakPointAction.setEnabled(False)
        #     self.clearEventQueueAction.setEnabled(False)

    def confirmAction(self, action):
        title = 'Are you sure?'
        msg   = 'Do you want to send command:\n{}'.format(action.text().replace('&',''))
        if QtGui.QMessageBox.critical(self, title, msg, buttons=QtGui.QMessageBox.Cancel|QtGui.QMessageBox.Ok, defaultButton=QtGui.QMessageBox.Cancel) == QtGui.QMessageBox.Ok:
            action.trigger()

    def getBreakPointAction(self, taskName):
        breakPoint = self.enginePathModel.taskStatus(taskName)['breakPoint']
        if breakPoint:
            return self.clearBreakPointAction
        else:
            return self.setBreakPointAction

    def getEmitEventMenu(self, taskName):
        menu = QtGui.QMenu('emit event', self)
        status = self.enginePathModel.taskStatus(taskName)
        if status is None:
            return menu
        eventNames = status['emitEventNames']

        for name in eventNames:
            # Here, we can iterate through the signals and create a lambda to call when
            # the action is triggered.
            # In the lambda, the "x" is a throw-away variable that gets set by the caller.
            # The "event=name" provides a variable local to the lambda, otherwise it would get
            # overwritten as the for-loop iterates.

            emitEvent = lambda x, event=name: self.enginePathModel.taskSendCommand(taskName, 'emit', [event], {})
            menu.addAction(QtGui.QAction(name, self, triggered=emitEvent))
        return menu

    def getSetLogLevelMenu(self, taskName):
        status = self.enginePathModel.taskStatus(taskName)
        if status is None:
            logLevel = 0
        else:
            logLevel = status['logLevel']
        logActions = {}

        # setLogLevel actions
        logActions[logging.DEBUG] = QtGui.QAction('&Debug', self, triggered=lambda : self.enginePathModel.taskSetLogLevel('DEBUG', taskName))
        logActions[logging.INFO] = QtGui.QAction('&Info', self, triggered=lambda : self.enginePathModel.taskSetLogLevel('INFO', taskName))
        logActions[logging.WARNING] = QtGui.QAction('&Warning', self, triggered=lambda : self.enginePathModel.taskSetLogLevel('WARNING', taskName))
        logActions[logging.ERROR] = QtGui.QAction('&Error', self, triggered=lambda : self.enginePathModel.taskSetLogLevel('ERROR', taskName))

        self.setLogLevelActionGroup = QtGui.QActionGroup(self)
        self.setLogLevelActionGroup.setExclusive(True)
        self.setLogLevelActionGroup.addAction(logActions[logging.DEBUG])
        self.setLogLevelActionGroup.addAction(logActions[logging.INFO])
        self.setLogLevelActionGroup.addAction(logActions[logging.WARNING])
        self.setLogLevelActionGroup.addAction(logActions[logging.ERROR])

        for action in self.setLogLevelActionGroup.actions():
            action.setCheckable(True)

        action = logActions.get(logLevel, None)
        if action is not None:
            action.setChecked(True)

        # setLogLevelMenu
        self.setLogLevelMenu = QtGui.QMenu('Log Level')
        self.setLogLevelMenu.addActions(self.setLogLevelActionGroup.actions())

        return self.setLogLevelMenu

    def sendCommandDialog(self, taskName):
        commandList = self.enginePathModel.taskStatus(taskName)['commands']
        dialog = TaskCommandDialog(taskName, commandList)
        if dialog.exec_() == QtGui.QDialog.Accepted:
            command = dialog.getMethodName()
            args = dialog.getArgs()
            # NOTE: commands that trigger long callbacks will cause the GUI to hang when used here
            self.enginePathModel.taskSendCommand(taskName, command, args)

    def stopTaskAndSubTasks(self, task):
        tasks = [task] + self.enginePathModel.getChildPaths(task, leaf_only=False)
        self.stopWorker = WorkerThread(self.stopTaskAndSubTasksInWorkerThread, tasks)
        self.stopWorker.start()

    def stopTaskAndSubTasksInWorkerThread(self, tasks):
        for task in tasks:
            self.enginePathModel.taskStop(task)
