from pyqtlib import QtCore, QtGui

from pyqtlib import DictionaryWidget

class ParameterWidget(QtGui.QWidget):

    getParametersRequest = QtCore.pyqtSignal(str)
    setParametersRequest = QtCore.pyqtSignal(str, dict)

    def __init__(self, parent=None):
        super(ParameterWidget, self).__init__(parent)

        self.dictionary = DictionaryWidget()
        self.getButton = QtGui.QPushButton('get')
        self.setButton = QtGui.QPushButton('set')
        self.taskName = None
        self.taskLabel = QtGui.QLabel()

        self.setButton.clicked.connect(self.sendParameters)
        self.getButton.clicked.connect(self.requestParameters)

        buttonLayout = QtGui.QHBoxLayout()
        buttonLayout.addWidget(self.getButton)
        buttonLayout.addWidget(self.setButton)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.dictionary)
        layout.addWidget(self.taskLabel)
        layout.addLayout(buttonLayout)

        self.setLayout(layout)

    def clear(self):
        self.dictionary.clear()
        self.taskLabel.setText('')

    def setParameters(self, taskName, parameters):
        self.taskName = taskName
        self.taskLabel.setText(self.taskName)
        self.dictionary.setDictionary(parameters)

    def getParameters(self):
        return self.dictionary.getDictionary()

    def sendParameters(self):
        parameters = self.getParameters()
        if isinstance(parameters, dict):
            self.setParametersRequest.emit(self.taskName, parameters)

    def requestParameters(self):
        self.getParametersRequest.emit(self.taskName)
