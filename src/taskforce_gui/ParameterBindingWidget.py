from pyqtlib import QtCore, QtGui

from taskforce_model import ParameterBindingModel


class AddParameterBindingCommand(QtGui.QUndoCommand):
    """
    QUndo Command for adding a parameter binding
    """
    def __init__(self, model, item, description):
        """
        Args:
            model (ParameterBindingModel)
            item (QStandardItem)
            description(str)
        """
        super(AddParameterBindingCommand, self).__init__(description)
        self.model = model
        self.item = item

    def redo(self):
        self.model.addBinding(self.item)

    def undo(self):
        self.model.removeLastBinding(self.item)


class EditParameterBindingCommand(QtGui.QUndoCommand):
    """
    QUndo Command for editing a parameter binding
    """
    def __init__(self, model, idx, preFocusText, postFocusText, description):
        """
        Args:
            model (ParameterBindingModel)
            idx (QStandardItem)
            preFocusText (str)
            postFocusTest (str)
            description (str)
        """
        super(EditParameterBindingCommand, self).__init__(description)
        self.model = model
        self.idx = idx
        self.preFocusText = preFocusText
        self.postFocusText = postFocusText

    def redo(self):
        self.model.item(self.idx.parent().row(), 0).setChild(self.idx.row(), self.idx.column(), QtGui.QStandardItem(self.postFocusText))

    def undo(self):
        self.model.item(self.idx.parent().row(), 0).setChild(self.idx.row(), self.idx.column(), QtGui.QStandardItem(self.preFocusText))


class RemoveParameterBindingCommand(QtGui.QUndoCommand):
    """
    QUndo Command for removing a parameter binding
    """
    def __init__(self, model, idx, description):
        super(RemoveParameterBindingCommand, self).__init__(description)
        self.model = model
        self.idx = idx
        self.target = ""
        self.destinationKey = ""

    def redo(self):
        self.target = self.idx.data(QtCore.Qt.DisplayRole)
        self.destinationKey = self.model.item(self.idx.parent().row(), ParameterBindingModel.targetColumn).child(self.idx.row(), 1).data(QtCore.Qt.DisplayRole)
        self.model.removeBinding(self.idx)
        self.model.setGroupBindingsFromModel()

    def undo(self):
        self.model.insertBinding(self.idx, self.target, self.destinationKey)
        self.model.setGroupBindingsFromModel()


class ParameterComboBox(QtGui.QComboBox):
    """
    Helper class for the combo boxes in order to properly implement undo redo functionality
    """
    optionChanged = QtCore.pyqtSignal(object, object, str, str)

    def __init__(self, model, idx, parent=None):
        super(ParameterComboBox, self).__init__(parent)
        self.preFocusText = self.currentText()
        self.idx = idx
        self.model = model

    def focusInEvent(self, event):
        self.preFocusText = self.currentText()
        super(ParameterComboBox, self).focusInEvent(event)

    def focusOutEvent(self, event):
        super(ParameterComboBox, self).focusOutEvent(event)
        postFocusText = self.currentText()

        if postFocusText != self.preFocusText:
            self.optionChanged.emit(self.model, self.idx, self.preFocusText, postFocusText)


class ParameterBindingDelegate(QtGui.QStyledItemDelegate):

    def __init__(self, parent=None):
        super(ParameterBindingDelegate, self).__init__(parent)
        self.undoStack = None

    def createEditor(self, parent, option, index):
        """

        Args:
            parent: parent item as a QWidget
            option: option as a QStyleOptionViewItem
            index: QModelIndex

        Returns:
            QtGui.QComboBox
        """
        model = index.model()
        comboBoxOptions = model.getOptions(index)
        if len(comboBoxOptions) > 0:
            comboBox = ParameterComboBox(model, index, parent)
            comboBox.addItems(comboBoxOptions)
            comboBox.optionChanged.connect(self.undoEditParameter)
            return comboBox
        return QtGui.QStyledItemDelegate.createEditor(self, parent, option, index)

    @QtCore.pyqtSlot(object, object, str, str)
    def undoEditParameter(self, model, idx, preFocusText, postFocusTest):
        self.undoStack.push(EditParameterBindingCommand(model, idx, preFocusText, postFocusTest, "Changed {} to {} in binding".format(preFocusText, postFocusTest)))

    def updateEditorGeometry(self, editor, option, index):
        editor.setGeometry(option.rect)

    def setUndoStack(self, undo_stack):
        """
        Set the relevant undo stack for the group controller that is active

        Args:
            undo_stack (QUndoStack)
        """
        self.undoStack = undo_stack


class ParameterBindingWidget(QtGui.QWidget):

    def __init__(self, parent=None):
        super(ParameterBindingWidget, self).__init__(parent)

        self.undoStack = None
        self.model = ParameterBindingModel()
        self.view = QtGui.QTreeView()

        self.delegate = ParameterBindingDelegate()
        self.view.setItemDelegate(self.delegate)
        self.view.setModel(self.model)

        self.addButton = QtGui.QPushButton('add')
        self.removeButton = QtGui.QPushButton('remove')
        self.addButton.clicked.connect(self.addBindingToSelected)
        self.removeButton.clicked.connect(self.removeSelectedBinding)

        buttonLayout = QtGui.QHBoxLayout()

        buttonLayout.addWidget(self.addButton)
        buttonLayout.addWidget(self.removeButton)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.view)
        layout.addLayout(buttonLayout)
        self.setLayout(layout)

    @QtCore.pyqtSlot()
    def addBindingToSelected(self):
        """
        Add a parameter binding to the currently selected parameter.

        This slot is connected to the "add" button.
        """
        indices = self.view.selectedIndexes()
        if len(indices) > 0:
            idx = indices[0]

            item = self.model.item(idx.row())
            if item is not None:
                self.undoStack.push(AddParameterBindingCommand(self.model, item, "Add parameter binding to {}".format(item.data(QtCore.Qt.DisplayRole))))

    @QtCore.pyqtSlot()
    def removeSelectedBinding(self):
        """
        Remove the currently selected binding

        This slot is connected to the "remove" button.
        """
        indices = self.view.selectedIndexes()
        if len(indices) > 0:
            idx = indices[0]
            if idx.parent() == self.model.invisibleRootItem().index():
                return
            self.undoStack.push(RemoveParameterBindingCommand(self.model, idx, "Remove parameter binding from {} to {}".format(idx.parent().data(QtCore.Qt.DisplayRole), idx.data(QtCore.Qt.DisplayRole))))

    def setGroup(self, group):
        """
        Set the Group for this widget

        Args:
            group (Group)
        """
        self.model.setGroup(group)

    def setUndoStack(self, undo_stack):
        """
        Set the relevant undo stack for the group controller that is active

        Args:
            undo_stack (QUndoStack)
        """
        self.undoStack = undo_stack
        self.delegate.setUndoStack(undo_stack)

    def refreshGroup(self):
        """
        Refresh the widget to adjust for changes in group parameters
        """
        self.model.setModelFromGroupBindings()
