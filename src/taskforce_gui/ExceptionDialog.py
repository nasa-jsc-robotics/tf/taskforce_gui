from pyqtlib import QtGui

import sys
import traceback


def getExceptionDialog(title='Unhandled Exception', message='An unhandled exception occured:'):
    exc_type, exc_value, exc_traceback = sys.exc_info()

    trace = traceback.format_exception(exc_type, exc_value,
                                       exc_traceback)
    msg = message
    msg += ''.join(trace)
    return QtGui.QMessageBox.critical(None, title, msg)
