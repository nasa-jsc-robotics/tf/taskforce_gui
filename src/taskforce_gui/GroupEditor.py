from pyqtlib import QtGui, QtCore
from pyqtlib import DictionaryDialog

import os
import logging

from pyqtlib.node_graph import Element, GraphView

from taskforce_model import GroupModel
from taskforce_common.utils import createObject, openJsonFile, saveJsonFile, getSourceCodeFileName, reload_module_by_name
from taskforce_common import BLOCK_FILE_EXTENSION
from taskforce_gui.GroupController import GroupController

from taskforce_gui.GroupToolBar import GroupToolBar
from taskforce_gui.FileToolBar import FileToolBar
from taskforce_gui.UndoToolbar import UndoToolbar

from .TaskItem import TaskItem
from .SubGroupItem import SubGroupItem

logger = logging.getLogger(__name__)


class GroupEditor(QtGui.QWidget):
    """
    This widget is a self-contained editor for creating / editing TaskForce blocks.
    """

    ##############
    # Signals:
    modifiedChanged = QtCore.pyqtSignal(bool)    # emited when the "modified" state of the block has changed.
    sceneChanged = QtCore.pyqtSignal(object)  # emited when something in the scene has changed
    titleUpdated = QtCore.pyqtSignal(str)     # emited when the title of the block has changed

    selectionChanged = QtCore.pyqtSignal()

    # emits the full pathname of the Task
    doubleClickedTask = QtCore.pyqtSignal(str)  # emitted when a Task in the scene has been double-clicked

    # emits the relative pathname of the SubBlock
    doubleClickedSubBlock = QtCore.pyqtSignal(str, str) # emitted when a SubBlock in the scene has been double-clicked

    # Signals (end)
    ################

    # Class Attributes
    SceneFileExtension = '.scene'
    BlockFileExtension = BLOCK_FILE_EXTENSION

    def __init__(self, relativePath='NewBlock' + BlockFileExtension, rootPath='.', title=None, controller=None, parent=None):
        """
        Args:
            relativePath: Relative path name for this block, as a string.
            rootPath: Root path of the location of this block, as a string
            title: optional title for this block, as a string.  If None, the title will be created using the relativePath
            parent: parent object
        """
        super(GroupEditor, self).__init__(parent=parent)

        # Settings
        APPLICATION = QtCore.QCoreApplication.applicationName()
        ORGANIZATION = QtCore.QCoreApplication.organizationName()
        self.settings = QtCore.QSettings(APPLICATION, ORGANIZATION)

        self.neverSaved = True

        self.model = GroupModel('name', relativePath)
        self.view = GraphView(self)
        self.view.itemDoubleClicked.connect(self.itemDoubleClicked)

        self.saveFileAction = QtGui.QAction('Save', self, triggered=self.saveBlock)
        self.saveFileAction.setShortcut(QtGui.QKeySequence("Ctrl+S"))
        self.saveFileAsAction = QtGui.QAction('Save file as...', self, triggered=self.saveBlockAs)

        self.addAction(self.saveFileAction)
        self.addAction(self.saveFileAsAction)

        #############
        #  Tool bar
        #############
        # File Toolbar
        fileToolBar = FileToolBar()
        fileToolBar.openFile.connect(self.openBlockDialog)
        fileToolBar.saveFile.connect(self.saveFileAction.trigger)
        fileToolBar.saveFileAs.connect(self.saveFileAsAction.trigger)

        # Block Toolbar
        self.blockToolBar = GroupToolBar()

        # Undo/Redo toolbar
        self.savedUndoIndex = 0
        self.undoToolbar = UndoToolbar()

        self.toolbar = QtGui.QToolBar()
        self.toolbar.addWidget(fileToolBar)
        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.blockToolBar)
        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.undoToolbar)
        #############
        #  Tool bar (end)
        #############

        self.blockFileName = None
        self.sceneFileName = None
        self.rootPath = rootPath
        self.setPathsAndName(os.path.abspath(os.path.join(os.path.dirname(rootPath), relativePath)))

        self.modifiedState = False
        self.title = title
        self.updateTitle()

        if controller is None:
            controller = GroupController(rootPath=self.rootPath)
        self.setController(controller)

        self.modifiedChanged.connect(self.updateTitle)

        ########
        # Layout
        ########
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.view)
        self.setLayout(layout)

    def addSubBlockDialog(self):
        """
        Add a SubBlock using a user-dialog.
        """
        dialog = DictionaryDialog()
        dialog.addSetting('name',default_value='TestSubBlock')
        dialog.addSetting('file_name', default_value='task_example_library/cow/herd' + GroupEditor.BlockFileExtension)
        if dialog.exec_() == QtGui.QDialog.Accepted:
            data = dialog.getData()
            blockTask = createObject('taskforce_common.task.Group', 'Group', args=(str(data['name']),), kwargs={'fileName':str(data['file_name'])})
            self.controller.addSubBlock(blockTask)
            return True
        else:
            return False

    def addTaskDialog(self):
        """
        Add a Task using a user-dialog
        """
        dialog = DictionaryDialog()
        dialog.addSetting('name', default_value='mable')
        dialog.addSetting('module', default_value='taskforce_example_library.cow.Cowtime')
        if dialog.exec_() == QtGui.QDialog.Accepted:
            data = dialog.getData()
            data['className'] = data['module'].split('.')[-1]
            task = createObject(data['module'], data['className'], args=(data['name'],))
            self.controller.addTask(task)
            return True
        else:
            return False

    def closeEvent(self, event):
        """
        Handle a closeEvent.

        This allows for one last, "Do you want to save your work?"

        Args:
            event: event
        """
        if self.saveChangesDialog():
            event.accept()
            return

        event.ignore()
        return

    def emitSceneChanged(self, objects):
        """
        Emit the sceneChanged signal.  This is connected to the underlying scene.

        Args:
            objects: list of graphics items
        """
        self.sceneChanged.emit(objects)

    def getGroupName(self):
        """
        Get the name of the Group represented in the editor's canvas

        Returns:
            Name of the Group as a string
        """
        return self.model.name

    def getSearchDirectory(self):
        """
        Get the search directory stored in the settings.

        This is useful for load/save dialog actions.

        Returns: Path to the directory as a string.

        """
        if not self.settings.contains('block_file_search_folder'):
            directory = '/home/{}'.format(os.environ['USER'])
            self.settings.setValue('block_file_search_folder',directory)
        return self.settings.value('block_file_search_folder')

    def getSelectedTaskName(self):
        return self.controller.getSelectedTaskName()

    def getSelectedTaskNameAndParameters(self):
        """
        Get the current selected Task's name and parameters.

        Returns:
            A tuple of (name, parameters) where name is a string, and parameters is a Python dictionary
        """
        name = self.getSelectedTaskName()
        if name is None:
            name = self.model.BlockAlias

        parameters = self.controller.getTaskParameters(name)
        return name, parameters

    def getGroup(self):
        """
        Get the Group instance being displayed

        Returns: Group instance
        """
        return self.model.getBlocks(self.model.BlockAlias)

    def getTask(self, name):
        """
        Get a Task instance from the model by name.

        Args:
            name: name of the task as a string

        Returns: Task instance
        """
        return self.model.getChild(name)

    def getTaskParameters(self, name):
        return self.controller.getTaskParameters(name)

    def getUndoStack(self):
        """
        Get the UndoStack

        Returns: QUndoStack object
        """
        return self.controller.getUndoStack()

    def isModified(self):
        """
        Returns whether or not the Block has been modified since the last save,

        Returns: Bool (True if modified, otherwise False
        """
        return self.modifiedState

    def itemDoubleClicked(self, item):
        """
        Slot for when an item in the scene has been double-clicked.

        This method will potentially emit either "doubleClikedTask", or "doubleClickedSubBlock", based on
        what was double-clicked.

        Args:
            item: Graphical item that was double-clicked
        """
        name = self.controller.getNameFromItem(item)

        if isinstance(item, TaskItem):
            name = name.split('/')[-1]
            obj = self.model.getChild(name)
            logger.info('Double clicked Task:{} ({})'.format(name, obj.module))
            fileName = getSourceCodeFileName(obj.module)
            self.doubleClickedTask.emit(fileName)

        elif isinstance(item, SubGroupItem):
            name = name.split('/')[-1]
            obj = self.model.getChild(name)
            fileName = obj.fileName
            logger.info('Double clicked Sub-block:{} ({}) title:{}'.format(name, fileName, self.title))
            name = self.controller.getNameFromItem(item)
            self.doubleClickedSubBlock.emit(obj.fileName, name)

    def openBlock(self, blockFileName):
        """
        Open a block file by filename.

        This will deserialize the block file and populate the scene and model.

        Args:
            blockFileName: Filename of the blockfile as a string.

        Returns:
            True on success, otherwise False
        """

        # first, try to open the file
        try:
            modelDefinition = openJsonFile(blockFileName)
        except Exception as e:
            logger.error('Error opening file "{}":{}'.format(blockFileName, e))
            return False

        if modelDefinition is None:
            logger.error('Error opening file "{}": definition is empty'.format(blockFileName))
            return False

        # If the model file is good, check for unsaved changes
        self.saveChangesDialog()

        # next, setup the paths
        self.setPathsAndName(blockFileName)

        # if the viewfile is gone or corrupted, just load the block without the view definition
        # This may not place the items in a nice way, but it's better than nothing.
        try:
            viewDefinition = openJsonFile(self.sceneFileName)
        except Exception:
            viewDefinition = None

        self.controller.deserialize(modelDefinition, viewDefinition=viewDefinition)
        self.controller.getUndoStack().clear()
        self.savedUndoIndex = self.controller.getUndoStack().index()
        self.updateModifiedState(self.savedUndoIndex)
        self.neverSaved = False
        return True

    def openBlockDialog(self):
        """
        Open a Block File using a OpenFileDialog.
        """
        # check for unsaved changes
        if self.saveChangesDialog():
            searchPath = self.getSearchDirectory()
            filename, filter = QtGui.QFileDialog.getOpenFileNameAndFilter(caption='Open Block file', directory=searchPath, filter='*{}'.format(GroupEditor.BlockFileExtension))
            if filename:
                self.setSearchDirectory(os.path.dirname(filename))
                self.openBlock(str(filename))

    def reloadModelPackages(self):
        """
        Clear all modules that are in the model from the import cache (sys.modules)
        """
        model, view = self.serialize()

        # objectDictionary = self.model.getObjectDictionary()
        objectDictionary = self.model.getChildren()
        moduleNames = set([obj.module for obj in objectDictionary.values()])

        for name in moduleNames:
            reload_module_by_name(name)

        self.controller.deserialize(model, view)

    def saveBlock(self):
        """
        Save the Block and View to a block file and a scene file.
        """
        if self.neverSaved:
            self.saveBlockAs()
            return

        # force refresh of the file name
        blockName = self.relativePath.split('/')[-1].split('.')[0]
        self.model.setFileName(str(self.relativePath))
        self.model.setName(blockName)

        saveJsonFile(self.blockFileName, self.controller.serializeModel())
        saveJsonFile(self.sceneFileName, self.controller.serializeView())

        logger.info('Saving file {}'.format(self.blockFileName))
        logger.info('Saving file {}'.format(self.sceneFileName))
        self.savedUndoIndex = self.controller.undoStack.index()
        self.updateModifiedState(self.savedUndoIndex)

        return True

    def saveBlockAs(self):
        """
        Save the block using a SaveFileDialog.
        """
        rootPath = QtGui.QFileDialog.getExistingDirectory(caption='choose library folder', directory=self.rootPath)
        if rootPath:
            try:
                filename, filter = QtGui.QFileDialog.getSaveFileNameAndFilter(caption='Save Block file as...', directory=rootPath, filter='*{}'.format(GroupEditor.BlockFileExtension))
            except Exception:
                filename, filter = QtGui.QFileDialog.getSaveFileName(caption='Save Block file as...', directory=rootPath, filter='*{}'.format(GroupEditor.BlockFileExtension))

            if filename:
                self.setPathsAndName(blockFilePath=filename, rootPath=rootPath)
                self.neverSaved = False
                self.saveBlock()
                return True
        return False

    def saveChangesDialog(self):
        """
        This method will pop-up a dialog if there are unsaved changes.

        If the users selects 'Save', the block will be saved, and this method returns 'True'.
        If the user selects 'Discard', the block will NOT be saved, and the method returns 'True'.
        If the user selects 'Cancel', the block will NOT be saved, and the method returns 'False',

        If there are no unsaved changes, this method returns 'True'
        Returns: bool
        """
        if self.isModified():
            msgBox = QtGui.QMessageBox()
            msgBox.setText("There are unsaved changes.")
            msgBox.setInformativeText("Do you want to save your changes?")
            msgBox.setStandardButtons(QtGui.QMessageBox.Save | QtGui.QMessageBox.Discard | QtGui.QMessageBox.Cancel)
            msgBox.setDefaultButton(QtGui.QMessageBox.Cancel)
            ret = msgBox.exec_()

            if ret == QtGui.QMessageBox.Save:
                self.saveBlock()
                return True
            elif ret == QtGui.QMessageBox.Discard:
                return True
            elif ret == QtGui.QMessageBox.Cancel:
                return False
        return True

    def serialize(self):
        """
        Return the serialization of the model and the view.

        Returns:
            Tuple of (modelDefinition, sceneDefinition)
        """
        return self.controller.serialize()

    def serializeBlock(self):
        """
        Return the serialization of the model.
        Returns:
            modelDefinition as a Python Dictionary
        """
        return self.controller.serializeModel()

    def serializeScene(self):
        """
        Return the serialized view.

        Returns:
            viewDefinition as a Python Dictionary.
        """
        return self.controller.serializeView()

    def setPathsAndName(self, blockFilePath, rootPath=None):
        """
        Set the local path-related attributes based on the full path to a .group file

        Args:
            file_path: full path to a target file
            root_path: If not none, this will set self.root_path
        """
        if not blockFilePath.endswith(GroupEditor.BlockFileExtension):
            blockFilePath = blockFilePath + GroupEditor.BlockFileExtension

        if rootPath:
            self.rootPath = rootPath

        self.relativePath = str(os.path.relpath(blockFilePath, os.path.dirname(self.rootPath)))
        self.blockFileName = str(blockFilePath)
        self.sceneFileName = str(blockFilePath + GroupEditor.SceneFileExtension)
        blockName = self.relativePath.split('/')[-1].split('.')[0]

        self.model.setFileName(str(self.relativePath))
        self.model.setName(blockName)

    def setSearchDirectory(self, directory):
        """
        Set the 'block_file_search_folder' setting.

        Args:
            directory: Search file directory as a string.
        """
        self.settings.setValue('block_file_search_folder',directory)

    def setTaskParameters(self, taskName, parameters):
        self.controller.setTaskParameters(taskName, parameters)

    def setSelectedTaskParameters(self, parameters):
        """
        Set the parameters of the currently selected Task/SubBlock

        Args:
            parameters: Parameters as a Python dictionary
        """
        name = self.getSelectedTaskName()
        if name is not None:
            self.controller.setTaskParameters(name, parameters)

    def setController(self, controller):
        if not isinstance(controller, GroupController):
            raise Exception('Trying to set controller to non-GroupController type: {}'.format(type(controller)))
        self.controller = controller
        self.controller.setParent(self)
        self.controller.setModel(self.model)
        self.controller.changed.connect(self.emitSceneChanged)
        self.controller.selectionChanged.connect(self.selectionChanged)

        self.undoToolbar.undo.connect(self.controller.undo)
        self.undoToolbar.redo.connect(self.controller.redo)
        self.blockToolBar.pointerSelected.connect(self.controller.setMouseModePointer)
        self.blockToolBar.connectSelected.connect(self.controller.setMouseModeConnect)
        self.blockToolBar.commentSelected.connect(self.controller.setMouseModeComment)
        self.blockToolBar.connectionStyleSelected.connect(self.controller.setConnectionStyle)
        self.controller.getUndoStack().canRedoChanged.connect(self.undoToolbar.setEnableRedo)
        self.controller.getUndoStack().canUndoChanged.connect(self.undoToolbar.setEnableUndo)
        self.controller.getUndoStack().indexChanged.connect(self.updateModifiedState)
        self.controller.getUndoStack().clear()
        self.savedUndoIndex = self.controller.getUndoStack().index()

        self.view.setScene(self.controller)

    def updateModifiedState(self, undoIndex):
        """
        Check whether the modified state has changed
        """
        old_state = self.modifiedState
        new_state = not self.savedUndoIndex == undoIndex

        self.modifiedState = new_state
        if old_state != new_state:
            self.modifiedChanged.emit(new_state)

    def updateTitle(self):
        """
        Update the title.  This will emit the "titleUpdated" signal.
        """
        if self.title:
            name = self.title
        else:
            name = self.model.getFileName()

        if self.isModified():
            self.setWindowTitle(name + '*')
        else:
            self.setWindowTitle(name)

        self.titleUpdated.emit(self.windowTitle())

    @QtCore.pyqtSlot()
    def _getSelectedParametersRequestHandler(self):
        """
        Private slot connected to the parameter widget's 'getSelectedParametersRequest'.  Sets the parameter widget
        with the parameter values of the currently selected Task/SubBlock
        """
        name, parameters = self.getSelectedTaskNameAndParameters()
        self.parameterWidget.setParameters(name, parameters)
