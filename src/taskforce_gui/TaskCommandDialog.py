from pyqtlib import QtGui, QtCore

import re


class TaskCommandDialog(QtGui.QDialog):

    """
    The TaskCommandDialog is a convenience dialog that can be used to construct a command to send to a Task.

    Construct the Dialog by passing the name of the task and a list of commands available.

    The user then chooses the method from a combo box and fills in the values for other arguments.

    When the dialog returns, the selected method name can be retrieved by calling "getMethodName", and the values
     for the arguments can be retrieved from "getArgs".
    """

    cmd_re_name = '([^(]*)'
    cmd_re_args = '\(([^)]*)'
    cmd_regex = cmd_re_name + cmd_re_args

    def __init__(self, taskName, commandList, parent=None):
        """
        Args:
            taskName: name of the task as a string
            commandList: list of commands available.  This will be a list of commands of the form: methodName(self, arg1, arg2, kwarg1=val)
            parent: parent item
        """
        super(TaskCommandDialog, self).__init__(parent=parent)

        self.setWindowTitle('Send command to "{}"'.format(taskName))
        self.args = []
        self.comboBox = QtGui.QComboBox()
        self.argsLayout = QtGui.QFormLayout()
        self.argWidgets = []

        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        self.commands = {}
        self.cmd_rg = re.compile(self.cmd_regex)
        self.updateCommandList(commandList)
        self.comboBox.currentIndexChanged.connect(self.updateArgs)
        self.updateArgs(0)

        self.mainLayout = QtGui.QVBoxLayout()
        self.mainLayout.addWidget(self.comboBox)
        self.mainLayout.addLayout(self.argsLayout)
        self.mainLayout.addWidget(buttonBox)
        self.setLayout(self.mainLayout)

    def getArgs(self):
        """
        Get the values of the arguments
        Returns:
            Argument values as a list
        """
        args = []
        for widget in self.argWidgets:
            args.append(widget.text())
        return args

    def getMethodName(self):
        """
        Get the name of the selected method.

        Returns:
            Method name as a string
        """
        return self.comboBox.currentText()

    @QtCore.pyqtSlot(str)
    def updateArgs(self, methodIndex):
        """
        Slot connected to the combo box.  This will force the line edit widgets to update to reflect the arguments
        for the chosen method.

        Args:
            methodIndex: index of the chosen method.  NOTE: This value is not used, but necessary due to the signal from
                         the combo box
        """
        self.argWidgets = []

        for cnt in reversed(range(self.argsLayout.count())):
            widget = self.argsLayout.takeAt(cnt).widget()
            if widget is not None:
                widget.deleteLater()
        self.argsLayout.update()

        methodName = str(self.comboBox.currentText())
        argList = self.commands[methodName]
        self.args = []
        for arg in argList:
            value = None
            if '=' in arg:
                arg, value = arg.split('=')
            widget = QtGui.QLineEdit()
            widget.setText(value)
            self.argWidgets.append(widget)
            self.argsLayout.addRow(arg, widget)
            self.args.append(arg)

    @QtCore.pyqtSlot(list)
    def updateCommandList(self, commandList):
        self.commands = {}
        for cmd in commandList:
            match = self.cmd_rg.match(cmd)
            if match:
                methodName = match.group(1)
                argList = match.group(2).split(', ')
                if 'self' in argList:
                    argList.remove('self')
                self.commands[methodName] = argList
        self.comboBox.clear()
        self.comboBox.addItems(self.commands.keys())