from pyqtlib import QtGui, QtCore

import taskforce_gui.resources.taskforce_icons as taskforce_icons


class UndoToolbar(QtGui.QToolBar):
    """
    Generic Undo / Redo tool bar.

    To use the buttons on this toolbar, simply connect the "undo"
    and "redo" signals to the undo() and redo() methods (respectively) of a
    QtGui.QUndoStack instance.  By connecting a QUndoStack's canUndoChanged()
    and canRedoChanged() to this method's setEnableUndo() and setEnableRedo()
    (respectively), the buttons with get disabled/enabled based on the state of
    stack.
    """

    undo = QtCore.pyqtSignal()
    redo = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(UndoToolbar, self).__init__(parent)
        self.setOrientation(QtCore.Qt.Horizontal)

        redoIcon = taskforce_icons.get_redo_icon()
        undoIcon = taskforce_icons.get_undo_icon()

        self.redoAction = QtGui.QAction(redoIcon, '&Redo', self)
        self.redoAction.setShortcutContext(QtCore.Qt.WidgetWithChildrenShortcut)
        self.redoAction.setShortcuts([QtGui.QKeySequence.Redo])
        self.redoAction.triggered.connect(self.redo.emit)

        self.undoAction = QtGui.QAction(undoIcon, '&Undo', self)
        self.undoAction.setShortcutContext(QtCore.Qt.WidgetWithChildrenShortcut)
        self.undoAction.setShortcuts([QtGui.QKeySequence.Undo])
        self.undoAction.triggered.connect(self.undo.emit)

        self.addAction(self.undoAction)
        self.addAction(self.redoAction)

    def setEnableRedo(self, enable):
        """
        Set the enable state of the redo action/button

        @type enable bool
        @parameter enable Desired state of the redo action/button
        """
        self.redoAction.setEnabled(enable)

    def setEnableUndo(self, enable):
        """
        Set the enable state of the undo action/button

        @type enable bool
        @parameter enable Desired state of the undo action/button
        """
        self.undoAction.setEnabled(enable)
