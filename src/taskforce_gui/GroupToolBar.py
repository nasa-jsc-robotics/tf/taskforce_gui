from pyqtlib import QtCore, QtGui

import taskforce_gui.resources.taskforce_icons as taskforce_icons
from pyqtlib.node_graph import MultiStyleConnection


class GroupToolBar(QtGui.QToolBar):

    pointerSelected = QtCore.pyqtSignal()
    connectSelected = QtCore.pyqtSignal()
    commentSelected = QtCore.pyqtSignal()

    # Values are in pyqtlib.node_graph.MultiSytleConnection.ConnectionStyle
    connectionStyleSelected = QtCore.pyqtSignal(int)

    def __init__(self, parent=None):
        super(GroupToolBar, self).__init__(parent)
        self.setOrientation(QtCore.Qt.Horizontal)

        selectIcon = taskforce_icons.get_selection_tool_icon()
        connectIcon = taskforce_icons.get_connection_tool_icon()
        commentIcon = taskforce_icons.get_comment_icon()

        # Action group is needed to make selections exclusive
        # which is set by default
        self.modeActionGroup = QtGui.QActionGroup(self)
        pointerAction = QtGui.QAction(selectIcon, 'Select', self.modeActionGroup)
        pointerAction.triggered.connect(self.pointerSelected.emit)
        pointerAction.setCheckable(True)
        connectAction = QtGui.QAction(connectIcon, 'Connect', self.modeActionGroup)
        connectAction.triggered.connect(self.connectSelected.emit)
        connectAction.setCheckable(True)
        pointerAction.setChecked(True)
        commentAction = QtGui.QAction(commentIcon, 'Comment', self.modeActionGroup)
        commentAction.triggered.connect(self.commentSelected.emit)
        commentAction.setCheckable(True)

        self.connectionStyleButtonGroup = QtGui.QButtonGroup()
        self.connectionStyleButtonGroup.setExclusive(True)

        lineConnectionButton = QtGui.QPushButton('Line')
        lineConnectionButton.setCheckable(True)
        lineConnectionButton.clicked.connect(self.emitLineConnectionStyle)
        lineConnectionButton.setChecked(True)
        orthoConnectionButton = QtGui.QPushButton('OrthoLine')
        orthoConnectionButton.setCheckable(True)
        orthoConnectionButton.clicked.connect(self.emitOrthoLineConnectionStyle)
        splineConnectionButton = QtGui.QPushButton('Spline')
        splineConnectionButton.setCheckable(True)
        splineConnectionButton.clicked.connect(self.emitSplineConnectionStyle)

        self.connectionStyleButtonGroup.addButton(lineConnectionButton)
        self.connectionStyleButtonGroup.addButton(orthoConnectionButton)
        self.connectionStyleButtonGroup.addButton(splineConnectionButton)

        self.addAction(pointerAction)
        self.addAction(connectAction)
        self.addAction(commentAction)
        self.addSeparator()
        self.addWidget(lineConnectionButton)
        self.addWidget(orthoConnectionButton)
        self.addWidget(splineConnectionButton)

    def emitLineConnectionStyle(self):
        self.connectionStyleSelected.emit(MultiStyleConnection.ConnectionStyle.Line)

    def emitOrthoLineConnectionStyle(self):
        self.connectionStyleSelected.emit(MultiStyleConnection.ConnectionStyle.OrthoLine)

    def emitSplineConnectionStyle(self):
        self.connectionStyleSelected.emit(MultiStyleConnection.ConnectionStyle.Spline)
