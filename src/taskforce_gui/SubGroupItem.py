from taskforce_model import TaskForceColors
from taskforce_common import BLOCK_FILE_EXTENSION
from .BlockItem import BlockItem


class SubGroupItem(BlockItem):
    """
    The SubGroupItem is a rounded square to represent a sub-block.
    """
    def __init__(self, instanceName, fileName):
        BlockItem.__init__(self, instanceName, fileName, defaultColor=TaskForceColors.GroupBlockDefaultColor)
        self.typeName = (fileName.split('/')[-1]).split(BLOCK_FILE_EXTENSION)[0]
        self.connectable = True

    def updateColorStatus(self):
        if self.status.get('state', None) is None:
            newColor = TaskForceColors.StatusUnknown
        else:
            newColor = self.defaultColor

        if self.color != newColor:
            self.color = newColor
