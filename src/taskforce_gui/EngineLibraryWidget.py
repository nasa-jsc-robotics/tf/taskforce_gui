from pyqtlib import QtGui, QtCore

from pyqtlib import PathWidget, WorkerThread


class EngineLibraryWidget(QtGui.QWidget):
    def __init__(self, engineInterface, parent=None):
        super(EngineLibraryWidget, self).__init__(parent)

        self.pathWidget = PathWidget(delimiters='.')
        self.pathWidget.setHeaderLabels(['item', 'hash'])
        self.pathWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.pathWidget.customContextMenuRequested.connect(self.showContextMenu)

        self.removeModuleAction = QtGui.QAction('&Remove Module', self)
        self.removeModuleAction.triggered.connect(self.removeModule)

        self.removePackageAction = QtGui.QAction('&Remove Package', self)
        self.removePackageAction.triggered.connect(self.removePackage)

        self.refreshButton = QtGui.QPushButton('Refresh')
        self.refreshButton.clicked.connect(self.refresh)
        self.engineInterface = engineInterface
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.pathWidget)
        layout.addWidget(self.refreshButton)
        self.setLayout(layout)
        self.refresh()
        # self.show()

    def removePackage(self):
        package = self.pathWidget.getSelectedPath()
        self.engineInterface.libraryRemovePackage(package)
        self.refresh()

    def removeModule(self):
        module = self.pathWidget.getSelectedPath()
        self.engineInterface.libraryRemoveModule(module)

    def showContextMenu(self, qpoint):
        base_menu = self.pathWidget.getContextMenu()
        base_menu.setTitle('&View')

        libraryInfo = self.pathWidget.getData(skip_empty=True)
        selectedPath = self.pathWidget.getSelectedPath()

        menu = QtGui.QMenu()

        # if the selected path has data, it is a module.  Otherwise, it is a package
        if selectedPath in libraryInfo:
            menu.addAction(self.removeModuleAction)
        else:
            menu.addAction(self.removePackageAction)

        menu.addSeparator()
        menu.addMenu(base_menu)
        menu.exec_(self.pathWidget.mapToGlobal(qpoint))

    def refresh(self):
        """
        Refresh the library view.

        NOTE: Because there can be a timeout with the engine, we put the call to the engine in it's own thread
        """
        self.worker = WorkerThread(self.engineInterface.libraryGetInfo)
        self.worker.workerDone.connect(self._refreshComplete)
        self.worker.start()

    def _refreshComplete(self, libraryInfo):
        self.pathWidget.clearData()
        if libraryInfo:
            for module, info in libraryInfo.items():
                self.pathWidget.setValue(module, 'hash', info.get('hash', 'UNKNOWN'))
        self.pathWidget.viewAll()