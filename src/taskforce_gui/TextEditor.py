import sys
from pyqtlib import QtGui, QtCore, QT_LIB
if QT_LIB == 'PyQt4':
    from PyQt4 import Qsci
else:
    from PyQt5 import Qsci
import traceback
from taskforce_common import BLOCK_FILE_EXTENSION

import logging

logger = logging.getLogger(__name__)


class FindReplaceDialog(QtGui.QDialog):

    findText = QtCore.pyqtSignal(str, bool, bool, bool, bool)
    replaceText = QtCore.pyqtSignal(str, str, bool, bool, bool, bool)
    replaceAllText = QtCore.pyqtSignal(str, str, bool, bool, bool, bool)

    def __init__(self, replace=True):
        """Constructor.

        @type replace bool
        @param replace True if dialog should show replace options (Ctrl-H vs Ctrl-F)
        """
        super(FindReplaceDialog, self).__init__()
        self.setLayout(QtGui.QVBoxLayout())
        if replace:
            self.setWindowTitle("Replace")
        else:
            self.setWindowTitle("Find")
        # edit boxes
        findLabel = QtGui.QLabel("Search for:")
        findLabel.setMinimumWidth(80)
        self.findEdit = QtGui.QLineEdit()
        findLayout = QtGui.QHBoxLayout()
        findLayout.addWidget(findLabel)
        findLayout.addWidget(self.findEdit)

        replaceLabel = QtGui.QLabel("Replace with:")
        replaceLabel.setMinimumWidth(80)
        self.replaceEdit = QtGui.QLineEdit()
        replaceLayout = QtGui.QHBoxLayout()
        replaceLayout.addWidget(replaceLabel)
        replaceLayout.addWidget(self.replaceEdit)

        # checkboxes
        checkboxLayout = QtGui.QVBoxLayout()
        self.matchCase = QtGui.QCheckBox("Match case")
        self.matchWord = QtGui.QCheckBox("Match entire word only")
        self.searchBackwards = QtGui.QCheckBox("Search backwards")
        # TODO: QScintilla seems to not search backwards correctly
        self.searchBackwards.setEnabled(False)
        self.wrapAround = QtGui.QCheckBox("Wrap around")
        self.wrapAround.setCheckState(QtCore.Qt.Checked)
        checkboxLayout.addWidget(self.matchCase)
        checkboxLayout.addWidget(self.matchWord)
        checkboxLayout.addWidget(self.searchBackwards)
        checkboxLayout.addWidget(self.wrapAround)

        # buttons
        findButton = QtGui.QPushButton("Find")
        findButton.setDefault(True)
        replaceButton = QtGui.QPushButton("Replace")
        replaceAllButton = QtGui.QPushButton("Replace All")
        closeButton = QtGui.QPushButton("Close")
        findButton.clicked.connect(self.find)
        replaceButton.clicked.connect(self.replace)
        replaceAllButton.clicked.connect(self.replaceAll)
        closeButton.clicked.connect(self.done)
        # button layout
        buttonLayout = QtGui.QHBoxLayout()
        buttonLayout.addWidget(closeButton)
        if replace:
            buttonLayout.addWidget(replaceAllButton)
            buttonLayout.addWidget(replaceButton)
        buttonLayout.addWidget(findButton)

        self.layout().addLayout(findLayout)
        if replace:
            self.layout().addLayout(replaceLayout)
        self.layout().addLayout(checkboxLayout)
        self.layout().addLayout(buttonLayout)

        self.resize(500, 100)

    @property
    def checkCase(self):
        if self.matchCase.checkState() == QtCore.Qt.Checked:
            return True
        else:
            return False

    @property
    def checkWord(self):
        if self.matchWord.checkState() == QtCore.Qt.Checked:
            return True
        else:
            return False

    @property
    def checkBackwards(self):
        # Default search option is forward.. the checkbox text is the inverse, so flip it here
        if self.searchBackwards.checkState() == QtCore.Qt.Checked:
            return False
        else:
            return True

    @property
    def checkWrap(self):
        if self.wrapAround.checkState() == QtCore.Qt.Checked:
            return True
        else:
            return False

    def find(self):
        """Emit findText with text in the edit field.

        From http://pyqt.sourceforge.net/Docs/QScintilla2/classQsciScintilla.html
        The QScintilla signature is:
        virtual bool QsciScintilla::findFirst (const QString &expr,
                                                     bool     re,
                                                     bool     cs,
                                                     bool     wo,
                                                     bool     wrap,
                                                     bool     forward = true,
                                                     int      line = -1,
                                                     int      index = -1,
                                                     bool     show = true,
                                                     bool     posix = false
                                              )

        Find the first occurrence of the string expr and return true if expr was found, otherwise returns false. If expr is found it becomes the current selection.

        -If re is true then expr is interpreted as a regular expression rather than a simple string.
        -If cs is true then the search is case sensitive.
        -If wo is true then the search looks for whole word matches only, otherwise it searches for any matching text.
        -If wrap is true then the search wraps around the end of the text.
        -If forward is true (the default) then the search is forward from the starting position to the end of the text, otherwise it is backwards to the beginning of the text.
        -If either line or index are negative (the default) then the search begins from the current cursor position. Otherwise the search begins at position index of line line.
        -If show is true (the default) then any text found is made visible (ie. it is unfolded).
        -If posix is true then a regular expression is treated in a more POSIX compatible manner by interpreting bare ( and ) as tagged sections rather than \( and \).

        """
        self.findText.emit(self.findEdit.text(), self.checkCase, self.checkWord, self.checkWrap, self.checkBackwards)

    def replace(self):
        """Replace selected text."""
        # replaceText(findText, replaceText, cs, wo, wrap, forward)
        self.replaceText.emit(self.findEdit.text(), self.replaceEdit.text(), self.checkCase, self.checkWord, self.checkWrap, self.checkBackwards)

    def replaceAll(self):
        """Replace all instances of text."""
        # replaceAllText(findText, replaceText, cs, wo, wrap, forward)
        self.replaceAllText.emit(self.findEdit.text(), self.replaceEdit.text(), self.checkCase, self.checkWord, self.checkWrap, self.checkBackwards)


class TextEditor(Qsci.QsciScintilla):

    escPressed = QtCore.pyqtSignal()

    # old filepath, new filepath
    filePathChanged = QtCore.pyqtSignal(str, str)

    # new title name
    titleUpdated = QtCore.pyqtSignal(str)

    ARROW_MARKER_NUM = 8

    def __init__(self, title=None, **kwargs):
        super(TextEditor, self).__init__(**kwargs)

        self.setFocusPolicy(QtCore.Qt.StrongFocus)

        # Set the default font
        font = QtGui.QFont()
        font.setFamily('Ubuntu Mono')
        font.setFixedPitch(True)
        font.setPointSize(11)
        self.setFont(font)
        self.setMarginsFont(font)
        self.font = font

        # Margin 0 is used for line numbers
        fontmetrics = QtGui.QFontMetrics(font)
        self.setMarginsFont(font)
        self.setMarginWidth(0, fontmetrics.width("0000"))
        self.setMarginLineNumbers(0, True)
        self.setMarginsBackgroundColor(QtGui.QColor("#eee"))

        # Clickable margin 1 for showing markers
        self.setMarginSensitivity(1, True)
        self.marginClicked.connect(self.errorClicked)
        self.markerDefine(Qsci.QsciScintilla.RightArrow, self.ARROW_MARKER_NUM)
        self.setMarkerBackgroundColor(QtGui.QColor("#ee1111"), self.ARROW_MARKER_NUM)

        # Brace matching: enable for a brace immediately before or after
        # the current position
        self.setBraceMatching(Qsci.QsciScintilla.SloppyBraceMatch)
        self.setIndentationsUseTabs(False)
        self.setIndentationWidth(4)
        self.setAutoIndent(True)
        self.setBackspaceUnindents(True)

        # Current line visible with special background color
        self.setCaretLineVisible(True)
        self.setCaretLineBackgroundColor(QtGui.QColor("#eee"))

        # Code Folding
        self.setFolding(Qsci.QsciScintilla.BoxedTreeFoldStyle)
        self.setTabWidth(4)

        # Auto-completion
        self.setAutoCompletionSource(Qsci.QsciScintilla.AcsAll)
        self.setAutoCompletionThreshold(2)

        # Don't want to see the horizontal scrollbar at all
        # Use raw message to Scintilla here (all messages are documented
        # here: http://www.scintilla.org/ScintillaDoc.html)
        #self.SendScintilla(QsciScintilla.SCI_SETHSCROLLBAR, 0)
        self.SendScintilla(Qsci.QsciScintilla.SCI_SETSCROLLWIDTH, 800)

        # Actions
        self.findDialogAction = QtGui.QAction('Find', self)
        self.findDialogAction.setShortcuts(QtGui.QKeySequence('Ctrl+F'))
        self.findDialogAction.setShortcutContext(QtCore.Qt.WidgetShortcut)
        self.findDialogAction.triggered.connect(self.findDialog)

        self.findAndReplaceDialogAction = QtGui.QAction('Find and Replace', self)
        self.findAndReplaceDialogAction.setShortcut(QtGui.QKeySequence('Ctrl+H'))
        self.findAndReplaceDialogAction.triggered.connect(self.findAndReplaceDialog)

        self.saveFileAction = QtGui.QAction('Save', self)
        self.saveFileAction.setShortcut(QtGui.QKeySequence.Save)
        self.saveFileAction.triggered.connect(self.save)

        self.saveAsFileAction = QtGui.QAction('Save As...', self)
        self.saveAsFileAction.triggered.connect(self.saveAs)

        self.addAction(self.findDialogAction)
        self.addAction(self.findAndReplaceDialogAction)
        self.addAction(self.saveFileAction)

        # Customize the context menu
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.showContextMenu)

        self.logger = logging.getLogger(__name__)

        # store the path of the current file
        self.path = None
        self.title = title

        self.modificationChanged.connect(self.updateTitle)

        self.error_text = ''

        # modified bool flag
        self.setModified(False)
        self.show()

    def closeFile(self):
        """
        Close the current file.

        If the current file has been modified, the user will be prompted to save, discard or cancel.
        After selecting save or discard, this method clears the contents of the editor and returns
        true.  If cancel is selected, the editor keeps the text and returns false.  If the text had
        not been modified, this method returns True
        """
        if self.isModified():
            msgBox = QtGui.QMessageBox()
            msgBox.setText("This document has been modified.")
            msgBox.setInformativeText("Do you want to save your changes?")
            msgBox.setStandardButtons(QtGui.QMessageBox.Save | QtGui.QMessageBox.Discard | QtGui.QMessageBox.Cancel)
            msgBox.setDefaultButton(QtGui.QMessageBox.Cancel)
            ret = msgBox.exec_()

            if ret == QtGui.QMessageBox.Save:
                self.save()
                self.setText('')
                self.setModified(False)
                return True
            elif ret == QtGui.QMessageBox.Discard:
                self.setText('')
                self.setModified(False)
                return True
            elif ret == QtGui.QMessageBox.Cancel:
                return False
        self.setText('')
        self.setModified(False)
        return True

    def doParse(self):
        try:
            self.setErrorMarker()
        except SyntaxError:
            self.error_text = '{0}: {1}'.format(e.__class__.__name__, e.msg)
            self.setErrorMarker(e.lineno - 1)
        except Exception:
            etype, error, tb = sys.exc_info()
            self.error_text = '{0}: {1}'.format(etype.__name__, error)
            line = traceback.extract_tb(tb)[-1][1]
            self.setErrorMarker(line - 1)

    def errorClicked(self, margin, line, modifiers):
        if self.markersAtLine(line):
            QtGui.QMessageBox.warning(self, 'Error on line {0}'.format(line + 1), self.error_text)

    def findAndReplaceDialog(self):
        findReplaceDialog = FindReplaceDialog()
        findReplaceDialog.findText.connect(self.findText)
        findReplaceDialog.replaceText.connect(self.replaceText)
        findReplaceDialog.replaceAllText.connect(self.replaceAll)
        findReplaceDialog.exec_()

    def findDialog(self):
        findDialog = FindReplaceDialog(False)
        findDialog.findText.connect(self.findText)
        findDialog.exec_()

    def findText(self, find, cs, wo, wrap, forward):
        """Emit QScintilla's findFirst method to search for text.

        @type find QString
        @param find The string to search for.

        @type cs bool
        @param cs True if search is case-sensitive.

        @type wo bool
        @param wo True if search should match exact word.

        @type wrap bool
        @param wrap True if search should wrap around.

        @type forward bool
        @param forward True if search goes forward.
        """
        # findFirst(string, regex, cs, wo, wrap, forward)
        self.findFirst(find, False, cs, wo, wrap, forward)

    def foldLine(self, line=-1):
        if line < 0:
            line, index = self.getCursorPosition()
        Qsci.QsciScintilla.foldLine(self, line)

    def getPath(self):
        return self.path

    def initializeLexer(self, filename):
        #TODO: select lexer based on file-type

        if filename.endswith('.py'):
            self.initializePythonLexer()
        elif filename.endswith(BLOCK_FILE_EXTENSION):
            self.initializeJSONLexer()
        else:
            self.initializePythonLexer()

    def initializePythonLexer(self):
        lexer = Qsci.QsciLexerPython()
        lexer.setDefaultFont(self.font)
        lexer.setFoldComments(True)
        lexer.setFoldQuotes(True)
        self.setLexer(lexer)

    def initializeJSONLexer(self):
        lexer = Qsci.QsciLexerJavaScript()
        lexer.setDefaultFont(self.font)
        lexer.setFoldComments(True)
        #lexer.setFoldQuotes(True)
        self.setLexer(lexer)

    def keyPressEvent(self, event):
        # allow untabbing a block of text with shift-tab (this is otherwise grabbed by QT to change focus)
        if event.key() == QtCore.Qt.Key_Backtab:
            event.accept()
            newev = QtGui.QKeyEvent(event.type(), QtCore.Qt.Key_Tab, QtCore.Qt.ShiftModifier)
            return super(TextEditor, self).keyPressEvent(newev)

        super(TextEditor, self).keyPressEvent(event)

    def openCode(self, code, extension='.py'):
        """Open an editor with the code instead of reading in from a file.

        @type code string
        @param code The code to open the editor with.

        @type extension string
        @param extension The file extension.  This is used to type the file for the lexer.
        """
        self.initializeLexer('code'+extension)
        self.setText(code)
        self.doParse()
        self.setModified(True)

    def openFile(self, path):
        """Open a text file.

        @type path string
        @param path Path of the file
        """
        self.path = path
        try:
            # if os.path.exists(path):
            #     option = 'r+'
            # else:
            #     option = 'w+'
            with open(path, 'a+') as f:
                self.setText(f.read())
            self.setModified(False)
            f.close()
            self.initializeLexer(path)
            self.doParse()
            return True
        except Exception as e:
            print e
            self.logger.error('Error opening file:{}'.format(path))
            return False

    def replaceAll(self, find, replace, cs, wo, wrap, forward):
        """Replace all text. See findText for arguments."""
        while self.findFirst(find, False, cs, wo, wrap, forward):
            self.replace(replace)

    def replaceAllText(self, value):
        """A dangerous utility function. Replace all text."""
        self.setText(value)

    def replaceText(self, find, replace, cs, wo, wrap, forward):
        """Replace text and search for the next. See findText for arguments."""
        self.replace(replace)
        self.findFirst(find, False, cs, wo, wrap, forward)

    def save(self):
        self.doParse()
        if not self.path:
            self.saveAs()
            return
        else:
            with open(self.path, 'w') as f:
                f.write(self.text())
            f.close()
            logger.info('Saved file {}'.format(self.path))
            self.setModified(False)

    def saveAs(self):
        self.doParse()

        # try-except used to handle API difference between PyQt4 and PyQt5
        try:
            newPath, filter = QtGui.QFileDialog.getSaveFileNameAndFilter()
        except AttributeError:
            newPath, filter = QtGui.QFileDialog.getSaveFileName()

        if newPath:
            self.path = newPath
            self.save()

    def setErrorMarker(self, line=-1):
        if line < 0:
            self.markerDeleteAll()
        else:
            self.markerAdd(line, self.ARROW_MARKER_NUM)

    def showContextMenu(self, event):
        menu = self.createStandardContextMenu()
        menu.addSeparator()
        menu.addAction(self.findDialogAction)
        menu.addAction(self.findAndReplaceDialogAction)
        menu.addSeparator()
        menu.addAction(self.saveFileAction)
        menu.addAction(self.saveAsFileAction)
        menu.addAction('Close', self.closeFile)
        menu.addSeparator()
        menu.addAction('Fold', self.foldLine)
        menu.addAction('Fold All', self.foldAll)
        menu.exec_(QtGui.QCursor.pos())

    def updateTitle(self):
        title = self.title

        if not title:
            title = self.path

        if self.isModified():
            title = title + '*'
        self.setWindowTitle(title)
        self.titleUpdated.emit(self.windowTitle())
