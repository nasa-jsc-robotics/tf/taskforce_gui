import logging
import os

from pyqtlib import QtGui, QtCore
from pyqtlib import LogFileView, TreeFilterWidget

from taskforce_model import EngineModel, EnginePathModel

from taskforce_gui import EngineLibraryWidget
from taskforce_gui import EngineView
from taskforce_gui import GroupEditor
from taskforce_gui import GroupLiveScene
from taskforce_gui import ParameterBindingWidget
from taskforce_gui import ParameterWidget
from taskforce_gui import LibraryView
from taskforce_gui import TaskInfoWidget
from taskforce_gui import TextEditor

from taskforce_gui.ExceptionDialog import getExceptionDialog
import taskforce_gui.resources.taskforce_icons
from taskforce_engine import EngineInterface
from taskforce_common.utils import getSourceCode, jsonToString
from taskforce_common import SUITE_NAME

logger = logging.getLogger('taskforce_gui.main')

DEFAULT_TASKFORCE_GUI_LIBRARY_LOCATION = os.path.join(os.environ['HOME'],'.taskforce_gui_library')


class Main(QtGui.QMainWindow):
    def __init__(self, clientFactory, subscriber, log_file_path=None):
        """
        TaskForce Editor

        @type clientFactory ClientFactory
        @param client Client port used to send commands to the Task Engine

        @type subscriber taskforce_common.Subscriber
        @param subscriber Subscriber port used to receive status from the Task Engine

        @type log_file_path string
        @param log_file_path Path to a custom log file
        """
        super(Main, self).__init__()

        # This line prevents "QObject::startTimer: QTimer can only be used with threads started with QThread" errors that were getting thrown on shutdown
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.setWindowTitle('TaskForce - {}'.format(SUITE_NAME))
        app_icon = taskforce_gui.resources.taskforce_icons.get_taskforce_logo()
        self.setWindowIcon(app_icon)

        # Initialize settings.  Settings are available in self.settings
        self.settings = None
        self.initialize_settings()
        self.taskforce_gui_library_location = self.settings.value('TASKFORCE_GUI_LIBRARY_LOCATION')

        # self.blockFileParser = BlockFileParser()
        self.engineClient = clientFactory.createClient()

        self.engineSubscriber = subscriber

        self.engineInterface = EngineInterface(self.engineClient)
        ######
        # Widgets
        ######
        # The blocks and tasks are managed using a LibraryModel.  This model can be manipulated using a LibraryView.
        # From this view, the user can add,delete,deploy,upload, and send files to an editor.


        ###############################
        # engineModel / localLibraryModel
        ###############################
        self.engineModel = EngineModel(self.engineClient, self.engineSubscriber)
        self.enginePathModel = EnginePathModel(checkable=False, delimiters='/')
        self.enginePathModel.engineModel = self.engineModel
        self.engineStatusModel = self.engineModel.getEngineStatusModel()

        self.localLibraryModel = self.engineModel.getLocalLibraryModel()
        self.localLibraryModel.setNameFilters(['*.py', '*.group'])
        self.localLibraryModel.setNameFilterDisables(False)
        self.localLibraryModel.setResolveSymlinks(True)
        self.localLibraryModel.setReadOnly(False)
        # Set the root path last, since this will refresh the root directory, and update the filters.
        self.localLibraryModel.setRootPath(self.taskforce_gui_library_location)

        #########################
        # Local Library
        #########################
        self.localLibraryView = LibraryView()
        self.localLibraryView.setModel(self.localLibraryModel)
        self.localLibraryView.setRootIndex(self.localLibraryModel.index(self.taskforce_gui_library_location))
        self.localLibraryView.setDragEnabled(True)
        self.localLibraryView.setAcceptDrops(True)
        self.localLibraryView.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.localLibraryView.hideColumn(1)
        self.localLibraryView.hideColumn(2)
        self.localLibraryView.hideColumn(3)

        self.refreshLibraryButton = QtGui.QPushButton('refresh library')
        self.refreshLibraryButton.clicked.connect(self.localLibraryModel.updateFileMap)

        self.localLibraryWidget = QtGui.QWidget()
        localLibraryLayout = QtGui.QVBoxLayout()
        localLibraryLayout.addWidget(self.localLibraryView)
        localLibraryLayout.addWidget(self.refreshLibraryButton)
        self.localLibraryWidget.setLayout(localLibraryLayout)


        ###############################
        # engineView / engineViewWidget
        ###############################
        # The engineView displays status and allows users to send commands directly to Tasks.
        self.engineView = EngineView()
        self.engineView.setModel(self.enginePathModel)

        self.engineStatusModel.connectionStateChanged.connect(self.engineView.connectionStateChanged)
        self.engineStatusModel.connectionStateChanged.connect(self.engineStatusChanged)

        self.engineViewWidget = TreeFilterWidget()
        self.engineViewWidget.setView(self.engineView)

        ################################
        # Engine Library
        ################################

        # The engineLibraryWidget displays what is currently in the connected
        # engine.
        self.engineLibraryWidget = EngineLibraryWidget(EngineInterface(clientFactory.createClient()))

        # The taskInfoWidget will display various information of tasks
        # selected from other widgets
        self.taskInfoWidget = TaskInfoWidget()

        # QListView to view the undo stack
        self.undoView = QtGui.QUndoView()

        # SerializeView to view block serialization
        self.serializeBlockView = QtGui.QPlainTextEdit()

        # Widget for viewing the log
        self.logWindow = LogFileView()
        if log_file_path:
            self.logWindow.addFile(log_file_path)

        # The main tab widget which contains various editors
        self.tabWidget = QtGui.QTabWidget(parent=self)
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.setMovable(True)
        self.tabWidget.tabCloseRequested.connect(self.closeTab)
        self.tabWidget.currentChanged.connect(self.currentTabChanged)

        # When a task is selected in the status menu, update the taskInfoWidget
        self.engineView.taskSelected.connect(self.taskInfoWidget.updateView)
        self.engineView.taskDoubleClicked.connect(self.engineViewTaskDoubleClicked)

        # Connect signals to the library manager
        self.localLibraryView.deployBlock.connect(self.engineModel.groupDeploy)

        self.localLibraryView.openFile.connect(self.openFile)
        self.localLibraryView.openFileAsText.connect(self.openFileWithTextEditor)
        self.localLibraryView.uploadModule.connect(self.uploadModule)

        #TODO: Connected the engine status widget up to the library manager
        #self.engineStatusWidget.connectedStateChanged.connect(self.libraryManager.setEngineConnected)


        #############
        #  Parameter widget
        #############
        self.parameterWidget = ParameterWidget()
        self.parameterWidget.getParametersRequest.connect(self._groupEditorSelectionChangedHandler)
        self.parameterWidget.setParametersRequest.connect(self.setCurrentEditorTaskParameters)

        self.parameterBindingWidget = ParameterBindingWidget()

        #######
        # Create docks for the widgets
        #######

        self.localLibraryDock = QtGui.QDockWidget('Library Manager')
        self.localLibraryDock.setWidget(self.localLibraryWidget)

        self.logWindowDock = QtGui.QDockWidget('Log Window')
        self.logWindowDock.setWidget(self.logWindow)

        self.engineViewWidgetDock = QtGui.QDockWidget('Engine Status ({})'.format(self.engineStatusModel.connectedAsString))
        self.engineViewWidgetDock.setWidget(self.engineViewWidget)

        self.taskInfoDock = QtGui.QDockWidget('Task Info')
        self.taskInfoDock.setWidget(self.taskInfoWidget)

        self.blockUndoDock = QtGui.QDockWidget('Undo')
        self.blockModelDock = QtGui.QDockWidget('Block Model')
        self.blockModelDock.setWidget(self.serializeBlockView)

        self.engineLibraryDock = QtGui.QDockWidget('Engine Library')
        self.engineLibraryDock.setWidget(self.engineLibraryWidget)

        self.parameterDock = QtGui.QDockWidget('Parameters')
        self.parameterDock.setWidget(self.parameterWidget)

        self.parameterBindingDock = QtGui.QDockWidget('Bindings')
        self.parameterBindingDock.setWidget(self.parameterBindingWidget)

        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.logWindowDock)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.localLibraryDock)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.engineViewWidgetDock)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.taskInfoDock)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.engineLibraryDock)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.blockUndoDock)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.blockModelDock)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.parameterDock)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.parameterBindingDock)

        self.tabifyDockWidget(self.engineViewWidgetDock, self.engineLibraryDock)
        self.tabifyDockWidget(self.parameterDock, self.parameterBindingDock)
        self.tabifyDockWidget(self.parameterDock, self.taskInfoDock)
        self.tabifyDockWidget(self.parameterDock, self.blockModelDock)
        self.tabifyDockWidget(self.parameterDock, self.blockUndoDock)

        self.setDockOptions(QtGui.QMainWindow.AllowTabbedDocks)

        self.engineViewWidgetDock.raise_()
        self.parameterDock.raise_()


        ##################
        # Actions
        ##################
        openFileDialogAction = QtGui.QAction('Open...', self)
        openFileDialogAction.triggered.connect(self.openFileDialog)

        ##################
        # Menubar
        ##################
        menuBar = self.menuBar()
        menuBar.setNativeMenuBar(False)

        fileMenu = menuBar.addMenu('File')
        fileMenu.addAction(openFileDialogAction)
        fileMenu.addActions(self.localLibraryView.getFileMenu().actions())

        # The "view" menu is the default pop-up for the dock widgets
        viewMenu = self.createPopupMenu()
        viewMenu.setTitle('View')
        menuBar.addMenu(viewMenu)

        #######
        # Layouts
        #######
        self.logWindow.setMinimumHeight(200)
        self.localLibraryView.setMinimumWidth(275)
        self.engineView.setMinimumWidth(350)
        self.setCentralWidget(self.tabWidget)

        self.engineView.expandAll()
        self.engineView.adjustColumns()

        self.localLibraryView.expandAll()
        self.localLibraryView.adjustColumns()

    @QtCore.pyqtSlot(int)
    def currentTabChanged(self, index):
        """
        Slot for when the current index of the tab has changed.

        This method is used to change the screen based on what tab is currently selected.
        :param index: New tab index
        """
        widget = self.tabWidget.currentWidget()
        self._groupEditorSelectionChangedHandler()

        if isinstance(widget, GroupEditor):
            undoView = QtGui.QUndoView(widget.getUndoStack())
            self.blockUndoDock.setWidget(undoView)
            group = widget.getGroup()
            self.parameterBindingWidget.setUndoStack(widget.controller.undoStack)
            self.parameterBindingWidget.setGroup(group)

    def createTask(self):
        """
        Create a new Task and open it in a text editor
        """
        templateCode = getSourceCode('taskforce_common.task.task_template')

        title = 'New Task'
        textEditor = TextEditor(title=title)
        textEditor.titleUpdated.connect(self.updateTabNames)

        textEditor.openCode(templateCode)
        index = self.tabWidget.addTab(textEditor, title)
        self.tabWidget.setCurrentIndex(index)

    def closeEvent(self, event):
        """
        Override the close event so that we can close the logServer
        """
        QtGui.QMainWindow.closeEvent(self, event)

    @QtCore.pyqtSlot(int)
    def closeTab(self, index):
        """
        Slot connected to the tab widget's "tabCloseRequest" signal
        Args:
            index:

        Returns:

        """
        widget = self.tabWidget.widget(index)
        close_ok = True

        if type(widget) == TextEditor:
            close_ok = widget.closeFile()

        elif type(widget) == GroupEditor:
            close_ok = widget.close()

        if close_ok:
            self.tabWidget.removeTab(index)

    @QtCore.pyqtSlot(str)
    def engineViewTaskDoubleClicked(self, taskName):
        taskStatus = self.engineModel.taskStatus(taskName)
        if taskStatus is None:
            return

        try:  # If the item clicked is a .group file, start the LiveViewer
            file_name = taskStatus['fileName']

        except KeyError:  # Otherwise, check if it's a .py file. If so, open that in the text viewer
            file_name = taskStatus['class'] + '.py'

            # Since the task does not have "fileName" in the 'status', we have to search for it and format it.
            new_path = None
            for path in self.localLibraryModel.getLibraryInfo():
                if str(path.split('.')[-1:][0]) + '.py' == file_name:
                    new_path = path
                    break
            if new_path is None:
                msg = (
                    'Unable to open file associated with deployed block ' +
                    file_name +
                    '. Please either open file outside the live viewer, or ' +
                    'ensure that the source python file is available in the Library Manager.'
                )
                logger.error(msg)
                return
            new_path = new_path.replace(".", "/") + ".py"

            self.openFile(new_path, title=taskName)
            return

        self.openBlockWithBlockLiveViewer(file_name, title=taskName)


    @QtCore.pyqtSlot(bool)
    def engineStatusChanged(self, connected):
        self.engineViewWidgetDock.setWindowTitle('Engine Status ({})'.format(self.engineStatusModel.connectedAsString))

    def findTab(self, tabText, widgetType):
        """
        Return the index of a tab that has the matching tabText and matching widgetType.

        NOTE: This method will match tabText if the text ends with a '*'
        Args:
            tabText: Name of the tab
            widgetType: Type of the widget for that tab

        Returns: Index of the tab, or -1 if not found.
        """
        for i in range(self.tabWidget.count()):
            if tabText == self.tabWidget.tabText(i).strip('*'):
                if type(self.tabWidget.widget(i)) == widgetType:
                    return i
        return -1

    def getCurrentGroupEditor(self):
        """
        Return the current GroupEditor in focus of the main Tab widget

        Returns:
            GroupEditor widget or None
        """
        currentWidget = self.tabWidget.currentWidget()
        if isinstance(currentWidget, GroupEditor):
            return currentWidget
        else:
            return None

    def getLibraryFullPath(self, path):
        return self.localLibraryModel.getFullPath(path)

    def getLibraryRelativePath(self, path):
        """
        This method will convert the input path to a path relative to the library.

        If the path is not a sub-folder of the library, it will return the full path.

        Args:
            path:

        Returns:

        """
        return self.localLibraryModel.getRelativePath(path)

    def initialize_settings(self):
        # required to set name, domain, app name
        # on Ubuntu, this will store settings in ~/.config/NASA_JSC/taskforce.conf
        QtCore.QCoreApplication.setOrganizationName('NASA_JSC')
        QtCore.QCoreApplication.setOrganizationDomain("jsc.nasa.gov")
        QtCore.QCoreApplication.setApplicationName("taskforce")

        # set default settings if necessary
        self.settings = QtCore.QSettings()

        ###################
        # Library settings
        ###################
        # If library setting does not exist, populate it with an empty value
        if not self.settings.contains('TASKFORCE_GUI_LIBRARY_LOCATION'):
            self.settings.setValue('TASKFORCE_GUI_LIBRARY_LOCATION', DEFAULT_TASKFORCE_GUI_LIBRARY_LOCATION)

        elif self.settings.value('TASKFORCE_GUI_LIBRARY_LOCATION') is None:
            self.settings.setValue('TASKFORCE_GUI_LIBRARY_LOCATION', DEFAULT_TASKFORCE_GUI_LIBRARY_LOCATION)

        path = self.settings.value('TASKFORCE_GUI_LIBRARY_LOCATION')
        if not os.path.exists(path):
            os.makedirs(path)
        self.save_settings()

    def openFile(self, path, title=None):
        self.localLibraryModel.updateFileMap()

        try:
            path = str(path)
            if path.endswith(GroupEditor.BlockFileExtension):
                self.openFileWithGroupEditor(path, title)
            else:
                self.openFileWithTextEditor(path, title)
        except Exception:
            getExceptionDialog('Error opening file!', 'Exception raised when opening the file')

    def openFileDialog(self):
        self.localLibraryModel.updateFileMap()
        path, filter = QtGui.QFileDialog.getOpenFileNameAndFilter()
        if path:
            path = str(path)
            self.openFile(path)

    def openFileWithTextEditor(self, path, title=None):
        """
        Open a file in a text editor.

        If the file is already open, the tab containing the file will become focused.

        @type path string
        @param path Path to the file to edit
        """
        if not title:
            title = self.getLibraryRelativePath(path)
            if title is None:
                title = path

        tabIdx = self.findTab(title, TextEditor)
        if tabIdx >= 0:
            self.tabWidget.setCurrentIndex(tabIdx)
            return

        libraryPath = self.localLibraryModel.getFullPath(path)
        if libraryPath is None:
            filepath = path
        else:
            filepath = libraryPath

        textEditor = TextEditor(title=title, parent=self)
        textEditor.titleUpdated.connect(self.updateTabNames)

        if textEditor.openFile(filepath):
            index = self.tabWidget.addTab(textEditor, title)
            self.tabWidget.setCurrentIndex(index)

    def openFileWithGroupEditor(self, path=None, title=None):
        """
        Open a block file in the block editor.

        If the file is already open, the tab containing the file will become focused.
        """
        try:
            filepath = self.getLibraryFullPath(path)
            if filepath is None:
                raise Exception('File path for "{}" is invalid.  Is file missing or moved?'.format(title))

            title = self.getLibraryRelativePath(path)
            if title is None:
                title = path
            root_path = filepath.split(title)[0]

            tabIdx = self.findTab(title, GroupEditor)
            if tabIdx >= 0:
                self.tabWidget.setCurrentIndex(tabIdx)
                return
            blockEditor = GroupEditor(title=title, rootPath=root_path, parent=self)
            blockEditor.titleUpdated.connect(self.updateTabNames)
            blockEditor.doubleClickedTask.connect(self.openFileWithTextEditor)
            blockEditor.doubleClickedSubBlock.connect(self.openFileWithGroupEditor)
            blockEditor.sceneChanged.connect(self.updateSerializeBlockView)
            blockEditor.selectionChanged.connect(self._groupEditorSelectionChangedHandler)
            blockEditor.controller.syncParameterWidget.connect(self._groupEditorSelectionChangedHandler)
            blockEditor.openBlock(filepath)

            index = self.tabWidget.addTab(blockEditor, blockEditor.windowTitle())
            self.tabWidget.setCurrentIndex(index)
        except Exception:
            getExceptionDialog("Error opening block file")

    def openBlockWithBlockLiveViewer(self, path, title):
        """
        Open a block file in the live viewer

        If the file is already open, the tab containing the file will become focused.
        """
        tabIdx = self.findTab(title, GroupEditor)
        if tabIdx >= 0:
            self.tabWidget.setCurrentIndex(tabIdx)
            return

        liveScene = GroupLiveScene(blockName=title)
        liveScene.setEnginePathModel(self.enginePathModel)
        liveScene.statusUpdated.connect(self._liveUpdateParametersHandler)

        liveEditor = GroupEditor(title=title, rootPath='../', parent=self)
        liveEditor.setController(liveScene)
        liveEditor.titleUpdated.connect(self.updateTabNames)
        liveEditor.doubleClickedTask.connect(self.openFileWithTextEditor)
        liveEditor.doubleClickedSubBlock.connect(self.openBlockWithBlockLiveViewer)
        liveEditor.selectionChanged.connect(self._groupEditorSelectionChangedHandler)
        liveEditor.controller.syncParameterWidget.connect(self._groupEditorSelectionChangedHandler)

        filepath = self.getLibraryFullPath(path)
        if liveEditor.openBlock(filepath):
            index = self.tabWidget.addTab(liveEditor, liveEditor.windowTitle())
            self.tabWidget.setCurrentIndex(index)

    @QtCore.pyqtSlot(str, dict)
    def setCurrentEditorTaskParameters(self, taskName, parameters):
        """
        Set the parameters of the currently selected Task/SubBlock

        This slot is connected to the ParameterWidget's "setTaskParametersRequest" signal

        Args:
            parameters:
        """
        blockEditor = self.getCurrentGroupEditor()

        if blockEditor is not None:
            if taskName is not None:
                blockEditor.setTaskParameters(taskName, parameters)

    @QtCore.pyqtSlot(object)
    def updateBlockFileMap(self, block_file_map):
        self.block_file_map = block_file_map
        # self.blockFileParser.block_file_map = block_file_map

    def updateTabNames(self):
        """
        Update the name of all tabs.

        For each tab, the text is set to the widget's windowTitle
        """
        for i in range(self.tabWidget.count()):
            txt = self.tabWidget.widget(i).windowTitle()
            self.tabWidget.setTabText(i, txt)

    def updateSerializeBlockView(self):
        """
        Updates the serializeBlockView with the serialization of the currently selected block editor tab
        """
        widget = self.tabWidget.currentWidget()
        if isinstance(widget, GroupEditor):
            text = 'Block:\n'
            text += jsonToString(widget.serializeBlock())
            text += 'Scene:\n'
            text += jsonToString(widget.serializeScene())
            self.serializeBlockView.setPlainText(text)

    def uploadModule(self, moduleName, filepath):
        """
        Upload a module to the engine.

        Args:
            moduleName: Name of the module as a string
            filepath: File path of the module as a string
        """
        logger.info('Uploading: module={}, filepath={}'.format(moduleName, filepath))
        self.engineInterface.libraryAddModuleFile(moduleName, filepath, force_reload=True)
        self.engineLibraryWidget.refresh()

    def save_settings(self):
        if self.settings:
            self.settings.sync()

    def syncRemoteLibrary(self):
        guiLibraryInfo = self.localLibraryModel.getLibraryInfo()
        engineLibraryInfo = self.engineInterface.libraryGetInfo()

        for moduleName, info in guiLibraryInfo.items():
            if moduleName not in engineLibraryInfo.keys():
                self.uploadModule(moduleName, info['path'])
            else:
                engineInfo = engineLibraryInfo[moduleName]
                if info['hash'] != engineInfo['hash']:
                    self.uploadModule(moduleName, info['path'])

    def _groupEditorSelectionChangedHandler(self, taskName=None):
        groupEditor = self.getCurrentGroupEditor()
        if groupEditor is not None:
            if taskName is None:
                name, parameters = groupEditor.getSelectedTaskNameAndParameters()
            else:
                name = taskName
                parameters = groupEditor.getTaskParameters(name)
            if name is not None:
                if parameters is None:
                    parameters = {}
                self.parameterWidget.setParameters(name, parameters)
            else:
                groupName = groupEditor.getGroupName()
                parameters = groupEditor.getTaskParameters(groupName)
                self.parameterWidget.setParameters(groupName, parameters)
            self.parameterBindingWidget.refreshGroup()
        else:
            self.parameterWidget.clear()

    def _liveUpdateParametersHandler(self):
        """
        Callback for when a Live view is updated.
        """
        groupEditor = self.getCurrentGroupEditor()
        if groupEditor is not None:
            # check to see what widget is currently in focus
            focus = QtGui.QApplication.focusWidget()
            widgets = []
            # check for all parents parents
            while focus is not None:
                widgets.append(focus)
                focus = focus.parent()

            # update the parameter widget unless it is currently in focus
            if self.parameterWidget not in widgets and self.parameterBindingWidget not in widgets:
                self._groupEditorSelectionChangedHandler()
