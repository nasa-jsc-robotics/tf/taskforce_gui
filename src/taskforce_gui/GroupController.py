from pyqtlib import QtGui, QtCore

from pyqtlib import DictionaryDialog, ComboBoxInputWidget

import os
import json
import logging
from .ExceptionDialog import getExceptionDialog

from pyqtlib.node_graph import GraphScene
from pyqtlib.node_graph import Port, ProvidesPort, RequiresPort
from pyqtlib.node_graph import PortOrientation

from taskforce_model import GroupModel

from taskforce_common.task import Task, Group, GroupException
from taskforce_common.utils import createObject, getClassDefinition, openJsonFile, stringToJson
from taskforce_common import BLOCK_FILE_EXTENSION

from .BlockItem import BlockItem
from .Comment import Comment
from .TaskItem import TaskItem
from .SubGroupItem import SubGroupItem
from .SubscriptionConnection import SubscriptionConnection
from .TaskView import TaskView

logger = logging.getLogger(__name__)

##############
#
# Undo / Redo Commands
#
##############


class AddSubBlockCommand(QtGui.QUndoCommand):
    """
    QUndoCommand for adding a SubBlock
    """
    def __init__(self, model, subBlock, description):
        super(AddSubBlockCommand, self).__init__(description)
        self.model = model
        self.subBlock = subBlock

    def redo(self):
        """Called by the QUndoStack to perform the command"""
        self.model.addSubGroup(self.subBlock)

    def undo(self):
        """Called by the QUndoStack to undo the command"""
        self.model.removeChild(self.subBlock.name)


class AddTaskCommand(QtGui.QUndoCommand):
    """
    QUndoCommand for adding a Task
    """
    def __init__(self, model, task, description):
        super(AddTaskCommand, self).__init__(description)
        self.model = model
        self.task = task

    def redo(self):
        """
        Called by the QUndoStack to perform the command
        """
        self.model.addTask(self.task)

    def undo(self):
        """
        Called by the QUndoStack to undo the command
        """
        self.model.removeChild(self.task.name)


class RemoveSubBlockCommand(QtGui.QUndoCommand):
    """
    QUndoCommand for adding a SubBlock
    """
    def __init__(self, model, subBlock, description):
        super(RemoveSubBlockCommand, self).__init__(description)
        self.model = model
        self.subBlock = subBlock

    def redo(self):
        """Called by the QUndoStack to perform the command"""
        self.model.removeChild(self.subBlock.name)

    def undo(self):
        """Called by the QUndoStack to undo the command"""
        self.model.addSubBlock(self.subBlock)


class RemoveTaskCommand(QtGui.QUndoCommand):
    """
    QUndoCommand for adding a Task
    """
    def __init__(self, model, task, description):
        super(RemoveTaskCommand, self).__init__(description)
        self.model = model
        self.task = task

    def redo(self):
        """
        Called by the QUndoStack to perform the command
        """
        self.model.removeChild(self.task.name)

    def undo(self):
        """
        Called by the QUndoStack to undo the command
        """
        self.model.addTask(self.task)


class AddCommentCommand(QtGui.QUndoCommand):
    """
    QUndoCommand for adding a Comment
    """
    def __init__(self, controller, text, position, description):
        super(AddCommentCommand, self).__init__(description)
        self.controller = controller
        self.text = text
        self.position = position

    def redo(self):
        """
        Called by QUndoStack to perform the command
        """
        self.controller.addComment(self.text, self.position)

    def undo(self):
        """
        Called by QUndoStack to undo the command
        """
        self.controller.removeComment(self.text, self.position)


class RemoveCommentCommand(QtGui.QUndoCommand):
    """
    QUndoCommand for removing a Comment
    """
    def __init__(self, controller, text, position, description):
        super(RemoveCommentCommand, self).__init__(description)
        self.controller = controller
        self.text = text
        self.position = position

    def redo(self):
        """
        Called by QUndoStack to perform the command
        """
        self.controller.removeComment(self.text, self.position)

    def undo(self):
        """
        Called by QUndoStack to undo the command
        """
        self.controller.addComment(self.text, self.position)


class EditCommentCommand(QtGui.QUndoCommand):
    """
    QUndoCommand for editing a Comment
    """
    def __init__(self, comment, pretext, posttext, description):
        super(EditCommentCommand, self).__init__(description)
        self.comment = comment
        self.pretext = pretext
        self.posttext = posttext

    def redo(self):
        """
        Called by QUndoStack to perform the command
        """
        self.comment.setText(self.posttext)

    def undo(self):
        """
        Called by QUndoStack to undo the command
        """
        self.comment.setText(self.pretext)


class SetParametersCommand(QtGui.QUndoCommand):
    """
    QUndoCommand for setting Task parameters.
    """
    def __init__(self, block, parameters, override, signal, description):
        super(SetParametersCommand, self).__init__(description)
        self.block = block
        self.override = override
        self.signal = signal
        self.parameters_new = parameters
        self.parameters_old = block.getParameter().copy()

    def redo(self):
        """
        Called by the QUndoStack to perform the command
        """
        if self.override:
            self.block.setParameter(self.parameters_new, self.override)
        else:
            self.block.setParameter(self.parameters_new)
        # Need to sync parameter widget
        self.signal.emit(self.block.name)

    def undo(self):
        """
        Called by the QUndoStack to undo the command
        """
        if self.override:
            self.block.setParameter(self.parameters_old, self.override)
        else:
            self.block.setParameter(self.parameters_old)
        # Need to sync parameter widget
        self.signal.emit(self.block.name)


class EditSubscriptionCommand(QtGui.QUndoCommand):
    """
    QUndo Command for editing a subscription
    """
    def __init__(self, model, destinationName, eventName, sourceName, callback, breakpoint, description):
        super(EditSubscriptionCommand, self).__init__(description)
        self.model = model
        self.destinationName = destinationName
        self.eventName = eventName
        self.sourceName = sourceName
        self.callback = callback
        self.breakpoint = breakpoint
        self.old = self.model.group.getSubscription(eventName, sourceName, destinationName, callback)

    def redo(self):
        """Called by the QUndoStack to perform the command"""
        self.model.subscribe(self.eventName, self.sourceName, self.destinationName, self.callback, self.breakpoint)

    def undo(self):
        """Called by the QUndoStack to undo the command"""
        self.model.subscribe(self.eventName, self.sourceName, self.destinationName, self.callback, self.old.breakpoint)


class SubscribeCommand(QtGui.QUndoCommand):
    """
    QUndoCommand for subscribing one task to another
    """
    def __init__(self, model, destinationName, eventName, sourceName, callback, breakpoint, description):
        super(SubscribeCommand, self).__init__(description)
        self.model = model
        self.destinationName = destinationName
        self.eventName = eventName
        self.sourceName = sourceName
        self.callback = callback
        self.breakpoint = breakpoint

    def redo(self):
        """Called by the QUndoStack to perform the command"""
        self.model.subscribe(self.eventName, self.sourceName, self.destinationName, self.callback, self.breakpoint)

    def undo(self):
        """Called by the QUndoStack to undo the command"""
        self.model.unsubscribe(self.eventName, self.sourceName, self.destinationName, self.callback)


class UnsubscribeCommand(QtGui.QUndoCommand):
    """
    QUndoCommand for unsubscribing
    """
    def __init__(self, model, destinationName, eventName, sourceName, callback, description):
        super(UnsubscribeCommand, self).__init__(description)
        self.model = model
        self.destinationName = destinationName
        self.eventName = eventName
        self.sourceName = sourceName
        self.callback = callback
        self.old = self.model.group.getSubscription(eventName, sourceName, destinationName, callback)

    def redo(self):
        """Called by the QUndoStack to perform the command"""
        self.model.unsubscribe(self.eventName, self.sourceName, self.destinationName, self.callback)

    def undo(self):
        """Called by the QUndoStack to undo the command"""
        self.model.subscribe(self.eventName, self.sourceName, self.destinationName, self.callback, self.old.breakpoint)


class MoveBlockItem(QtGui.QUndoCommand):
    """
    QUndoCommand for moving a BlockItem (Task or Group)
    """
    def __init__(self, controller, itemName, position, description):
        super(MoveBlockItem, self).__init__(description)
        self.controller = controller
        self.itemName = itemName
        self.position = position
        self.oldPosition = self.controller.nameToItem[self.itemName].pos()

    def redo(self):
        item = self.controller.nameToItem[self.itemName]
        item.setPos(self.position)

    def undo(self):
        item = self.controller.nameToItem[self.itemName]
        item.setPos(self.oldPosition)


class RenameObjectCommand(QtGui.QUndoCommand):
    def __init__(self, model, oldName, newName, description):
        super(RenameObjectCommand, self).__init__(description)
        self.model = model
        self.oldName = oldName
        self.newName = newName

    def redo(self):
        self.model.changeName(self.oldName, self.newName)

    def undo(self):
        self.model.changeName(self.newName, self.oldName)

##############
#
# Undo / Redo Commands (end)
#
##############


class GroupController(GraphScene):
    """
    The GroupController co-ordinates the actions between the user and the underlying BlockModel.

    The GroupController has several class attributes that are used to define object types that will
    be constructed when certain actions are performed.  By changing these attributes, the controller
    can be configured to construct different object types, based on need.  This could be useful when
    a user wants to change the graphical object represented by a Task, for example.

    To operate properly, this class needs a BlockModel instance (set by calling setModel()).
    This item can be set as the Scene for a QGraphicsView by passing an instance of this class to 'setScene()'
    on the view.
    """

    GraphScene.MouseMode.Comment = 2

    # Attributes for setting default placement of graphical items connecting this block to a Task/SubBlock
    BlockProxyConnectionOffset_x = 50
    BlockProxyConnectionOffset_y = 0

    syncParameterWidget = QtCore.pyqtSignal(str)

    def __init__(self, rootPath='.', parent=None):
        super(GroupController, self).__init__(parent)
        self._model = None
        self.nameToItem = {}
        self.itemToName = {}
        self.taskViews = []
        self.comments = []
        self.connectionDrawComplete.connect(self.drawCompleteHandler)
        self.changeNameAction = QtGui.QAction('Change Name', self, triggered=self.changeObjectNameDialog)
        self.rootPath = rootPath
        self.connectionStyle = SubscriptionConnection.ConnectionStyle.Line

    @property
    def model(self):
        """
        Returns: The BlockModel instance
        """
        return self._model

    def addBreakpoint(self, item):
        item.breakpoint = True
        self.undoStack.push(EditSubscriptionCommand(self.model, item.destinationName,
                                                    item.eventName, item.sourceName, item.callback, True, 'Add breakpoint'))

    def addComment(self, text, position):
        """
        Add a Comment

        Args:
            text: str
            position: (float, float)
        """
        comment = Comment(text, pos=position)
        comment.textEditedUndo.connect(self.editCommentUndo)
        self.comments.append(comment)
        self.addItem(comment, store_undo=False)

    def addSubBlock(self, subBlock):
        """
        Add a SubBlock.

        Args:
            subBlock: An instance of a BlockTask object (or sub-class)
        """
        if subBlock.name in self.nameToItem:
            raise Exception("Trying to add an item, but and item with that name already exists")
        self.undoStack.push(AddSubBlockCommand(self.model, subBlock, 'Add sub-block {}'.format(subBlock.name)))

    def addSubBlockFromArgs(self, name, fileName, rootPath=None, pos=(0., 0.)):
        """
        Create and then add a Group object as a SubGroup

        Args:
            name: Instance name of the BlockTask as a string
            fileName: File name that the BlockTask represents as a string
            pos: Position to place the GraphicsItem as a tuple of floats (x, y)

        Returns:
            Name of the add sub-group as a string
        """

        if rootPath is None:
            rootPath = self.rootPath
        path = os.path.join(rootPath, fileName)
        try:
            subGroupDef = openJsonFile(path)
        except IOError as e:
            logger.error('Error adding "{}": {}'.format(name, e))
            return None
        parameters = subGroupDef["group"].get("parameters", {})

        group = Group(name, fileName=fileName)
        group.setParameter(parameters)
        base_name = name
        count = 0
        prev_name = name
        while True:
            new_name = '{}_{}'.format(base_name, count)
            group.changeName(prev_name, new_name)
            try:
                self.addSubBlock(group)
                break
            except Exception:
                prev_name = new_name
                count += 1

        # TODO: Need to check for existence of the key first
        item = self.nameToItem[new_name]
        item.setPos(*pos)
        return new_name

    def addTask(self, task):
        """
        Add a Task instance.

        Args:
            task: Instance of a Task object (or sub-class)
        """
        if task.name in self.nameToItem:
            raise Exception("Trying to add an item, but and item with that name already exists")
        self.undoStack.push(AddTaskCommand(self.model, task, 'Add task {}'.format(task.name)))

    def addTaskFromArgs(self, module, className, name, pos=(0., 0.)):
        """
        Create and add an instance of a Task object.

        Args:
            module: Module 'path' of the Task as a string (e.g. mypackage.subpackage.mymodule)
            className: Name of the class contained in the sub-module as a string (e.g. MyTask)
            name: Instance name as a string
            pos: Position to place the GraphicsItem as a tuple of floats (x, y)

        Returns:
            Name of the added Task as a string
        """
        task = createObject(module, className, args=(name,))
        if not isinstance(task, Task):
            raise ValueError('Trying to addTaskFromArgs with non-Task class')
        base_name = name
        count = 0
        prev_name = name
        while True:
            new_name = '{}_{}'.format(base_name, count)
            task.changeName(prev_name, new_name)
            try:
                self.addTask(task)
                break
            except Exception:
                prev_name = new_name
                count += 1

        item = self.nameToItem[new_name]
        item.setPos(*pos)
        return new_name

    def addTaskViewFromArgs(self, module, className, args=(), kwargs={}, pos=(0., 0.)):
        view = createObject(module, className, args, kwargs)
        if not isinstance(view, TaskView):
            raise ValueError('Trying to addTaskViewFromArgs with non-TaskView class')

        # self.model.statusChanged.connect(view.updateView)
        self.addItem(view)
        view.setPos(*pos)
        self.taskViews.append(view)
        return view

    @QtCore.pyqtSlot(str, str)
    def changeObjectName(self, oldName, newName):
        print 'changeObjectName {} {}'.format(oldName, newName)
        for item, name in self.itemToName.items():
            if isinstance(item, (SubscriptionConnection, Port)):
                split_name = name.split(',')
                if oldName in split_name:
                    split_name[split_name.index(oldName)] = newName
                    tempName = ','.join(split_name)
                    self.itemToName[item] = tempName
                    if isinstance(item, SubscriptionConnection):
                        item.setNamesFromLookupString(tempName)
            if isinstance(item, BlockItem):
                if item.instanceName == oldName:
                    item.instanceName = newName
                    self.itemToName[item] = newName
            else:
                if name == oldName:
                    self.itemToName[item] = newName
        for name, item in self.nameToItem.items():
            if isinstance(item, (SubscriptionConnection, Port)):
                split_name = name.split(',')
                if oldName in split_name:
                    split_name[split_name.index(oldName)] = newName
                    tempName = ','.join(split_name)
                    temp = self.nameToItem.pop(name)
                    self.nameToItem[tempName] = temp
                    if isinstance(item, SubscriptionConnection):
                        item.setNamesFromLookupString(tempName)
            else:
                if name == oldName:
                    temp = self.nameToItem.pop(name)
                    self.nameToItem[newName] = temp
        for view in self.taskViews:
            linkedTaskName = view.getLinkedTask()
            if linkedTaskName == oldName:
                view.setLinkedTask(newName)

    def changeObjectNameDialog(self):
        items = self.selectedItems()
        if len(items) == 1:
            item = items[0]
            if isinstance(item, TaskItem) or isinstance(item, SubGroupItem):
                oldName = self.itemToName[item]
                newName, ok = QtGui.QInputDialog.getText(None, 'New name', 'name', text=oldName)
                if ok:
                    item.instanceName = newName
                    self.model.changeObjectName(oldName, newName)

    def clear(self):
        """
        Clear all elements from the Scene and the model
        """
        GraphScene.clear(self)
        self.model.reset()
        self.nameToItem.clear()
        self.itemToName.clear()
        self.undoStack.clear()

    def contextMenuEvent(self, event):
        """
        General contextMenuEvent handler.

        Args:
            event: Event
        """
        menu = self.getContextMenu(event)
        if menu is not None:
            event.accept()
            menu.exec_(event.screenPos())
        else:
            super(GroupController, self).contextMenuEvent(event)

    def copySelectedItems(self):
        """
        Copy selected items to the clipboard
        """
        clipBoard = QtGui.QApplication.clipboard()

        copiedItems = []
        selectedItems = self.selectedItems()

        for item in selectedItems:
            if isinstance(item, BlockItem):
                name = self.itemToName[item]
                modelChild = self.model.getChild(name)
                definition = {'definition': modelChild.serialize(),
                              'position': {'x': item.pos().x(),
                                           'y': item.pos().y()}}
                copiedItems.append(definition)
            # if isinstance(item, SubscriptionConnection):
            #     print 'subscription:', item

        cursorPos = QtGui.QCursor.pos()
        scenePos = self.views()[0].mapToScene(cursorPos)
        if len(copiedItems) > 0:
            clipBoardData = {
                'scenePos': {'x': scenePos.x(),
                             'y': scenePos.y()},
                'items': copiedItems
            }
            clipBoard.setText(json.dumps(clipBoardData))

    def createProvidesPortItem(self, taskItem):
        """
        Create and return a new ProvidesPort.

        This method is called in response to a Task subscribing to this block.

        Args:
            taskItem: Graphics item representing the Task.

        Returns: A ProvidesPort graphics object.
        """
        item = ProvidesPort(orientation=PortOrientation.Right)
        itemPos = taskItem.scenePos()
        pos = (itemPos.x() - self.BlockProxyConnectionOffset_x, itemPos.y() + self.BlockProxyConnectionOffset_y)
        item.setPos(*pos)
        return item

    def createRequiresPortItem(self, taskItem):
        """
        Create and return a new RequiresPort.

        This method is called in response to this block subscribing to a Task.

        Args:
            taskItem: Graphics item representing the Task.

        Returns: A RequiresPort graphics object.
        """
        item = RequiresPort(orientation=PortOrientation.Left)
        itemPos = taskItem.scenePos()
        pos = (itemPos.x() + self.BlockProxyConnectionOffset_x, itemPos.y() + self.BlockProxyConnectionOffset_y)
        item.setPos(*pos)
        return item

    def createSubscriptionLookupName(self, destinationName, sourceName=None, eventName=None, callback=None, typeName=None):
        """
        Method to generate a Subscription key.

        This method creates a key for a subscription which can later be used for matching.  This method is used
        in storing the lookup key for a specific object representing the subscription.  In the case of subscriptions
        to/from the block, there are potentially several objects that are used in a single subscription (the line and
        the dot).  The order of the arguments allows for incrementally filtering objects that match.

        Args:
            destinationName: name of the destination Task/SubBlock as a string
            eventName: name of the event as a string
            sourceName: name of the source Task/SubBlock as a string
            callback: name of the callback method as a string
            typeName: name of the type of object representing the connection as a string

        Returns: Subscription lookup name as a string
        """
        name = destinationName
        if sourceName:
            name = ','.join([name, sourceName])
            if eventName:
                name = ','.join([name, eventName])
                if callback:
                    name = ','.join([name, callback])
                    if typeName:
                        name = ','.join([name, typeName])
        return str(name)

    def createSubGroupItem(self, name, className):
        """
        Create a new item to represent a SubBlock
        Args:
            name: Instance name of the SubBlock
            className: Class name of the subblock

        Returns: QGraphicsItem
        """
        item = SubGroupItem(name, className)
        item.instanceNameEdited.connect(self.objectNameEdited)
        return item

    def createSubscriptionDialog(self, sourceName, destinationName):
        """
        Open a dialog box for the user to input information for a subscription.

        Args:
            sourceName: name of the source Task as a string
            destinationName: name of the destination Task as a string
        """
        events = self.model.getEventNames(str(sourceName))
        eventComboBox = QtGui.QComboBox()
        eventComboBox.addItems(events)

        callbacks = self.model.getCommandNames(destinationName)
        callbackComboBox = QtGui.QComboBox()
        callbackComboBox.addItems(callbacks)

        dialog = DictionaryDialog('{}-->{}'.format(sourceName, destinationName))
        dialog.addSetting('event', ComboBoxInputWidget(comboBox=eventComboBox))
        dialog.addSetting('callback', ComboBoxInputWidget(comboBox=callbackComboBox))

        if dialog.exec_() == QtGui.QDialog.Accepted:
            data = dialog.getData()
            self.undoStack.push(SubscribeCommand(self.model, destinationName, data['event'], sourceName, data['callback'], False, 'Subscribe'))

    def createTaskItem(self, name, className):
        """
        Create and return a graphics item representing a Task.

        By default, this is a rounded blue box.

        Args:
            name: Instance name of the Task as a string
            className: Class name of the Task as a string

        Returns: A TaskTypeItem object.
        """
        item = TaskItem(name, className)
        item.instanceNameEdited.connect(self.objectNameEdited)
        return item

    def deleteItems(self, items):
        """
        Delete items.

        This method is called in response to the user manually deleting items, usually by selecting one or
        more items and pressing 'delete' on the keyboard.

        Args:
            items: List of graphics items
        """
        self.undoStack.beginMacro('Delete Items')
        for item in items:
            if item in self.itemToName.keys():
                name = self.itemToName[item]
                if isinstance(item, TaskItem) or isinstance(item, SubGroupItem):
                    subscriptions = self.model.getSubscriptions()
                    self.undoStack.beginMacro('Remove Task')
                    for subscription in subscriptions:
                        eventName = subscription.eventName
                        destination = subscription.destination.split('/')[-1]
                        if destination == self.model.name:
                            destination = self.model.BlockAlias
                        source = subscription.source.split('/')[-1]
                        if source == self.model.name:
                            source = self.model.BlockAlias
                        callback = subscription.callback
                        if name in [source, destination]:
                            self.undoStack.push(UnsubscribeCommand(self.model, destination, eventName, source, callback, 'Unsubscribe'))
                    self.undoStack.push(MoveBlockItem(self, name, item.scenePos(), 'Move Item'))
                    self.undoStack.push(RemoveTaskCommand(self.model, self.model.getChild(name), "Remove Task"))
                    self.undoStack.endMacro()

                elif isinstance(item, SubGroupItem):
                    self.undoStack.push(RemoveSubBlockCommand(self.model, self.model.getChild(name), "Remove Task"))

                elif isinstance(item, SubscriptionConnection):
                    subscriberName, source, eventName, callback, typeName = name.split(',')
                    self.undoStack.beginMacro('Unsubscribe')
                    self.undoStack.push(UnsubscribeCommand(self.model, subscriberName, eventName, source, callback, 'Unsubscribe'))
                    self.undoStack.endMacro()

                elif isinstance(item, Port):
                    subscriberName, source, eventName, callback, typeName = name.split(',')
                    # self.undoStack.push(UnsubscribeCommand(self.model, subscriberName, eventName, source, callback, 'Unsubscribe'))

            elif isinstance(item, TaskView):
                item.unlinkTask()
                if item in self.taskViews:
                    self.taskViews.remove(item)
                self.removeItem(item)

            elif isinstance(item, Comment):
                pos = (item.scenePos().x(), item.scenePos().y())
                self.undoStack.push(RemoveCommentCommand(self, item.text(), pos, 'Removing comment {}'.format(item.text())))

            # this is the case where we delete a port that is not a block proxy (like a port within a Task node block)
            elif isinstance(item, Port):
                connectionsToDelete = []
                for connection in item.connections:
                    connectionsToDelete.append(connection)
                self.undoStack.beginMacro("Unsubscribe")
                self.deleteItems(connectionsToDelete)
                self.removeItem(item, store_undo=False)
                self.undoStack.endMacro()
            else:
                self.removeItem(item)
        self.undoStack.endMacro()

    def deserialize(self, modelDefinition, viewDefinition=None):
        """
        Populate the scene from a model definition and a view definition.

        This method will completely clear / wipe-out the current contents of the scene and model and populate it with
        the modelDefinition and the viewDefinition.

        The modelDefinition is a Python dictionary which can be generated by calling the 'serialize' method on a Block
        model, or the 'serialize' method of this class.  This dictionary is all that is necessary to recreate a serialized
        block representation.

        The viewDefinition is a Python dictionary which can be generated by calling the 'serialize' method of this
        class.  It encodes information about placement of graphical objects within the scene.  If this definition is
        missing (or just None), the scene will still be populated, and placed auto-matically on the page in default
        locations.

        Args:
            modelDefinition: Representation of the model as a Python dictionary
            viewDefinition: Representation of the view as a Python dictionary.

        Returns:

        """
        self.deserializeModel(modelDefinition)
        if viewDefinition:
            self.deserializeView(viewDefinition)
        self.updateStatus()

    def deserializeModel(self, definition):
        """
        Clear the view and populate the model from a serialized definition

        Args:
            definition: Serialized model as a Python dictionary

        Returns:

        """
        self.clear()
        self.model.deserialize(definition)

    def deserializeView(self, definition):
        """
        Move the items in the scene based on a view definition.

        Args:
            definition: Serialized view as a Python dictionary

        Returns:

        """
        blockItems = definition.get('BlockItems', {})
        for name, itemDef in blockItems.items():
            try:
                item = self.nameToItem[str(name)]
                item.deserialize(itemDef)
            except Exception:

                errorBox = getExceptionDialog(title='Deserialize View Exception',
                                              message='Error while deserializing item "{}":\n'.format(name))

        taskViews = definition.get('TaskViews', [])
        for taskView in taskViews:
            try:
                module = taskView['module']
                className = taskView['class']
                args = taskView['args']
                kwargs = taskView['kwargs']
                linkedTask = taskView['linkedTask']
                position = taskView['position']
                if linkedTask in self.nameToItem.keys() or linkedTask is None:
                    view = self.addTaskViewFromArgs(module, className, args, kwargs, position)
                    view.setLinkedTask(linkedTask)
                else:
                    msg =  'Trying to deserialize TaskView with no matching task.  This view will not be added to the scene\n'
                    msg += '    module={}\n'.format(module)
                    msg += '    className={}\n'.format(className)
                    msg += '    args={}\n'.format(args)
                    msg += '    kwargs={}\n'.format(kwargs)
                    msg += '    linkedTask={}\n'.format(linkedTask)

                    errorbox = QtGui.QMessageBox.critical(None, 'Deserialize View Error', msg)

            except Exception:
                errorBox = getExceptionDialog(title='Deserialize View Exception',
                                              message='Error while deserializing view:\n')

        comments = definition.get('Comments', [])
        for comment in comments:
            try:
                text = comment['text']
                position = comment['position']
                self.addComment(text, position)

            except Exception:
                errorBox = getExceptionDialog(title='Deserialize View Exception',
                                              message='Error while deserializing view:\n')

    @QtCore.pyqtSlot('QGraphicsItem', 'QGraphicsItem', 'QGraphicsItem')
    def drawCompleteHandler(self, sourceItem, destinationItem, connectionItem):
        """
        Handler for when the user connects two valid objects with the draw connection tool.

        If both items are Tasks, this will open a dialog to fill in information for a subscription.

        Args:
            sourceItem: Graphical item at the source of the connection
            destinationItem: Graphical item at the destination of the connection
            connectionItem: Graphical item (the line) that connects the two items
        """
        self.removeItem(connectionItem, store_undo=False)
        sourceName = self.itemToName[sourceItem]
        destinationName = self.itemToName[destinationItem]
        self.createSubscriptionDialog(sourceName, destinationName)

    def dropEvent(self, event):
        """
        Event handler for a dropEvent.

        A drop event occurs when a user does a 'drag-and-drop' from some other source.  If the MIME object contains text
        data, this method will try to deserialize as if it were a JSON string with the following structure:

         For drag-and-drop events, this model provides serialized JSON dictionary as a string with the following format:
        {
            "rootPath" : <string>,
            "relativePaths" : [ <string>, ... ]
        }

        To convert back to a dictionary, use taskforce_common.utils.stringToJson

        For conversion to occur, this event handler expects a unix-style file path to either a '.py' file or a '.group' file.
        If it is a '.py' file, the file will be converted to a module path.

        Example:
            'my_library/my_package/my_module.py' --> 'my_library.my_package.my_module'

        A Task will then be added to the scene representing the class within that module.

        If the file is a '.group' file, a SubBlock will be created representing the BlockTask represented by that file.

        Args:
            event: dropEvent
        """
        try:
            mimeData = event.mimeData()
            # formats = mimeData.formats()
            # if mimeData.hasUrls():
                # urls = mimeData.urls()

            if mimeData.hasText():
                data = stringToJson(mimeData.text())

                rootPath = data["rootPath"]
                relativePath = data["relativePaths"][0]
                # typeName = os.path.basename(relativePath).split('.')[0]
                pos = (event.scenePos().x(), event.scenePos().y())

                if relativePath.endswith('.py'):
                    module = relativePath.split('.py')[0].replace('/', '.')
                    className = module.split('.')[-1]
                    name = className

                    classDef = getClassDefinition(module, className, forceReload=True)

                    if issubclass(classDef, Task):
                        self.addTaskFromArgs(module, className, name, pos)

                    elif issubclass(classDef, TaskView):
                        self.addTaskViewFromArgs(module, className, pos=pos)
                    else:
                        raise Exception('Trying to add invalid code block.  Code must be subclass of Task or TaskView.')

                elif relativePath.endswith(BLOCK_FILE_EXTENSION):
                    name = os.path.basename(relativePath).rsplit('.', 1)[0]
                    self.addSubBlockFromArgs(name, relativePath, rootPath, pos)
        except Exception:
            getExceptionDialog(title='Drop Event Exception', message='Exception was raised during a drop event:')

    @QtCore.pyqtSlot(object, str, str)
    def editCommentUndo(self, comment, pretext, posttext):
        self.undoStack.push(EditCommentCommand(comment, pretext, posttext, 'Editing comment'))

    def getContextMenu(self, event):
        item = self.getTaskItemAtPoint(event.scenePos())
        if isinstance(item, (TaskItem, SubGroupItem)):
            return self.getTaskContextMenu(item, event)
        elif isinstance(item, SubscriptionConnection):
            return self.getSubscriptionConnectionContextMenu(item, event)
        else:
            return None

    def getItemFromName(self, name):
        return self.nameToItem[name]

    def getNameFromItem(self, item):
        """
        The the name of a graphics item.

        Args:
            item: Graphics item in the scene

        Returns: Name of the item as a string
        """
        return self.itemToName.get(item, None)

    def getSelectedTask(self):
        """
        Get the selected Task/SubBlock object from the model.

        If more than one item is selected, or no Tasks / SubBlocks are selected, this method will return 'None'

        Returns:
            Task/SubBlock object, or None
        """
        name = self.getSelectedTaskName()
        task = None
        if name is not None:
            task = self.model.getBlocks(name)
        return task

    def getSelectedTaskName(self):
        """
        Get the name of the currently selected Task / SubBlock.

        If more than one item is selected, or no Tasks / SubBlocks are selected, this method will return 'None'.
        If no items are selected, return the name of the Group represented in the model.

        Returns:
            Name of the Task/SubBlock as a string or None
        """
        name = None
        selection = self.selectedItems()
        if len(selection) == 1:
            item = selection[0]
            if isinstance(item, TaskItem) or isinstance(item, SubGroupItem):
                name = self.getNameFromItem(item)
        return name

    def getSelectedTaskParameters(self):
        """
        Get the parameters from the selected Task / SubBlock.

        If more than one item is selected, or no Tasks / SubBlocks are selected, this method will return 'None'

        Returns:
            Parameters of the Task/SubBlock or None
        """
        task = self.getSelectedTask()
        if task is not None:
            return task.getParameter()

    def getSubscriptionConnectionContextMenu(self, item, event):
        """
        Get a context menu for the selected subscription connection

        This method is called from the general 'contextMenuEvent' method in this class.

        Args:
            item: SubscriptionConnection
            event: Event
        """
        #menu = QtGui.QMenu('Subscription')
        menu = item.getContextMenu(event)
        menu.addSeparator()
        if not item.breakpoint:
            addBreakpointAction = QtGui.QAction('Add breakpoint', self,
                                                triggered=lambda: self.addBreakpoint(item))
            menu.addAction(addBreakpointAction)

        else:
            removeBreakpointAction = QtGui.QAction('Remove breakpoint', self,
                                                   triggered=lambda: self.removeBreakpoint(item))
            menu.addAction(removeBreakpointAction)
        return menu

    def getTaskFullPath(self, taskName):
        return '/'.join([self.model.name, taskName])

    def getTaskNameAtPoint(self, point):
        item = self.getTaskItemAtPoint(point)
        if item is not None:
            return self.itemToName[item]

    def getTaskItemAtPoint(self, point):
        item = self.itemAt(point, QtGui.QTransform())
        if item is not None:
            if isinstance(item, TaskView):
                return None
            if isinstance(item, TaskItem) or isinstance(item, SubGroupItem) or isinstance(item, SubscriptionConnection):
                return item
            else:
                item = item.parentItem()
        return item

    def getTaskContextMenu(self, item, event):
        """
        Get a context menu for the selected task

        This method is called from the general 'contextMenuEvent' method in this class.

        Args:
            item: TaskItem or SubgroupItem
            event: Event
        """
        menu = QtGui.QMenu('Task')

        name = self.itemToName[item]

        connectBlockToTaskDialogAction = QtGui.QAction('Connect {} --> {}'.format(self.model.BlockAlias, name), self,
                                                       triggered=lambda: self.createSubscriptionDialog(self.model.BlockAlias, name))
        connectTaskToBlockDialogAction = QtGui.QAction('Connect {} --> {}'.format(name, self.model.BlockAlias), self,
                                                       triggered=lambda: self.createSubscriptionDialog(name, self.model.BlockAlias))

        menu.addAction(connectBlockToTaskDialogAction)
        menu.addAction(connectTaskToBlockDialogAction)
        menu.addSeparator()

        #  task = self.model.getBlocks(name)

            # TODO: Implement this
            # if task.getBreakPoint():
            #     breakPointAction = QtGui.QAction('clear &breakpoint', self,triggered=lambda: self.setTaskBreakPoint(name, False))
            # else:
            #     breakPointAction = QtGui.QAction('set &breakpoint', self,triggered=lambda: self.setTaskBreakPoint(name, True))
            # menu.addAction(breakPointAction)

        itemMenu = item.getContextMenu(event)
        if itemMenu is not None:
            menu.addSeparator()
            itemMenu.setParent(self.views()[0])
            menu.addActions(itemMenu.actions())

        port = item.getPortFromPos(event.scenePos())
        if port is not None:
            removePortAction = QtGui.QAction('Remove Port', self, triggered=lambda: self.deleteItems([port]))
            menu.addAction(removePortAction)

        return menu

    def getTaskParameters(self, taskName):
        """
        Get the parameters for a Task

        Args:
            taskName: name of the Task as a string

        Returns:
            Dictionary of parameters, or None if the Task does not exist.
        """
        try:
            task = self.model.getBlocks(str(taskName))
        except GroupException:
            return None

        if task is not None:
            return task.getParameter()
        else:
            return None

    def keyPressEvent(self, event):
        """
        Event handler for key presses.

        This method has been overloaded to allow users to remove items by
        using the "delete" or "backspace" keys on their keyboard

        @type event QKeyEvent
        @parameter event The key event
        """
        if event.key() in [QtCore.Qt.Key_Delete, QtCore.Qt.Key_Backspace]:
            focusItem = self.focusItem()

            # If a QGraphicsTextItem is being edited, do not capture the keypress.
            # If it is captured, the user cannot use delete or backspace while editing.
            if not isinstance(focusItem, QtGui.QGraphicsTextItem):
                items = self.selectedItems()
                itemsToRemove = []
                for item in items:
                    # only delete top-level items
                    if hasattr(item, 'deleteable'):
                        if item.deleteable:
                            itemsToRemove.append(item)
                    else:
                        itemsToRemove.append(item)
                if len(itemsToRemove) > 0:
                    self.deleteItems(itemsToRemove)
                event.accept()
                return
        elif event.matches(QtGui.QKeySequence.Copy):
            self.copySelectedItems()
            event.accept()
            return

        elif event.matches(QtGui.QKeySequence.Paste):
            self.pasteFromClipboard()
            event.accept()
            return

        super(GroupController, self).keyPressEvent(event)

    def mousePressEvent(self, event):
        """
        Overload mouse press event so that we can add comments when in comment mode

        @type event QMouseEvent
        @parameter event Incoming mouse event
        """
        if self.mouseMode == GraphScene.MouseMode.Comment:
            if event.button() == QtCore.Qt.LeftButton:
                pos = (event.scenePos().x(), event.scenePos().y())
                self.undoStack.push(AddCommentCommand(self, "comment", pos, 'Adding comment'))
            event.accept()
            return
        super(GroupController, self).mousePressEvent(event)

    @QtCore.pyqtSlot(str)
    def objectRemoved(self, name):
        """
        Remove a task by name. Connected to the model's "objectRemoved" signal

        Args:
            name: Name of the Task/SubBlock
        """
        name = str(name)
        # remove all connections that went to this item, since they won't be deleted by the unsubscribe signals
        for item, itemName in list(self.itemToName.items()):
            if isinstance(item, SubscriptionConnection):
                destinationName, sourceName, eventName, callback, typeName = itemName.split(',')
                if destinationName == name or sourceName == name:
                    self.removeSubscriptionConnection(item)

        # remove any linked tasks
        for view in self.taskViews:
            if view.getLinkedTask() == name:
                self.taskViews.remove(view)
                self.removeItem(view, store_undo=False)

        # remove the main object
        item = self.nameToItem.pop(name)
        self.itemToName.pop(item)
        self.removeItem(item, store_undo=False)

    def pasteFromClipboard(self):
        """
        Paste Blocks from the clipboard
        """
        clipboard = QtGui.QApplication.clipboard()
        try:
            clipBoardData = json.loads(clipboard.text())
        # Chances are, the data you have is not valid for pasting
        except ValueError:
            logging.error('Error pasting.  Invalid data:{}'.format(clipboard.text()))
            return

        copyPos = clipBoardData['scenePos']
        eventPos = self.views()[0].mapToScene(QtGui.QCursor.pos())
        delta = [copyPos['x'] - eventPos.x(), copyPos['y'] - eventPos.y()]
        copiedItems = clipBoardData['items']

        for item in copiedItems:
            definition = item['definition']
            position = item['position']
            taskDefinition = definition.get('task', None)
            groupDefinition = definition.get('group', None)
            if taskDefinition:
                name = taskDefinition['name']
                module = taskDefinition['module']
                className = taskDefinition['class']
                parameters = taskDefinition['parameters']
                pos = (position['x'] - delta[0], position['y'] - delta[1])
                name = self.addTaskFromArgs(module, className, name, pos)
                self.setTaskParameters(name, parameters)
            elif groupDefinition:
                name = groupDefinition['name']
                fileName = groupDefinition['fileName']
                parameters = groupDefinition['parameters']
                pos = (position['x'] - delta[0], position['y'] - delta[1])
                name = self.addSubBlockFromArgs(name, fileName, pos=pos)

    def removeBreakpoint(self, item):
        item.breakpoint = False
        self.undoStack.push(EditSubscriptionCommand(self.model, item.destinationName,
                                                    item.eventName, item.sourceName, item.callback, False, 'Remove breakpoint'))

    def removeComment(self, text, position):
        for comment in list(self.comments):
            pos = (comment.scenePos().x(), comment.scenePos().y())
            if comment.text() == text and position == pos:
                self.removeItem(comment, store_undo=False)
                self.comments.remove(comment)
                break

    def removeSubscriptionConnection(self, item):
        deletePorts = []

        if item.startItem:
            if item.startItem.numConnections == 1:
                deletePorts.append(item.startItem)
        if item.endItem:
            if item.endItem.numConnections == 1:
                deletePorts.append(item.endItem)
        name = self.itemToName.pop(item)
        self.nameToItem.pop(name)
        self.removeItem(item, store_undo=False)
        for port in deletePorts:
            self.removeItem(port, store_undo=False)
            if port in self.itemToName:
                name = self.itemToName.pop(port)
                self.nameToItem.pop(name)

    def serialize(self):
        """
        Serialize the model and the view into Python dictionaries

        Returns:
            Tuple of (modelDefinition, viewDefinition).
            Each definition is a python dictionary
        """
        modelDefinition = self.serializeModel()
        viewDefinition = self.serializeView()
        return modelDefinition, viewDefinition

    def serializeModel(self):
        """
        Seriailize the model into a Python dictionary.

        Returns: Python dictionary
        """
        return self.model.serialize()

    def serializeView(self):
        """
        Seriailize the view as a Python dictionary.

        Returns: Python dictionary
        """
        definition = {}
        blockItems = {}
        for name, item in self.nameToItem.items():
            blockItems[name] = item.serialize()
        taskViews = self.serializeTaskViews()
        comments = self.serializeComments()
        definition['BlockItems'] = blockItems
        definition['TaskViews'] = taskViews
        definition['Comments'] = comments
        return definition

    def serializeComments(self):
        """
        Serialize all the comments as a Python Dictionary
        Returns: List of Python Dictionary
        """
        definition = []
        for comment in self.comments:
            comment_def = {}
            position = (comment.scenePos().x(), comment.scenePos().y())
            comment_def['text'] = comment.text()
            comment_def['position'] = position
            definition.append(comment_def)
        return definition

    def serializeTaskViews(self):
        """
        Serialize all the TaskViews as a Python Dictionary
        Returns: List of Python Dictionaries
        """
        definition = []
        for view in self.taskViews:
            d = view.serialize()
            if d is not None:
                definition.append(d)
        return definition

    def setModel(self, model):
        """
        Set the model for the view

        Args:
            model: A GroupModel instance
        """
        if not isinstance(model, GroupModel):
            raise Exception("Setting model to wrong type.  Must be a BlockModel.")
        self._model = model
        self.model.taskAdded.connect(self.taskAdded)
        self.model.subGroupAdded.connect(self.subBlockAdded)
        self.model.blockRemoved.connect(self.objectRemoved)
        self.model.blockNameChanged.connect(self.changeObjectName)
        self.model.subscribeEvent.connect(self.subscribeEvent)
        self.model.unsubscribeEvent.connect(self.unsubscribeEvent)
        self.model.unsubscribeFromSourceEvent.connect(self.unsubscribeFromSourceEvent)
        self.model.statusChanged.connect(self.updateStatus)

    def setMouseModeComment(self):
        print('set mouse mode comment')
        self.setMouseMode(GraphScene.MouseMode.Comment)

    def setConnectionStyle(self, style):
        self.connectionStyle = style
        self.setNewConnectionKwargs({'style': self.connectionStyle})

    def setSelectedTaskParameters(self, parameters):
        """
        Set the parameters of the currently selected Task

        Args:
            parameters: dictionary of parameter values
        """
        selectedTask = self.getSelectedTask()
        if selectedTask is not None:
            self.undoStack.push(SetParametersCommand(selectedTask, parameters, 'Set parameters on {}'.format(selectedTask.name)))

    def setTaskBreakPoint(self, taskName, enable):
        """
        Set the breakpoint for a Task

        Args:
            taskName: Name of the Task as a string
            enable: State of the breakpoint as a boolean
        """
        self.model.setBreakPoint(taskName, enable)

    def setTaskParameters(self, taskName, parameters):
        """
        Set the parameters of a Task

        Args:
            taskName: Name of the Task as a string
            parameters: Parameters as a Python dictionary

        Raises:
            GroupException: If there is an exception thrown trying to get the Task.  This will also pop up an error box
                            which must be dismissed by the user.
        """
        try:
            task = self.model.getBlocks(taskName)
        except GroupException:
            title = 'Set Task Parameter Exception'
            message = 'Error while trying to set parameters on "{}"\nThe name of the block may have changed.\n\
                       Try re-selecting the block in the editor window.\n'.format(taskName)
            errorBox = QtGui.QMessageBox.critical(None, title, message)
            return
        if isinstance(task, Group):
            self.undoStack.push(SetParametersCommand(task, parameters, True, self.syncParameterWidget, 'Set parameters on {}'.format(task.name)))
        else:
            self.undoStack.push(SetParametersCommand(task, parameters, False, self.syncParameterWidget, 'Set parameters on {}'.format(task.name)))

    @QtCore.pyqtSlot(str, str)
    def subBlockAdded(self, name, className):
        """
        Slot for when a SubBlock has been added to the model.

        This will create a graphics item representing the added SubBlock.

        Args:
            name: instance name of the SubBlock
            className: Class name of the subblock
        """
        name = str(name)
        className = str(className)

        item = self.createSubGroupItem(name, className)
        self.addItem(item, store_undo=False)
        self.nameToItem[name] = item
        self.itemToName[item] = name

    @QtCore.pyqtSlot(str, str, str, str, bool)
    def subscribeEvent(self, eventName, sourceName, destinationName, callback, breakpoint):
        """
        Subscribe to an event.  This slot is connected to the model's "subscribeEvent" signal, and should not be called
        directly.

        Args:
            destinationName: name of the destination Task/SubBlock as a string
            eventName: name of the event as a string
            sourceName: name of the source Task/SubBlock as a string
            callback: name of the callback method as a string
            breakpoint: bool of whether or not this subscription has a breakpoint
        """
        sourcePort = None
        destinationPort = None
        if sourceName == self.model.BlockAlias:
            destinationItem = self.nameToItem[destinationName]
            destinationPort = destinationItem.getDestinationPort(callback)

            sourcePortName = self.createSubscriptionLookupName(destinationName, sourceName, eventName, callback, ProvidesPort.__name__)
            if sourcePortName in self.nameToItem:
                sourcePort = self.nameToItem[sourcePortName]
            else:
                sourcePort = self.createProvidesPortItem(destinationPort)
                sourcePort.name = eventName
                self.itemToName[sourcePort] = sourcePortName
                self.nameToItem[sourcePortName] = sourcePort
                self.addItem(sourcePort)

        elif destinationName == self.model.BlockAlias:
            sourceItem = self.nameToItem[sourceName]
            sourcePort = sourceItem.getSourcePort(eventName)

            destinationPortLookupName = self.createSubscriptionLookupName(destinationName, sourceName, eventName, callback, RequiresPort.__name__)
            if destinationPortLookupName in self.nameToItem:
                destinationPort = self.nameToItem[destinationPortLookupName]
            else:
                destinationPort = self.createRequiresPortItem(sourcePort)
                destinationPort.name = callback
                self.itemToName[destinationPort] = destinationPortLookupName
                self.nameToItem[destinationPortLookupName] = destinationPort
                self.addItem(destinationPort)

        else:
            sourceItem = self.nameToItem[sourceName]
            destinationItem = self.nameToItem[destinationName]

            sourcePort = sourceItem.getSourcePort(eventName)
            destinationPort = destinationItem.getDestinationPort(callback)

        connection = SubscriptionConnection(destinationName, eventName, sourceName, callback, breakpoint, style=self.connectionStyle)
        connectionName = self.createSubscriptionLookupName(destinationName, sourceName, eventName, callback, SubscriptionConnection.__name__)

        if connectionName in self.nameToItem:
            self.nameToItem[connectionName].breakpoint = breakpoint
        else:
            self.connectItems(sourcePort, destinationPort, connection)
            self.itemToName[connection] = connectionName
            self.nameToItem[connectionName] = connection

    @QtCore.pyqtSlot(str, str)
    def taskAdded(self, name, className):
        """
        Adds a task.  This method is connected to the model's "taskAdded" signal, and should not be called directly.

        Args:
            name: instance name of the task
            className: classname of the task
        """
        name = str(name)
        className = str(className)

        # don't create a graphic for the actual model
        if not name == self.model.BlockAlias:
            item = self.createTaskItem(name, className)
            self.addItem(item, store_undo=False)
            self.nameToItem[name] = item
            self.itemToName[item] = name

    @QtCore.pyqtSlot(object, str, str)
    def objectNameEdited(self, blockItem, oldName, newName):
        """
        Slot callback for when an object name has been edited.

        This slot is triggered when the name of an object has been changed, usually by the user.

        Args:
            blockItem: item whose name changed
            oldName: original name of the block (before editing) as a string
            newName: new name of the block (after editing) as a string
        """
        if newName in self.model.getChildren(recursive=False).keys():
            blockItem.instanceName = oldName
            msg = 'Error changing "{}" to "{}".  There is already a Block with that name'.format(oldName, newName)
            QtGui.QMessageBox.critical(None, 'Change name error', msg)
            return
        blockItem.instanceName = oldName
        self.undoStack.push(RenameObjectCommand(self.model, oldName, newName, 'Change name'))

    @QtCore.pyqtSlot(str, str, str, str)
    def unsubscribeEvent(self, eventName, source, destination, callback):
        """
        Unsubscribe an event.  This slot is connected to the model's "unsubscribeEvent" method, and should to be
        called directly.

        Args:
            destinationName: name of the destination Task/SubBlock as a string
            eventName: name of the event as a string
            sourceName: name of the source Task/SubBlock as a string
        """
        destinationName = str(destination)
        eventName = str(eventName)
        sourceName = str(source)

        name = self.createSubscriptionLookupName(destinationName, sourceName, eventName)
        for key in self.nameToItem.keys():
            if key.startswith(name):
                if key in self.nameToItem:  # Additonal check because more than one thing can be deleted in a remove subscription
                    item = self.nameToItem[key]
                    if isinstance(item, SubscriptionConnection):
                        self.removeSubscriptionConnection(item)

    @QtCore.pyqtSlot(str, str)
    def unsubscribeFromSourceEvent(self, destinationName, sourceName):
        """
        Unsubscribe all Tasks/SubBlocks from a particular source.  This slot is connected to the model's
        "unsubscribeFromSourceEvent" method, and should not be called directly.

        Args:
            destinationName: name of the destination Task/SubBlock as a string
            sourceName: name of the source Task/SubBlock as a string
        """
        destinationName = str(destinationName)
        sourceName = str(sourceName)

        name = self.createSubscriptionLookupName(destinationName, sourceName)
        for key in self.nameToItem.keys():
            if key.startswith(name):
                itemToRemove = self.nameToItem.pop(key)
                # nameToRemove = self.itemToName.pop(itemToRemove)
                if isinstance(itemToRemove, SubscriptionConnection):
                    itemToRemove.disconnect()
                self.removeItem(itemToRemove)

    def updateStatus(self):
        """
        Update the status for all Tasks
        """
        for task in self.model.getChildren().values():
            self.updateTaskStatus(task.name, task.status())

    def updateTaskStatus(self, name, status):
        """
        Update the status for a specific Task

        Args:
            name: name of the Task as a string
            status: status of the Task as a dictionary
        """
        item = self.nameToItem.get(name, None)
        if item is not None:
            item.updateStatus(status)
