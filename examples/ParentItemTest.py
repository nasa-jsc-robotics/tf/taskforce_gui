from pyqtlib import QtGui, QtCore
from pyqtlib import PathWidget, DictionaryDialog, ComboBoxInputWidget

import sys, os


from pyqtlib.node_graph import GraphScene, GraphView
from taskforce_gui.BlockItem import BlockItem

class Block(BlockItem):

    def contextMenuEvent(self, event):
        menu = QtGui.QMenu()
        menu.addAction('LinkParent',self.setLinkDialog)
        menu.addAction('Unlink',self.unlink)
        menu.exec_(event.screenPos())


    def setLinkDialog(self):
        allItems = self.scene().getTopLevelItems()

        items = {}
        for item in allItems:
            if isinstance(item,Block) and item != self:
                items[item.instanceName] = item

        taskComboBox = QtGui.QComboBox()
        taskComboBox.addItems(items.keys())

        dialog = DictionaryDialog('Link to Task')
        dialog.addSetting('task', ComboBoxInputWidget(comboBox=taskComboBox))

        if dialog.exec_() == QtGui.QDialog.Accepted:
            data = dialog.getData()
            taskName = data['task']
            self.setParent(items[taskName])

    def setParent(self, item):
        pos = self.mapFromScene(item.scenePos())
        self.setParentItem(item)
        #self.setTransformOriginPoint(pos)
        self.setPos(-pos.x(),-pos.y())

    def unlink(self):
        pos = self.scenePos()
        self.setParentItem(None)
        self.setPos(pos)

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)


        self.view = GraphView()
        self.scene = GraphScene()
        self.view.setScene(self.scene)

        self.addBlockButton = QtGui.QPushButton('Add Block')
        self.addBlockButton.clicked.connect(self.addBlock)

        self.toolBar = QtGui.QToolBar()
        self.toolBar.addWidget(self.addBlockButton)

        self.statusBar().show()

        self.vLine = QtGui.QGraphicsLineItem(0.0,-50.0,0.0,50.0)
        self.hLine = QtGui.QGraphicsLineItem(-50.0, 0.0, 50.0, 0.0)
        self.scene.addItem(self.vLine)
        self.scene.addItem(self.hLine)

        self.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)

        # self.editor = BlockEditor()
        # self.editor.modifiedChanged.connect(self.updateTitle)
        #
        # self.modelTree = ObjectTreeWidget()
        # self.modelTreeDock = QtGui.QDockWidget('Model')
        # self.modelTreeDock.setWidget(self.modelTree)
        #
        self.viewTree = PathWidget()
        self.viewTreeDock = QtGui.QDockWidget('View')
        self.viewTreeDock.setWidget(self.viewTree)
        self.viewTree.setMinimumWidth(400)
        #
        # self.undoView = QtGui.QUndoView(self.editor.controller.getUndoStack())
        # self.undoDock = QtGui.QDockWidget('Undo')
        # self.undoDock.setWidget(self.undoView)
        #
        # self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.modelTreeDock)
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.viewTreeDock)
        # self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.undoDock)
        #
        # self.tabifyDockWidget(self.modelTreeDock, self.viewTreeDock)
        self.num = 0
        self.block0 = self.addBlock()
        block1 = self.addBlock()

        self.block0.setPos(100, 100)
        block1.setPos(-125, -125)


        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateView)
        self.timer.start(20)

        self.setCentralWidget(self.view)


        # self.timer = QtCore.QTimer()
        # self.timer.timeout.connect(self.updateModelTree)
        # self.timer.start(250)

    def addBlock(self):
        block = Block('Block_{}'.format(self.num), 'block')
        self.num += 1
        self.scene.addItem(block)
        return block

    def updateStatusBar(self):
        mousePos = self.view.mapToScene(self.view.mapFromGlobal(self.cursor().pos()))
        self.statusBar().showMessage('scenePos:{},{}'.format(mousePos.x(), mousePos.y()))

    def updateView(self):
        self.updateStatusBar()
        self.updateViewTree()

    def updateViewTree(self):
        viewTree = {}
        for item in self.scene.items():
            if isinstance(item, Block):
                name = item.instanceName
                pos = item.pos()
                scenePos = item.scenePos()
                parent = item.parentItem()
                mapToBlock0 = item.mapToItem(self.block0,scenePos)
                mapFromScene = item.mapFromScene(self.block0.scenePos())
                if isinstance(parent,BlockItem):
                    parentName = parent.getInstanceName()
                else:
                    parentName = 'none'
                self.viewTree.setValue(name, 'parent', parentName)
                self.viewTree.setValue(name, 'scene.x', scenePos.x())
                self.viewTree.setValue(name, 'scene.y', scenePos.y())
                self.viewTree.setValue(name, 'pos.x', pos.x())
                self.viewTree.setValue(name, 'pos.y', pos.y())
                self.viewTree.setValue(name, 'mapToBlock0.x', mapToBlock0.x())
                self.viewTree.setValue(name, 'mapToBlock0.y', mapToBlock0.y())
                self.viewTree.setValue(name, 'mapFromScene.x', mapFromScene.x())
                self.viewTree.setValue(name, 'mapFromScene.y', mapFromScene.y())


        self.viewTree.expandAll()
        self.viewTree.adjustColumns()

if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    QtCore.QCoreApplication.setOrganizationName("NASA");
    QtCore.QCoreApplication.setApplicationName("TaskForce");
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()

    ex.resize(1200,900)
    ex.show()
    sys.exit(app.exec_())
