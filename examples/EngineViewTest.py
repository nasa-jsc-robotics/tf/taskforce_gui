from pyqtlib import QtGui, QtCore
import time

import sys
import signal
from pprint import pformat

from pyqtlib import TreeFilterWidget

from taskforce_model import EngineModel
from taskforce_model import EnginePathModel
from taskforce_gui import EngineView

from taskforce_plugin_zmq import ZMQClient, ZMQSubscriber


class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.engineClient = ZMQClient(port=6555)
        self.engineSubscriber = ZMQSubscriber(port=6556)

        self.engineModel = EngineModel(self.engineClient, self.engineSubscriber)
        self.enginePathModel = EnginePathModel(checkable=False, delimiters='/')
        self.enginePathModel.engineModel = self.engineModel



        # The engineView displays status and allows users to send commands directly to Tasks.
        self.engineView = EngineView()
        self.engineView.setModel(self.enginePathModel)

        # By placing the engineView inside a TreeFilterWidget, we can filter the view with the power of regex
        self.engineViewWidget = TreeFilterWidget()
        self.engineViewWidget.setView(self.engineView)

        self.setCentralWidget(self.engineViewWidget)

if __name__ == "__main__":

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication([])
    mw  = MainWindow()
    mw.show()

    sys.exit(app.exec_())
