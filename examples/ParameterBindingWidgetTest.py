from pyqtlib import QtGui, QtCore
import time

import sys
import signal
from pprint import pformat

from pyqtlib import TreeFilterWidget, ObjectTreeWidget

from taskforce_gui.ParameterBindingWidget import ParameterBindingWidget
from taskforce_common import Group, Task

class TestTask(Task):

    parameters = {
        'param1': True,
        'param2': False
    }

class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.objectTree = ObjectTreeWidget()
        self.objectTree.show()

        task1 = TestTask('task1')
        task2 = TestTask('task2')
        task3 = TestTask('task3')

        self.group = Group('testGroup', 'testGroup.group')

        parameters = {'foo': True,
                      'bar': False,
                      'baz': [1, 2, 3]}
        self.group.setParameter(parameters)
        self.group.addTask(task1)
        self.group.addTask(task2)
        self.group.addTask(task3)

        self.group.bindParameter('foo', 'param1', 'task1')
        self.group.bindParameter('foo', 'param1', 'task2')

        self.parameterBindingsWidget = ParameterBindingWidget()
        self.parameterBindingsWidget.setGroup(self.group)
        self.parameterBindingsWidget.bindingsChanged.connect(self.bindingsChanged)

        self.setCentralWidget(self.parameterBindingsWidget)

        # Start the update timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateTree)
        self.timer.start(200)

    def bindingsChanged(self):
        print 'bindings changed!'

    def updateTree(self):
        self.objectTree.updateView('group', self.group.serialize())

if __name__ == "__main__":

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication([])
    mw  = MainWindow()
    mw.show()

    sys.exit(app.exec_())