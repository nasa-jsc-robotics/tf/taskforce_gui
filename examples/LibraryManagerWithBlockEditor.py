from pyqtlib import QtGui, QtCore
import os


from taskforce_common import BLOCK_FILE_EXTENSION
from taskforce_gui import LibraryView, GroupEditor
from taskforce_model import LibraryModel
import sys

folder = os.path.dirname(__file__)
ROOT_PATH = os.path.join(folder, '..', 'test', 'local_library_root')

class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        blockEditor = GroupEditor(parent=self)

        model = LibraryModel()
        model.setRootPath(ROOT_PATH)
        model.setNameFilters(['*.py', '*' + BLOCK_FILE_EXTENSION])
        model.setNameFilterDisables(False)
        model.setResolveSymlinks(True)

        view = LibraryView()
        view.setModel(model)
        view.setRootIndex(model.index(ROOT_PATH))
        view.setDragEnabled(True)
        view.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        view.hideColumn(1)
        view.hideColumn(2)
        view.hideColumn(3)

        viewDock = QtGui.QDockWidget('Library')
        viewDock.setWidget(view)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea,viewDock)

        self.setCentralWidget(blockEditor)


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    QtCore.QCoreApplication.setOrganizationName("NASA");
    QtCore.QCoreApplication.setApplicationName("TaskForce");
    app = QtGui.QApplication(sys.argv[1:])
    ex = MainWindow()

    #ex = SignalTest()
    ex.resize(1200,600)
    ex.show()
    sys.exit(app.exec_())