from pyqtlib import QtGui, QtCore

import os
import sys
import signal

from taskforce_model import EngineModel
from taskforce_plugin_zmq import ZMQClient, ZMQSubscriber
from taskforce_common import BLOCK_FILE_EXTENSION

DEFAULT_COMMAND_PORT = 6555
DEFAULT_STATUS_PORT = 6556

LOCAL_LIBRARY_ROOT = os.path.join(os.environ['HOME'], '.taskforce_gui_library')
LIBRARY_ROOT = os.path.join(os.environ['HOME'], 'taskforce_ws', 'src', 'taskforce_example_library')


class MainWindow(QtGui.QMainWindow):

    def __init__(self, client, subscriber, parent=None):
        super(MainWindow, self).__init__(parent)

        self.engineModel = EngineModel(clientPort=client, subscriber=subscriber)
        self.engineModel.setLocalLibraryRoot(LOCAL_LIBRARY_ROOT)
        self.engineModel.localLibraryAddLink(LIBRARY_ROOT)

        self.startRobotButton = QtGui.QPushButton('start robot')
        self.startRobotButton.clicked.connect(self.startRobot)

        self.setCentralWidget(self.startRobotButton)

    def startRobot(self):
        self.engineModel.groupDeploy('taskforce_example_library/demos/block_demo' + BLOCK_FILE_EXTENSION)
        self.engineModel.taskStartup('block_demo')


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    subscriber = ZMQSubscriber(port=DEFAULT_STATUS_PORT)
    client = ZMQClient(port=DEFAULT_COMMAND_PORT)

    app = QtGui.QApplication([])
    main = MainWindow(client, subscriber)
    main.show()

    sys.exit(app.exec_())
