from pyqtlib import QtGui, QtCore

import sys
import signal

from taskforce_common import Group, Task


class ComboBoxDelegate(QtGui.QStyledItemDelegate):

    COLUMNS = {
        'name': 0,
        'target': 1,
        'param': 2
    }

    def __init__(self, parent=None):
        super(ComboBoxDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):
        """

        Args:
            parent: parent item as a QWidget
            option: option as a QStyleOptionViewItem
            index: QModelIndex

        Returns:
            QtGui.QComboBox
        """
        if index.column() == ComboBoxDelegate.COLUMNS['param']:
            comboBox = QtGui.QComboBox(parent)
            model = index.model()
            options = model.getOptions(index)
            comboBox.addItems(options)
            return comboBox
        else:
            return QtGui.QStyledItemDelegate.createEditor(self, parent, option, index)


# ComboBoxItemDelegate::ComboBoxItemDelegate(QObject *parent)
#     : QStyledItemDelegate(parent)
# {
# }


# #include "comboboxitemdelegate.h"
# #include <QComboBox>
#
#
# ComboBoxItemDelegate::~ComboBoxItemDelegate()
# {
# }
#
#
# QWidget *ComboBoxItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
# {
#     // Create the combobox and populate it
#     QComboBox *cb = new QComboBox(parent);
#     const int row = index.row();
#     cb->addItem(QString("one in row %1").arg(row));
#     cb->addItem(QString("two in row %1").arg(row));
#     cb->addItem(QString("three in row %1").arg(row));
#     return cb;
# }
#
#
# void ComboBoxItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
# {
#     QComboBox *cb = qobject_cast<QComboBox *>(editor);
#     Q_ASSERT(cb);
#     // get the index of the text in the combobox that matches the current value of the item
#     const QString currentText = index.data(Qt::EditRole).toString();
#     const int cbIndex = cb->findText(currentText);
#     // if it is valid, adjust the combobox
#     if (cbIndex >= 0)
#        cb->setCurrentIndex(cbIndex);
# }
#
#
# void ComboBoxItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
# {
#     QComboBox *cb = qobject_cast<QComboBox *>(editor);
#     Q_ASSERT(cb);
#     model->setData(index, cb->currentText(), Qt::EditRole);
# }





class BindingView(QtGui.QTreeView):

    def __init__(self, parent=None):
        super(BindingView, self).__init__(parent)


class BindingWidget(QtGui.QWidget):

    def __init__(self, parent=None):
        super(BindingWidget, self).__init__(parent)

        self.model = BindingModel()
        self.view = BindingView()

        self.setModel(self.model)

    def setModel(self, model):
        self.view.setModel(model)


class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.group = Group('testGroup', 'testGroup.group')

        parameters = {'foo': True,
                      'bar': False,
                      'baz': [1, 2, 3]}
        self.group.setParameter(parameters)
        self.group.bindParameter('foo', 'param1', 'child1Task')
        self.group.bindParameter('foo', 'param1', 'child2Task')

        task1 = Task('task1')
        task2 = Task('task2')
        task3 = Task('task3')

        task1.setParameter({'task1_param1': 1,
                            'task1_param2': 2})

        task2.setParameter({'task2_param1': 3,
                            'task2_param2': 4})

        task3.setParameter({'task3_param1': 4,
                            'task3_param2': 5})


        self.bindingWidget = BindingWidget()

        self.setCentralWidget(self.bindingWidget)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication([])
    mw = MainWindow()
    mw.show()

    sys.exit(app.exec_())