#!/usr/bin/env python
import sys
from pyqtlib import QtGui, QtCore
from pyqtlib.node_graph import TextItem

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)

        self.lineEdit = QtGui.QLineEdit()
        self.button = QtGui.QPushButton('change text')

        self.button.clicked.connect(self.setText)

        self.textItem = TextItem('test')
        self.textItem.textEdited.connect(self.textChanged)

        self.scene = QtGui.QGraphicsScene()
        self.view = QtGui.QGraphicsView()
        self.view.setScene(self.scene)

        self.scene.addItem(self.textItem)

        widget= QtGui.QWidget()
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.view)
        layout.addWidget(self.lineEdit)
        layout.addWidget(self.button)
        widget.setLayout(layout)
        self.setCentralWidget(widget)
        self.show()

    def setText(self):
        text = self.lineEdit.text()
        self.textItem.setText(text)

    def textChanged(self, pre, post):
        print 'text changed from:{} to: {}'.format(pre, post)

if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.resize(1200,600)
    sys.exit(app.exec_())
