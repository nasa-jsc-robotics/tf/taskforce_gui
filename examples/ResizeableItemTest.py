from pyqtlib import QtGui, QtCore
from pyqtlib import PathWidget, DictionaryDialog, ComboBoxInputWidget

import sys, os

from pyqtlib.node_graph import GraphScene, GraphView
from taskforce_gui.BlockItem import BlockItem
from taskforce_gui.GroupController import GroupController
from taskforce_gui import GroupEditor

# class ResizeableBlock(BlockItem):
#
#     def __init__(self, *args, **kwargs):
#         super(ResizeableBlock, self).__init__(*args, **kwargs)
#
#         self.resizing = False
#         self.in_resize_region = False
#         self.start_resize_pos = self.pos()
#
#     def hoverMoveEvent(self, event):
#         """
#
#         Args:
#             event: QGraphicsSceneHoverEvent
#
#         Returns:
#
#         """
#         resizeMargin = 10
#         bottomRight = self.boundingRect().bottomRight()
#         eventPos = event.pos()
#         if (bottomRight.x() - eventPos.x()) < resizeMargin and  (bottomRight.y() - eventPos.y()) < resizeMargin:
#             print 'hoverEvent  pos:{},{}'.format(event.pos().x(),event.pos().y())
#             self.setCursor(QtCore.Qt.SizeFDiagCursor)
#             self.in_resize_region = True
#         else:
#             self.setCursor(QtCore.Qt.ArrowCursor)
#             self.in_resize_region = False
#
#     def mousePressEvent(self, event):
#         """
#
#         Args:
#             event: QGraphicsSceneMouseEvent
#
#         Returns:
#         """
#         if self.in_resize_region:
#             self.resizing = True
#             self.start_resize_pos = event.pos()
#             self.start_resize_width = self.boxItem.boundingRect().width()
#             self.start_resize_height = self.boxItem.boundingRect().height()
#             self.start_resize_topLeft = self.boxItem.boundingRect().topLeft()
#             print 'start resizing!!!'
#             event.accept()
#             return
#         super(ResizeableBlock, self).mousePressEvent(event)
#
#     def mouseReleaseEvent(self, event):
#         """
#
#         Args:
#             event: QGraphicsSceneMouseEvent
#
#         Returns:
#
#         """
#         if self.resizing:
#             self.resizing = False
#             event.accept()
#             return
#         super(ResizeableBlock,self).mouseReleaseEvent(event)
#
#     def mouseMoveEvent(self, event):
#         """
#
#         Args:
#             event: QGraphicsSceneMouseEvent
#
#         Returns:
#
#         """
#         if self.resizing:
#             delta_x = event.pos().x() - self.start_resize_pos.x()
#             delta_y = event.pos().y() - self.start_resize_pos.y()
#             width = self.start_resize_width + delta_x
#             height = self.start_resize_height + delta_y
#             self.resize(width,height)
#             event.accept()
#             return
#
#         super(ResizeableBlock, self).mouseMoveEvent(event)
#

folder = os.path.dirname(__file__)
ROOT_PATH = os.path.join(folder,'test_root')

sys.path += [ROOT_PATH] + sys.path

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)


        #self.view  = GraphView()
        #self.scene = GraphScene()
        #self.scene = BlockController()
        #self.view.setScene(self.scene)
        self.tabWidget = QtGui.QTabWidget()
        self.editor = GroupEditor()
        self.tabWidget.addTab(self.editor,'editor')
        self.scene = self.editor.controller

        self.addBlockButton = QtGui.QPushButton('Add Block')
        self.addBlockButton.clicked.connect(self.addBlock)

        self.addBlockDialog = QtGui.QPushButton('addBlockDialog')
        self.addBlockDialog.clicked.connect(self.editor.addTaskDialog)

        self.toolBar = QtGui.QToolBar()
        self.toolBar.addWidget(self.addBlockButton)
        self.toolBar.addWidget(self.addBlockDialog)

        self.statusBar().show()

        self.vLine = QtGui.QGraphicsLineItem(0.0,-50.0,0.0,50.0)
        self.hLine = QtGui.QGraphicsLineItem(-50.0, 0.0, 50.0, 0.0)
        self.scene.addItem(self.vLine)
        self.scene.addItem(self.hLine)

        self.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.setCentralWidget(self.tabWidget)

        self.num = 0

    def addBlock(self):
        block = BlockItem('Block_{}'.format(self.num), 'block')
        self.num += 1
        self.scene.addItem(block)
        return block

    # def addResizeableBlock(self):
    #     block = ResizeableBlock('Block_{}'.format(self.num), 'block')
    #     self.num += 1
    #     self.scene.addItem(block)
    #     return block

if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    QtCore.QCoreApplication.setOrganizationName("NASA");
    QtCore.QCoreApplication.setApplicationName("TaskForce");
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()

    ex.resize(1200,900)
    ex.show()
    sys.exit(app.exec_())