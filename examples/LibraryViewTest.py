#!/usr/bin/env python
from pyqtlib import QtCore, QtGui
from pprint import pprint
from taskforce_gui import LibraryView
from taskforce_model import LibraryModel
from taskforce_common import BLOCK_FILE_EXTENSION

import os

folder = os.path.dirname(__file__)
ROOT_PATH = os.path.join(folder, '..', 'test', 'local_library_root')

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        model = LibraryModel()
        model.setNameFilters(['*.py', '*' + BLOCK_FILE_EXTENSION])
        model.setNameFilterDisables(False)
        model.setResolveSymlinks(True)
        model.setRootPath(ROOT_PATH)

        view = LibraryView()
        view.setModel(model)
        view.setRootIndex(model.index(ROOT_PATH))
        view.setDragEnabled(True)
        view.hideColumn(1)
        view.hideColumn(2)
        view.hideColumn(3)

        self.setCentralWidget(view)
        self.show()

        # test signals
        view.deployBlock.connect(self.deployBlock)
        view.openFile.connect(self.openFile)
        view.openFileAsText.connect(self.openFileAsText)
        view.uploadModule.connect(self.uploadModule)

        pprint(model.fileMap)
        pprint(model.reverseFileMap)
        print model.getRelativePath(os.path.join(ROOT_PATH, 'r2_task_library', 'blocks', 'R2SimPose' + BLOCK_FILE_EXTENSION))
        print model.getRelativePath('/usr/local/lib')

    def deployBlock(self, name, path):
        print 'deployBlock signal received: {} {}'.format(name, path)

    def openFile(self, path):
        print 'openFile signal received: {}'.format(path)

    def openFileAsText(self, path):
        print 'openFileAsText signal received: {}'.format(path)

    def uploadModule(self, name, path):
        print 'uploadModule signal received: {} {}'.format(name, path)


def main():
    import signal, sys

    app = QtGui.QApplication([])
    main = MainWindow()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()