from pyqtlib import QtGui, QtCore
import time

import sys
import signal
from pprint import pformat

from taskforce_model import EngineModel
from taskforce_model import EnginePathModel

from taskforce_plugin_zmq import ZMQClient, ZMQSubscriber

class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.client = ZMQClient(port=6555)
        self.subscriber = ZMQSubscriber(port=6556)

        self.engineModel = EngineModel(self.client, self.subscriber)
        self.model = EnginePathModel(checkable=False,delimiters='/')
        self.model.engineModel = self.engineModel
        self.view = QtGui.QTreeView()
        self.view.setModel(self.model)

        # self.model.connectionStateChanged.connect(self.connectionChanged)
        # self.model.statusUpdated.connect(self.statusUpdate)
        # self.textBox = QtGui.QPlainTextEdit()
        #
        #
        # self.shutDownButton = QtGui.QPushButton('shutdown')
        # self.shutDownButton.clicked.connect(self.shutdown)
        #
        # widget = QtGui.QWidget()
        #
        # layout = QtGui.QVBoxLayout()
        # layout.addWidget(self.textBox)
        # layout.addWidget(self.shutDownButton)
        # widget.setLayout(layout)

        self.setCentralWidget(self.view)



    # def connectionChanged(self, value):
    #     print 'Connection changed: {}'.format(value)
    #
    # def statusUpdate(self):
    #     self.textBox.setPlainText(pformat(self.model.getStatus()))
    #     print '{} updated'.format(time.time())
    #
    # def shutdown(self):
    #     self.model.shutdown()


if __name__ == "__main__":

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication([])
    mw  = MainWindow()
    mw.show()

    sys.exit(app.exec_())