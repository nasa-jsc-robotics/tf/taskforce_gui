from pyqtlib import QtGui, QtCore
from pyqtlib import ObjectTreeWidget

import sys, os

from taskforce_gui.GroupEditor import GroupEditor

path = os.path.join(os.environ['HOME'],'taskforce_ws','src')
sys.path = [path] + sys.path
library_root = os.path.join(path,'taskforce_example_library')

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)

        self.editor = GroupEditor()
        self.editor.modifiedChanged.connect(self.updateTitle)

        self.modelTree = ObjectTreeWidget()
        self.modelTreeDock = QtGui.QDockWidget('Model')
        self.modelTreeDock.setWidget(self.modelTree)

        self.viewTree = ObjectTreeWidget()
        self.viewTreeDock = QtGui.QDockWidget('View')
        self.viewTreeDock.setWidget(self.viewTree)

        self.undoView = QtGui.QUndoView(self.editor.controller.getUndoStack())
        self.undoDock = QtGui.QDockWidget('Undo')
        self.undoDock.setWidget(self.undoView)

        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.modelTreeDock)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.viewTreeDock)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.undoDock)

        self.tabifyDockWidget(self.modelTreeDock, self.viewTreeDock)

        self.setCentralWidget(self.editor)


        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateModelTree)
        self.timer.start(250)

    def updateModelTree(self):

        model, view = self.editor.serialize()
        self.modelTree.updateView('Model', model)
        self.viewTree.updateView('View', view)

    def updateTitle(self):
        self.setWindowTitle(self.editor.windowTitle())


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    QtCore.QCoreApplication.setOrganizationName("NASA");
    QtCore.QCoreApplication.setApplicationName("TaskForce");
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()

    #ex = SignalTest()
    ex.resize(1200,600)
    ex.show()
    sys.exit(app.exec_())