Change Log
==========

3.1.4
-----

* Fixed GUI crash when interacting with a deployed group that has been shutdown, but its subgroups are blocks are still deployed.
* Fixed a GUI crash when attempting to open the source python file of a deployed block when the source file is not available in the Library Viewer
* Added ability to add comments on scenes
* Added ability to have group level parameters that can be bound to other group level or task level parameters, as well as the ability to add and remove group parameters from the GUI
* Fixed issue when saving files from LiveViewer where internal file paths would be saved without the level of detail needed
* Made parameter widget update with live values in live viewer, and make setting parameters in live view only set on the live instance of the deployed groups
* Fix calls in EngineModelContextMenu to be nonblocking for startup and init
* Fix bug when removing connections between tasks and groups that leave leftover unused objects in the scene, causing errros when deserializing
* Change new block dialog from file toolbar to new group
* Add support to add and remove breakpoints between tasks.
* Added support for multiple line-styles for subscription connections 

3.1.3
-----

* Added LICENSE.md

3.1.2
-----

* Fixed issue where unittests would hang

3.1.1
-----

* Added missing entries in the CHANGELOG that caused problems with VDD scripts

3.1.0
-----

* Added support for "failed state", which will turn Tasks red at run-time
* Refactored how icons found for GUI toolbars
* Removed some deprecated / unused GUI toolbar icons

3.0.0
-----

* Added application icon to window title bar
* Updated .block/.group name changes in variable references and file names
    * *File Changes*
        * BlockController.py -> `GroupController.py`
        * BlockEditor.py -> `GroupEditor.py`
        * BlockLiveScene.py -> `GroupLiveScene.py`
        * BlockToolBar.py -> `GroupToolBar.py`
        * SubBlockItem.py -> `SubGroupItem.py`

    * *Object Name Changes*
        * SubBlockItemType -> `SubGroupItemType`

    * Other variables referencing `block` naming convention has been updated to `group`

* Code updates to enable compatible functionality with Engine 2.0 version
    * `getObject()` references changed to `getChild()`
    * `removeObject()` references changed to `removeChild()`
    * Test/Example files reflect naming updates
    * Subscription parameter ordering fixed
* Added version number to title bar
* There were two versions of the changelog: `CHANGLOG.md` and `CHANGELOG.md`. Renamed first one to correct spelling and removed the second one. Now only one `CHANGELOG`
* allows setting log file from command line
* fixed GUI crash when getting/setting parameters for blocks that have changed names
* fixed crash bug when opening deployed blocks in the editor
* fixed crash bug when trying to delete events 
* Added Cut and Paste
* API Change on some emitted signls from BlockItem

2.0.4
-----

* Fixed Qt5-related bug with API change for QtGui.QDialog.getSaveFileName when performing as "Save As..." operation on Blocks and Tasks.

2.0.3
-----

* Fixed Qt5-related bug with API change for QtGui.QDialog.getOpenFileName when creating a Task from a template

2.0.2
-----

* Fixed Qt5-related pyqtSlot bug in BlockController

2.0.1
-----

* Updated changelog
* Added xenial ci

2.0.0
-----

* Added dependency to qscintilla2 (dependency used to be in pyqtlib)
* Fixed imports in TextEditor
* Made changes to make code work with PyQt4 and PyQt5: BlockController, EngineView
* Removed "scene" parameter from TaskView
* Fixed bug in BlockEditor when returning certain item types
* Updated BlockEditor unittests with new imports

1.1.1
-----

* Added application icon
* Fixed unittests to work on a headless system
