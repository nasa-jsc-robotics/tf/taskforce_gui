from taskforce_common import Task, TaskState, Response, ErrorResponse

class ExceptionBlock(Task):

    def __init__(self, name):
        super(ExceptionBlock, self).__init__(name)

        raise Exception('')

