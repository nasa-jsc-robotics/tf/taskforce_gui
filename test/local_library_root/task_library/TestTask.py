from taskforce_common import Task


class TestTask(Task):

    def __init__(self, name='test'):
        super(TestTask, self).__init__(name)
