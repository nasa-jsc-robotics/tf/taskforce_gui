from taskforce_common import Task, TaskState, Response, ErrorResponse

class ExceptionTask(Task):

    def __init__(self, name):
        super(ExceptionTask, self).__init__(name)

        raise Exception('')