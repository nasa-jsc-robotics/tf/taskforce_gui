import unittest
from pyqtlib import QtGui, QtCore
from taskforce_common import Task, Group
from taskforce_model import ParameterBindingModel

class TestTask(Task):

    parameters = {'param1': 'value1',
                  'param2': 'value2'}


class ParameterBindingModelTest(unittest.TestCase):

    def setUp(self):
        groupParameters = {'foo': True,
                           'bar': False,
                           'baz': [1, 2, 3]}

        task1 = TestTask('task1')
        task2 = TestTask('task2')
        task3 = TestTask('task3')

        task1Parameters = {'param1': 1,
                           'param2': 2}

        task2Parameters = {'param1': 3,
                           'param2': 4}

        task3Parameters = {'param1': 4,
                           'param2': 5}

        task1.setParameter(task1Parameters)
        task2.setParameter(task2Parameters)
        task3.setParameter(task3Parameters)

        self.assertDictEqual(task1Parameters, task1.getParameter())

        group = Group('test_group', 'test_group.group')
        group.setParameter(groupParameters)
        group.addTask(task1)
        group.addTask(task2)
        group.addTask(task3)

        group.bindParameter('foo', 'param1', 'task1')
        group.bindParameter('foo', 'param1', 'task2')
        group.bindParameter('baz', 'param1', 'task3')
        self.group = group

    def tearDown(self):
        pass

    def test_setGroup(self):
        """
        Test that group is set correctly 
        """
        # Table should look like:
        # > bar
        # V baz
        #     task3  task3_param1
        # V foo
        #     task1  task1_param1
        #     task2  task2_param2

        model = ParameterBindingModel()
        model.setGroup(self.group)

        self.assertEqual(3, model.rowCount())
        self.assertEqual('bar', model.item(0, 0).data(QtCore.Qt.DisplayRole))
        self.assertEqual('baz', model.item(1, 0).data(QtCore.Qt.DisplayRole))
        self.assertEqual('foo', model.item(2, 0).data(QtCore.Qt.DisplayRole))

        self.assertEqual(0, model.item(0, 0).rowCount())
        self.assertEqual(1, model.item(1, 0).rowCount())
        self.assertEqual(2, model.item(2, 0).rowCount())
        self.assertEqual('task1', model.item(2, 0).child(0, 0).data(QtCore.Qt.DisplayRole))
        self.assertEqual('param1', model.item(2, 0).child(0, 1).data(QtCore.Qt.DisplayRole))
        self.assertEqual('task2', model.item(2, 0).child(1, 0).data(QtCore.Qt.DisplayRole))
        self.assertEqual('param1', model.item(2, 0).child(1, 1).data(QtCore.Qt.DisplayRole))

    def test_getOptions(self):
        """
        Test that all blocks and parameters are found correctly when gathering options
        """
        model = ParameterBindingModel()
        model.setGroup(self.group)

        # options for top-level items should be the parameter keys of the group
        index = model.item(0, 0).index()
        options = model.getOptions(index)
        self.assertListEqual(['bar', 'baz', 'foo'], options)

        # options for the second-level items, first column, should be the names of the Blocks within the group
        index = model.item(1, 0).child(0, 0).index()
        options = model.getOptions(index)
        self.assertListEqual(['task1', 'task2', 'task3'], options)

        # options for the second-level items, second column, should be the names of the keys of the parameters of the
        # block listed in the first column
        index = model.item(2, 0).child(0, 1).index()
        options = model.getOptions(index)
        self.assertListEqual(['param1', 'param2'], options)

    def test_parametersChanged(self):
        """
        Test that bounded parameters were updated from their initial values
        """
        model = ParameterBindingModel()
        model.setGroup(self.group)
        tasks = self.group.getTasks()
        task1 = tasks['/test_group/task1']
        task2 = tasks['/test_group/task2']
        task3 = tasks['/test_group/task3']
        self.assertEqual(task1.parameters['param1'], self.group.parameters['foo'])
        self.assertEqual(task1.parameters['param2'], 2)
        self.assertEqual(task2.parameters['param1'], self.group.parameters['foo'])
        self.assertEqual(task2.parameters['param2'], 4)
        self.assertEqual(task3.parameters['param1'], self.group.parameters['baz'])
        self.assertEqual(task3.parameters['param2'], 5)
        
    def test_add_removeParameterBinding(self):
        """
        Test that bounded parameters are removed and added back correctly
        """
        model = ParameterBindingModel()
        model.setGroup(self.group)
        # This should be removing the binding from foo to param1 in task1
        index = model.item(2, 0).child(0, 0).index()
        model.removeBinding(index)
        model.setGroupBindingsFromModel()
        # reset task1 param1
        tasks = self.group.getTasks()
        task1 = tasks['/test_group/task1']
        task1Parameters = {'param1': 1}
        task1.setParameter(task1Parameters)
        # Set group parameters again
        groupParameters = {'foo': False}
        self.group.setParameter(groupParameters)
        # Check that task1 parameter did not chagne
        self.assertEqual(task1.parameters['param1'], 1)
        # Readd binding from foo to param1 in task1
        model.addBinding(model.item(2, 0), 'task1', 'param1')
        model.setGroupBindingsFromModel()
        # Check that task1 parameter did change
        self.assertEqual(task1.parameters['param1'], False)
