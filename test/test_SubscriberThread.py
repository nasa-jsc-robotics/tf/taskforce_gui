import unittest
from taskforce_common import QueuePubSub
from taskforce_model import SubscriberThread
from pyqtlib import QtCore, QtGui
import time

app = None


class Receiver(QtCore.QObject):

    def __init__(self):
        super(Receiver, self).__init__()
        self.count = 0
        self.data = None

    @QtCore.pyqtSlot(object)
    def callback(self, obj):
        self.count += 1
        self.data = obj


class Sender(QtCore.QObject):

    sendData = QtCore.pyqtSignal(object)

    def send(self, data):
        self.sendData.emit(data)


class TestSubscriberThread(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        global app
        if app is None:
            app = QtGui.QApplication([])

    @classmethod
    def tearDownClass(cls):
        global app
        if app is not None:
            app = None

    def test_signal_slot(self):
        sender = Sender()
        recv = Receiver()

        sender.sendData.connect(recv.callback)

        sender.send('test')
        time.sleep(0.1)
        self.assertEqual('test', recv.data)

    def test_run(self):
        global app

        conn = QueuePubSub(poll_timeout=0.0)
        pub = conn.getPublisher()
        sub = conn.getSubscriber()

        receiver = Receiver()

        subscriberThread = SubscriberThread(sub)
        subscriberThread.messageReceived.connect(receiver.callback)
        subscriberThread.start()

        pub.send('data')

        # need to sleep, tell Qt to process the event loop, and then sleep again to get the signal through
        time.sleep(0.1)
        app.processEvents()
        time.sleep(0.1)

        self.assertEqual('data', receiver.data)

        subscriberThread.stop()


if __name__ == "__main__":
    unittest.main()
