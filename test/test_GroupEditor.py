import unittest

from pyqtlib import QtCore, QtGui, QtTest

from pyqtlib.node_graph import BoxNode

from taskforce_common import logcfg
from taskforce_gui import GroupEditor
from taskforce_gui.GroupController import SubscribeCommand

import json

app = None

logcfg.configure_common_logging()


class TestGroupEditor(unittest.TestCase):

    def selectionChanged(self):
        print 'here!!!'
        self.printMousePos()

    @classmethod
    def setUpClass(cls):
        global app
        if app is None:
            app = QtGui.QApplication([])

    @classmethod
    def tearDownClass(cls):
        global app
        if app is not None:
            app = None

    def setUp(self):
        self.editor = GroupEditor()
        self.editor.selectionChanged.connect(self.selectionChanged)
        self.editor.setGeometry(0, 0, 400, 400)
        self.editor.show()
        #time.sleep(0.1)
        QtTest.QTest.qWaitForWindowShown(self.editor)

    def tearDown(self):
        pass
        #del self.editor

    # def test_dropGoodTaskFromLibrary(self):
    #     event = QtGui.QGraphicsSceneDragDropEvent()
    #
    #     mimeData = QtCore.QMimeData()
    #     urls = [QtCore.QUrl('file:///home/pstrawse/.taskforce_gui_library/taskforce_example_library/test/Test.block')]
    #     text = 'taskforce_example_library/test/Test.block'
    #     mimeData.setText(text)
    #     mimeData.setUrls(urls)
    #
    #     event.mimeData()
    #
    #     self.editor.dropEvent

    def printMousePos(self):
        self.printObjectPos('mouse', QtGui.QCursor)

    def printObjectPos(self, label, obj):
        self.printPoint(label, obj.pos())

    def printPoint(self, label, point):
        print '{}: x={} y={}'.format(label, point.x(), point.y())

    def test_subscribeTaskToBlock(self):
        controller = self.editor.controller

        controller.addTaskFromArgs('taskforce_common.task.Task', 'Task', 'Task')
        controller.undoStack.push(SubscribeCommand(controller.model, '<group>', 'complete', 'Task_0', 'exitBlock', False, 'Subscribe'))

        modelDef = controller.serializeModel()

        tasks_json = json.dumps(modelDef, indent=4, sort_keys=True, separators=(',', ': '))
        tasks_string = """{
    "group": {
        "eventRelays": [
            {
                "eventName": "complete",
                "sourceName": "<group>"
            },
            {
                "eventName": "shutdown",
                "sourceName": "<group>"
            },
            {
                "eventName": "startup",
                "sourceName": "<group>"
            },
            {
                "eventName": "stop",
                "sourceName": "<group>"
            }
        ],
        "fileName": "NewBlock.group",
        "name": "NewBlock",
        "parameterBindings": [],
        "parameters": {},
        "subGroups": [],
        "subscriptions": [
            {
                "breakpoint": false,
                "callback": "exitBlock",
                "destination": "<group>",
                "eventName": "complete",
                "source": "Task_0"
            }
        ],
        "tasks": [
            {
                "task": {
                    "class": "Task",
                    "module": "taskforce_common.task.Task",
                    "name": "Task_0",
                    "parameters": {}
                }
            }
        ]
    },
    "version": 2.0
}"""
        self.assertEqual(tasks_string, tasks_json)

    @unittest.skip("testing how to do a qtest")
    def test_addTaskFromArgs(self):
        controller = self.editor.controller
        view = self.editor.view

        controller.addTaskFromArgs('taskforce_common.task.Task', 'Task', 'Task')
        #self.runApp()

        item = controller.getItemFromName('Task_0')
        self.assertTrue(isinstance(item, BoxNode))
        self.assertEqual(item.instanceName, 'Task_0')
        self.assertEqual(item.typeName, 'Task')

        rect = item.boundingRect()
        x = rect.x() + rect.width() / 2.
        y = rect.y() + rect.height() / 2. - 20
        pos = QtCore.QPointF(x,y)
        print rect.width(), rect.height()
        print rect.x(), rect.y()

        scenePos = item.mapToScene(pos)
        viewPos = view.mapFromScene(scenePos)
        globalPos = view.viewport().mapToGlobal(viewPos)

        self.printObjectPos('item', item)
        self.printPoint('mid-point', pos)
        self.printPoint('item(scene)', scenePos)
        self.printPoint('item(view)', viewPos)
        self.printPoint('item(global)', globalPos)


        itemPos = QtCore.QPoint(globalPos.x(), globalPos.y())
        self.printMousePos()
        QTest.mouseMove(view, viewPos)
        self.printMousePos()

        QTest.mouseClick(view.viewport(), QtCore.Qt.LeftButton, pos=viewPos)
        print 'mouse clicked'
        QTest.qWait(10)

        print controller.selectedItems()
        pass

        #self.assertTrue(item in controller.selectedItems())
        #self.runApp()

    def test_openBlock_None(self):

        fileName = None
        self.assertFalse(self.editor.openBlock(fileName))

    def test_openBlock_missing_file(self):
        fileName = 'abcd'
        self.assertFalse(self.editor.openBlock(fileName))

    def runApp(self):
        '''
        Run this function to start the application for a test case.

        Just insert:
            self.runApp()

        anywhere in the source code

        Helpful for debugging.
        '''
        global app
        self.editor.show()
        app.exec_()


if __name__ == "__main__":
    unittest.main()
