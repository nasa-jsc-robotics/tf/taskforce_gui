import unittest
import time
from pyqtlib import QtGui, QtCore
#from taskforce_common import PipePubSub
from taskforce_model import EngineStatusModel
from taskforce_common.connection import PipePubSub, Message
from taskforce_common.task import Task

app = None


class TestStatusSignals(QtCore.QObject):

    def __init__(self, parent=None):
        super(TestStatusSignals, self).__init__(parent)
        self.update = None
        self.state = None

    @QtCore.pyqtSlot()
    def statusUpdated(self):
        self.update = True

    @QtCore.pyqtSlot(bool)
    def connectionStateChanged(self, val):
        self.state = val


class TestEngineStatusModel(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        global app
        if app is None:
            app = QtGui.QApplication([])

    @classmethod
    def tearDownClass(cls):
        global app
        if app is not None:
            app = None

    def setUp(self):
        self.conn = PipePubSub()
        self.publisher = self.conn.getPublisher()
        self.subscriber = self.conn.getSubscriber()

    def tearDown(self):
        pass

    def test_connected(self):
        global app

        engineStatusModel = EngineStatusModel(self.subscriber)

        # Assert that we start unconnected
        app.processEvents()
        self.assertFalse(engineStatusModel.connected)

        # Send a msg from the engine
        msg = Message('test', source='engine', data={})
        self.publisher.send(msg)
        time.sleep(0.1)

        # Assert that we are now connected
        app.processEvents()
        self.assertTrue(engineStatusModel.connected)

        # wait for the timeout, and assert that we are no longer connected.  This tests the staleTimeout functionality
        # NOTE the * 1.1 here is to make sure there is no race condition with the assert statement
        time.sleep((EngineStatusModel.statusTimeout * 1.1) / 1000.0)
        app.processEvents()
        self.assertFalse(engineStatusModel.connected)

    def test_connectedAsString(self):
        global app

        engineStatusModel = EngineStatusModel(self.subscriber)

        # Assert that we start unconnected
        app.processEvents()
        self.assertEqual('disconnected', engineStatusModel.connectedAsString)

        # Send a msg from the engine
        msg = Message('test', source='engine', data={})
        self.publisher.send(msg)
        time.sleep(0.1)

        # Assert that we are now connected
        app.processEvents()
        self.assertEqual('connected', engineStatusModel.connectedAsString)

        # wait for the timeout, and assert that we are no longer connected
        time.sleep((EngineStatusModel.statusTimeout * 1.1) / 1000.0)
        app.processEvents()
        self.assertEqual('disconnected', engineStatusModel.connectedAsString)

    def test_getStatus(self):
        global app

        engineStatusModel = EngineStatusModel(self.subscriber)

        task1 = Task("testTask1")
        task2 = Task("testTask2")

        self.assertEqual({}, engineStatusModel.getStatus())

        # Send a msg from the engine
        status = {'task1': task1.status(), 'task2': task2.status()}
        msg = Message('test', source='engine', data=status)
        self.publisher.send(msg)
        time.sleep(0.1)
        app.processEvents()

        status1 = engineStatusModel.getStatus('task1')
        status2 = engineStatusModel.getStatus('task2')
        self.assertEqual(status, engineStatusModel.getStatus())
        self.assertEqual('Task', status1['class'])
        self.assertEqual('Task', status2['class'])

    def test_shutdown(self):
        engineStatusModel = EngineStatusModel(self.subscriber)

        # Send a msg from the engine
        status1 = {'task1': 'good', 'task2': 'bad'}
        msg = Message('test', source='engine', data=status1)
        self.publisher.send(msg)
        time.sleep(0.1)
        app.processEvents()

        engineStatusModel.shutdown()

        # send another message.  Now that we are disconnected, we shouldn't get another incoming message
        status2 = {'task1': 'bad', 'task2': 'good'}
        msg = Message('test', source='engine', data=status2)
        self.publisher.send(msg)
        time.sleep(0.1)
        app.processEvents()

        self.assertTrue(engineStatusModel._shuttingDown)


        self.assertEqual(status1, engineStatusModel.getStatus())

    def test_signals(self):
        engineStatusModel = EngineStatusModel(self.subscriber)

        tester = TestStatusSignals()
        engineStatusModel.statusUpdated.connect(tester.statusUpdated)
        engineStatusModel.connectionStateChanged.connect(tester.connectionStateChanged)

        msg = Message('test', source='engine', data={})
        self.publisher.send(msg)
        time.sleep(0.1)
        app.processEvents()

        self.assertEqual(True, tester.update)
        self.assertTrue(tester.state)

        time.sleep(EngineStatusModel.statusTimeout * 1.1 / 1000.0)
        app.processEvents()

        self.assertFalse(tester.state)
