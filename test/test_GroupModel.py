import unittest
import json

from pyqtlib import QtCore, QtGui

from taskforce_common import Task, Group, GroupException, BlockException
from taskforce_model import GroupModel

from local_library_root.task_library.TestTask import TestTask

app = None


class TestQObject(QtCore.QObject):

    def __init__(self, parent=None):
        super(TestQObject, self).__init__(parent)

        self.task_added = None
        self.subgroup_added = None
        self.block_name_changed = None

    @QtCore.pyqtSlot(str, str)
    def taskAdded(self, name, className):
        self.task_added = (name, className)

    @QtCore.pyqtSlot(str, str)
    def subGroupAdded(self, name, className):
        self.subgroup_added = (name, className)

    @QtCore.pyqtSlot(str, str)
    def blockNameChanged(self, oldName, newName):
        self.block_name_changed = (oldName, newName)


class TestGroupModel(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        global app
        if app is None:
            app = QtGui.QApplication([])

    @classmethod
    def tearDownClass(cls):
        global app
        if app is not None:
            app = None

    def setUp(self):
        global app
        self.app = app

    def tearDown(self):
        pass

    def test_addSubGroup(self):
        name = 'TestGroup'
        filename = 'testGroup.block'
        model = GroupModel(name, filename)
        testQObject = TestQObject()
        model.subGroupAdded.connect(testQObject.subGroupAdded)
        model.taskAdded.connect(testQObject.taskAdded)

        subGroupName = 'testGroup'
        subGroup = Group(subGroupName, 'test_filename.group')
        model.addSubGroup(subGroup)

        # Assert that we got the right signal
        self.app.processEvents()
        self.assertEqual(('testGroup', 'test_filename.group'), testQObject.subgroup_added)

        self.assertEqual(subGroup, model.getChild('testGroup'))

        # Assert that adding a task or subtask with an existing name will raise an exception
        self.assertRaises(GroupException, model.addSubGroup, subGroup)

        # Assert that you cannot add some other object as a task
        self.assertRaises(GroupException, model.addSubGroup, TestTask('fooTask'))

        self.assertEqual(subGroup, model.getChild(subGroupName))
        self.assertRaises(GroupException, model.getChild, 'foo')

    def test_addTask(self):
        name = 'TestGroup'
        filename = 'testGroup.block'
        model = GroupModel(name, filename)
        testQObject = TestQObject()
        model.subGroupAdded.connect(testQObject.subGroupAdded)
        model.taskAdded.connect(testQObject.taskAdded)

        taskName = 'testTask'
        task = TestTask(taskName)
        model.addTask(task)

        # Assert that we got the right signal
        self.app.processEvents()
        self.assertEqual(('testTask', 'TestTask'), testQObject.task_added)

        # Assert that adding a task or subtask with an existing name will raise an exception
        self.assertRaises(GroupException, model.addTask, task)

        # Assert that you cannot add some other object as a task
        self.assertRaises(GroupException, model.addTask, 4)

        self.assertEqual(task, model.getChild(taskName))
        self.assertRaises(GroupException, model.getChild, 'foo')

    def test_changeName(self):
        name = 'TestGroup'
        filename = 'testGroup.group'
        model = GroupModel(name, filename)
        testQObject = TestQObject()
        model.subGroupAdded.connect(testQObject.subGroupAdded)
        model.taskAdded.connect(testQObject.taskAdded)
        model.blockNameChanged.connect(testQObject.blockNameChanged)

        model.addTask(Task('taskA'))
        model.addTask(Task('taskB'))

        # change the name of taskB to taskC
        model.changeName('taskB', 'taskC')

        self.app.processEvents()
        self.assertEqual(('taskB', 'taskC'), testQObject.block_name_changed)

        # Assert that taskB cannot be found
        self.assertRaises(Exception, model.getChild, 'taskB')

        # Assert that taskC can be found
        self.assertNotEqual(None, model.getChild('taskC'))

        # subscribe taskC to taskA
        model.subscribe('complete', 'taskA', 'taskC', 'startup', False)

        # assert that we can see the subscription
        subscriptions = model.group.getSubscriptions(recursive=False)
        found = False
        for s in subscriptions:
            if s.eventName == 'complete' and s.source == 'taskA':
                found = True
                break
        self.assertTrue(found)

        # change the name of taskA to taskD
        model.changeName('taskA', 'taskD')
        self.app.processEvents()
        self.assertEqual(('taskA', 'taskD'), testQObject.block_name_changed)

        # now, assert that the subscription to taskA is not found ...
        subscriptions = model.group.getSubscriptions(recursive=False)
        found = False
        for s in subscriptions:
            if s.eventName == 'complete' and s.source == 'taskA':
                found = True
                break
        self.assertFalse(found)

        # ... and that the subscription is now pointing to taskD
        subscriptions = model.group.getSubscriptions(recursive=False)
        found = False
        for s in subscriptions:
            if s.eventName == 'complete' and s.source == 'taskD':
                found = True
                break
        self.assertTrue(found)

    def test_reset(self):
        name = 'TestBlock'
        filename = 'testBlock.block'
        model = GroupModel(name, filename)

        taskName = 'testTask'
        taskObject = TestTask(taskName)
        model.addTask(taskObject)

        subGroupName = 'testGroup'
        subGroupObject = Group(subGroupName, 'testGroup.group')
        model.addSubGroup(subGroupObject)

        blocks = model.getBlocks()
        self.assertTrue('<group>' in blocks.keys())
        self.assertTrue('testTask' in blocks.keys())
        self.assertTrue('testGroup' in blocks.keys())

        model.reset()
        blocks = model.getBlocks()
        self.assertTrue('<group>' in blocks.keys())
        self.assertFalse('testTask' in blocks.keys())
        self.assertFalse('testGroup' in blocks.keys())
        self.assertEqual(name, model.name)
        self.assertEqual(filename, model.fileName)

    def test_constructor(self):
        self.assertRaises(BlockException,  GroupModel, None, None)
        self.assertRaises(BlockException, GroupModel, None, 'filename')
        model2 = GroupModel('test', None)

    def test_deserialize(self):
        name = 'TestBlock'
        filename = 'testBlock.group'
        model = GroupModel(name, filename)

        definition = {  "group": {
                            "eventRelays": [
                                {
                                    "eventName": "startup",
                                    "sourceName": "<group>"
                                }
                            ],
                            "fileName": "taskforce_library/cow/two_cows.group",
                            "name": "two_cows",
                            "parameterBindings": [],
                            "parameters": {},
                            "subGroups": [],
                            "subscriptions": [
                                {
                                    "callback": "startup",
                                    "destination": "task2",
                                    "eventName": "complete",
                                    "source": "task1"
                                },
                                {
                                    "callback": "startup",
                                    "destination": "task1",
                                    "eventName": "startup",
                                    "source": "<group>",
                                }
                            ],
                            "tasks": [
                                {
                                    "task": {
                                        "class": "Task",
                                        "module": "taskforce_common.task.Task",
                                        "name": "task2",
                                        "parameters": {},

                                    }
                                },
                                {
                                    "task": {
                                        "class": "Task",
                                        "module": "taskforce_common.task.Task",
                                        "name": "task1",
                                        "parameters": {}
                                    }
                                }
                            ]
                        }
                    }
        model.deserialize(definition)

        self.assertEqual(3, len(model.getBlocks()))
        groupObj = model.getBlocks()['<group>']
        subscriptions = groupObj.getSubscriptions()
        self.assertEqual(2, len(subscriptions))

    def test_getCommandNames(self):
        name = 'TestBlock'
        filename = 'testBlock.block'
        model = GroupModel(name, filename)

        model.addTask(Task('testTask'))
        commandNames = model.getCommandNames('testTask')
        self.assertEqual(sorted(commandNames), sorted(['init',
                                                       'getParameter',
                                                       'setParameter',
                                                       'shutdown',
                                                       'startup',
                                                       'status',
                                                       'stop']))

    def test_getEmitEventNames(self):
        name = 'TestBlock'
        filename = 'testBlock.block'
        model = GroupModel(name, filename)

        model.addTask(Task('testTask'))
        emitEventNames = model.getEventNames('testTask')
        self.assertEqual(sorted(emitEventNames), sorted(['started', 'stopped', 'status', 'paused', 'resumed', 'complete', 'failed', 'shutdown']))

    def test_removeObject(self):
        name = 'TestBlock'
        filename = 'testBlock.block'
        model = GroupModel(name, filename)

        taskA = Task('taskA')
        taskB = Task('taskB')
        model.addTask(taskA)
        model.addTask(taskB)

        model.subscribe('complete', 'taskA', 'taskB', 'startup', False)

        subscriptions = model.getSubscriptions()
        found = False
        for subscription in subscriptions:
            if subscription.eventName == 'complete' and subscription.source == taskA.path:
                found = True
                break
        self.assertTrue(found)

        # remove taskA
        model.removeChild('taskA')

        # test that the subscription from taskA by taskB is also removed
        subscriptions = model.getSubscriptions()
        found = False
        for subscription in subscriptions:
            if subscription.eventName == 'complete' and subscription.source == taskA.path:
                found = True
                break
        self.assertFalse(found)

    def test_serialize_task_block_subsriber(self):
        name = 'TestBlock'
        filename = 'testBlock.block'
        model = GroupModel(name, filename)

        taskName = 'Task_1'
        taskObject = Task(taskName)
        model.addTask(taskObject)

        model.subscribe('<block>', 'complete', 'Task_1', 'exitBlock', False)

        definition = model.serialize()

        tasks_json = json.dumps(definition, indent=4, sort_keys=True, separators=(',',': '))
        print tasks_json
        tasks_string = """{
    "group": {
        "eventRelays": [
            {
                "eventName": "complete",
                "sourceName": "<group>"
            },
            {
                "eventName": "shutdown",
                "sourceName": "<group>"
            },
            {
                "eventName": "startup",
                "sourceName": "<group>"
            },
            {
                "eventName": "stop",
                "sourceName": "<group>"
            }
        ],
        "fileName": "testBlock.block",
        "name": "TestBlock",
        "parameterBindings": [],
        "parameters": {},
        "subGroups": [],
        "subscriptions": [
            {
                "breakpoint": false,
                "callback": "exitBlock",
                "destination": "Task_1",
                "eventName": "<block>",
                "source": "complete"
            }
        ],
        "tasks": [
            {
                "task": {
                    "class": "Task",
                    "module": "taskforce_common.task.Task",
                    "name": "Task_1",
                    "parameters": {}
                }
            }
        ]
    },
    "version": 2.0
}"""
        self.assertEqual(tasks_string, tasks_json)

    def test_serialize_task_sorting(self):
        name = 'TestBlock'
        filename = 'testBlock.block'
        model = GroupModel(name, filename)

        taskName = 'Task_2'
        taskObject = Task(taskName)
        model.addTask(taskObject)

        taskName = 'Task_1'
        taskObject = Task(taskName)
        model.addTask(taskObject)

        taskName = 'Task_3'
        taskObject = TestTask(taskName)
        model.addTask(taskObject)

        definition = model.serialize()

        tasks_def = definition['group']['tasks']
        tasks_json = json.dumps(tasks_def, indent=4, sort_keys=True, separators=(',', ': '))
        tasks_string = """[
    {
        "task": {
            "class": "Task",
            "module": "taskforce_common.task.Task",
            "name": "Task_1",
            "parameters": {}
        }
    },
    {
        "task": {
            "class": "Task",
            "module": "taskforce_common.task.Task",
            "name": "Task_2",
            "parameters": {}
        }
    },
    {
        "task": {
            "class": "TestTask",
            "module": "local_library_root.task_library.TestTask",
            "name": "Task_3",
            "parameters": {}
        }
    }
]"""
        self.assertEqual(tasks_json, tasks_string)

    def test_serialize_subblock_sorting(self):
        name = 'TestBlock'
        filename = 'testBlock.block'
        model = GroupModel(name, filename)

        subBlockName = 'Block_3'
        subBlockObject = Group(subBlockName, 'path/to/blockA.block')
        model.addSubGroup(subBlockObject)

        subBlockName = 'Block_2'
        subBlockObject = Group(subBlockName, 'path/to/blockA.block')
        model.addSubGroup(subBlockObject)

        subBlockName = 'Block_1'
        subBlockObject = Group(subBlockName, 'path/to/blockB.block')
        model.addSubGroup(subBlockObject)

        definition = model.serialize()
        subBlocks_def = definition['group']['subGroups']

        subBlock_json = json.dumps(subBlocks_def, indent=4, sort_keys=True, separators=(',', ': '))
        subBlock_string = """[
    {
        "eventRelays": [
            {
                "eventName": "complete",
                "sourceName": "<group>"
            },
            {
                "eventName": "shutdown",
                "sourceName": "<group>"
            },
            {
                "eventName": "startup",
                "sourceName": "<group>"
            },
            {
                "eventName": "stop",
                "sourceName": "<group>"
            }
        ],
        "fileName": "path/to/blockA.block",
        "name": "Block_2",
        "parameters": {}
    },
    {
        "eventRelays": [
            {
                "eventName": "complete",
                "sourceName": "<group>"
            },
            {
                "eventName": "shutdown",
                "sourceName": "<group>"
            },
            {
                "eventName": "startup",
                "sourceName": "<group>"
            },
            {
                "eventName": "stop",
                "sourceName": "<group>"
            }
        ],
        "fileName": "path/to/blockA.block",
        "name": "Block_3",
        "parameters": {}
    },
    {
        "eventRelays": [
            {
                "eventName": "complete",
                "sourceName": "<group>"
            },
            {
                "eventName": "shutdown",
                "sourceName": "<group>"
            },
            {
                "eventName": "startup",
                "sourceName": "<group>"
            },
            {
                "eventName": "stop",
                "sourceName": "<group>"
            }
        ],
        "fileName": "path/to/blockB.block",
        "name": "Block_1",
        "parameters": {}
    }
]"""
        self.assertEqual(subBlock_string, subBlock_json)

    def test_subscribe(self):
        name = 'TestBlock'
        filename = 'testBlock.block'
        model = GroupModel(name, filename)

        taskA = Task('taskA')
        taskB = Task('taskB')

        model.addTask(taskA)
        model.addTask(taskB)

        subscriptions = model.getSubscriptions()
        found = False
        for subscription in subscriptions:
            if subscription.eventName == 'complete' and subscription.source == taskA.path:
                found = True
                break
        self.assertFalse(found)

        model.subscribe('complete', 'taskA', 'taskB', 'startup', False)

        subscriptions = model.getSubscriptions()
        found = False
        for subscription in subscriptions:
            if subscription.eventName == 'complete' and subscription.source == taskA.path:
                found = True
                break
        self.assertTrue(found)

    def test_unsubscribe(self):
        name = 'TestBlock'
        filename = 'testBlock.block'
        model = GroupModel(name, filename)

        taskA = Task('taskA')
        taskB = Task('taskB')

        model.addTask(taskA)
        model.addTask(taskB)

        model.subscribe('complete', 'taskA', 'taskB', 'startup', False)

        subscriptions = model.getSubscriptions()
        found = False
        for subscription in subscriptions:
            if subscription.eventName == 'complete' and subscription.source == taskA.path:
                found = True
                break
        self.assertTrue(found)

        model.unsubscribe('complete', 'taskA', 'taskB', 'startup')

        subscriptions = model.getSubscriptions()
        found = False
        for subscription in subscriptions:
            if subscription.eventName == 'complete' and subscription.source == taskA.path:
                found = True
                break
        self.assertFalse(found)


if __name__ == "__main__":
    unittest.main()
