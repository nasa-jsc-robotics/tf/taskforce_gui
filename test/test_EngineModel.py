import unittest
import os
import sys
import shutil

from pyqtlib import QtGui

from taskforce_common import PipePubSub, PipeReqRep
from taskforce_engine import Engine
from taskforce_model import EngineModel

app = None

TEST_FOLDER = os.path.dirname(os.path.realpath(__file__))
LOCAL_LIBRARY_ROOT = os.path.join(TEST_FOLDER, 'local_library_root')
TASK_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'task_library')
BLOCK_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'block_library')
ENGINE_LOCAL_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'engine_local_library_path')


class TestEngineModel(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        global app
        if app is None:
            app = QtGui.QApplication([])

    @classmethod
    def tearDownClass(cls):
        global app
        if app is not None:
            app = None

    def setUp(self):
        self.pubSubConn = PipePubSub()
        self.publisher = self.pubSubConn .getPublisher()
        self.subscriber = self.pubSubConn .getSubscriber()

        self.reqRepConn = PipeReqRep()
        self.client = self.reqRepConn.getClient()
        self.server = self.reqRepConn.getServer()

        self.engine = Engine([self.server], [], libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        self.engine.startup()

    def tearDown(self):
        self.engine.shutdown()
        if os.path.exists(ENGINE_LOCAL_LIBRARY_PATH):
            shutil.rmtree(ENGINE_LOCAL_LIBRARY_PATH)

    def test_groupDeploy(self):
        engineModel = EngineModel(self.client, self.subscriber, LOCAL_LIBRARY_ROOT)
        engineModel.groupDeploy("block_library/two_cows.group")

        self.assertEqual(3, len(self.engine.blocks))


    @unittest.skip("unittest TODO")
    def test_getLocalLibraryModel(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_getEngineStatusModel(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_localLibraryAddLink(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_localLibraryRemoveLink(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineShutdown(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineSerialize(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineSetLogLevel(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineSetStatusRate(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineStatus(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineLibraryAddModule(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineLibraryAddModuleFile(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineLibraryClear(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineLibraryGetInfo(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineLibraryGetModulePath(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineLibraryGetModuleHash(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineLibraryGetModuleInfo(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineLibraryRemoveModule(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineLibraryRemovePackage(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_engineListTasks(self):
        self.fail()

    def test_setLocalLibraryRoot(self):
        engineModel = EngineModel(self.client, self.subscriber)
        engineModel.setLocalLibraryRoot(LOCAL_LIBRARY_ROOT)
        self.assertTrue(LOCAL_LIBRARY_ROOT in sys.path)
        self.assertEqual(10, len(engineModel.engineManager.fileMap.getMap()))

    @unittest.skip("unittest TODO")
    def test_shutdown(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_synchEngineLibraryWithLocalLibrary(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskDeploy(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskGetParameter(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskSendCommand(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskSetBreakPoint(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskSetLogLevel(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskSetCommandHandlerLogLevel(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskSetEventHandlerLogLevel(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskSetParameter(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskShutdown(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskShutdownAll(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskStartup(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskStatus(self):
        self.fail()

    @unittest.skip("unittest TODO")
    def test_taskStop(self):
        self.fail()
