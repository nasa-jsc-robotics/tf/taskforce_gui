from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['taskforce_gui',
              'taskforce_model',
              'taskforce_gui.resources'],
    package_data={'taskforce_gui': ['resources/*.png']},
    package_dir={'': 'src'},
    scripts=['scripts/engine_status',
             'scripts/taskforce',
             'scripts/remove_redundant_taskviews']
)

setup(**d)
